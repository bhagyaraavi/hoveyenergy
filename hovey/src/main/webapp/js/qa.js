
/**
 * Created By BHagya On june 29th,2015
 * Js functions for QA Popup UI and datepicker
 */

	/**
	 * Function for closing the QA Popup Form
	 * Added on july 16th,2015
	 */
	var counter=1;
	
	function closeQAForm(id){
		var i=$('#'+id).data('id');
		$("#qaForm"+i).css('display','none');
	}

	$(document).ready(function() {
		  
	  	//close popup
	  		$("#close_popup").click(function(){
	  			$("#qa_form").fadeOut(500);
	  		});
	  		
	  		
	  		  		
	  	  	//maintain the popup at center of the page when browser resized
	  	  	$(window).bind('resize',positionQAPopup);
	  	  
		});
	/**
	 * ADded by bhagya on june 29th,2015
	 * @param dealId
	 * @param qaOrderStatus
	 *  this function for adding the qa report for deal
	 *  if qaorderStatus is false ,we are showing the qa popup form
	 *  else showing alert message
	 */
	function addQA(dealId,qaOrderStatus,isQAPartialReady){
		var orderId=0;
		if(isQAPartialReady==false){
			if(qaOrderStatus==false){
				qaFormProcess(dealId,orderId);
			}
			else{
				alert("QA Status Already Submitted By Order,You Can Check By Individual Order through View Option");
			}
		}
		else{
			alert("Some Of The Orders In Deal Not Ready For QA,You Can Submit QA Status By Individual Order through View Option");
		}
	}
	/**
	 * In this function sending the request to qa form
	 * and appending the form to Jsp page
	 */
	
	function qaFormProcess(dealId,orderId){
		var $body=$('body');
		$body.addClass('loading'); 
		 $.ajax({
				url:"/hoveyenergy/qa/addqa.do",
				data:	{ dealId:dealId,orderId:orderId,counter:counter},							
				type: "GET",	
				 beforeSend: function(){
					   $body.addClass('loading');
			        },
				success: function(data){
					$("#qa_form").append(data);
					$("#qa_form").fadeIn(1000);
					$('#qaDate'+counter).focus();
					 positionQAPopup();
					 $("#qaDate"+counter).datepicker({
			    	  	changeMonth: true,
				        changeYear: true,
					   	showButtonPanel: true,	
					   	closeText: 'Clear',
				        dateFormat: 'mm/dd/yy',
				        yearRange: '2012:c+10',
						showOn: "button",
						buttonImage: "/hoveyenergy/images/calendar.gif",
						buttonImageOnly: true, 
						 onSelect: function(selected) {
						        $("#qaDate").datepicker("option", selected);
				        }
					 });
					 counter++;
					
				},
				
				complete: function(){
				   $body.removeClass('loading');
		        } 
		});					
		
	}
	//position the popup at the center of the page
		function positionQAPopup(){
		  if(!$("#qa_form").is(':visible')){
		    return;
		  } 
		  $("#qa_form").css({
		      left: ($(window).width() - $('#qa_form').width()) / 2,
		      top: ($(window).width() - $('#qa_form').width()) / 21,
		      position:'absolute'
		  });
		}
	/**
	 * Function for the add qa for individual order
	 * @param id
	 */
	function qaForm(orderId){
		 var dealId=0;
		qaFormProcess(dealId,orderId);
		
	}
	
	
	/**
	 * Added By Bhagya On July 01st,2015
	 * Function for validating the qa Form
	 * @returns {Boolean}
	 */
	function validateQAForm(i){
		var qaDate=$('#qaDate'+i).val();
		var qaScore=$('#qaScore'+i).val();
		var qaStatus=$('#qaStatus'+i).val();
		var reportedBy=$('#reportedBy'+i).val();
		var a,b,c,d=true;
		if(qaDate==null || qaDate==''){
			$('#qaDate_error'+i).html("Date should not be empty");
			a=false;
		}
		else{
			$('#qaDate_error'+i).html('');
			a=true;
		}
		if(qaScore==null || qaScore==''){
			$('#qaScore_error'+i).html("score should not be empty");
			b=false;
		}
		else{
			$('#qaScore_error'+i).html('');
			b=true;
		}
		
		if(qaStatus==null || qaStatus=='' || qaStatus=='--select--'){
			$('#qaStatus_error'+i).html("QA status should  be select");
			c=false;
		}
		else{
			$('#qaStatus_error'+i).html('');
			c=true;
		}
		if(reportedBy==null || reportedBy==''){
			$('#reportedBy_error'+i).html("Reported By should not be empty");
			d=false;
		}
		else{
			$('#reportedBy_error'+i).html('');
			d=true;
		}
		if(a==true && b==true && c==true && d==true ){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	function editQA(dealId,qaOrderStatus){
		var orderId=0;
		if(qaOrderStatus==false){
				qaFormProcess(dealId,orderId);
			}
			else{
				alert("QA Status Already Submitted By Order,You Can Check By Individual Order through View Option");
			}
		}
	