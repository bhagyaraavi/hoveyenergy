	<!-- Created By Bhagya On June 26th,2015
	Jsp for the Editing QA FOrm -->
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="qaForm${counter}" class="qaForm">
   	<div id="content">
	  <div id="wrapper"> 
	  	<div class="data-input-form qa-main-div" style="height:600px;overflow-y:scroll;width:701px;background: white;border: 5px solid rgb(140, 177, 231);">
			<div class="title">Edit Quality Assurance Report</div>
				<a href="#" id="close_popup${counter}" data-id="${counter}" onclick="closeQAForm(this.id);"><img src="/hoveyenergy/images/close_icon1.png" height="28px;" width="28px;" style="float:right;margin-right: -75px;margin-top: -37px;"></a>
	  			<form:form class="form-horizontal1 form-validate1" method="POST" action="/hoveyenergy/qa/addqa.do" id="hoveyQAForm" commandName="hoveyQA" onsubmit="return validateQAForm(${counter})">
	  				<input type="hidden" name="dealId" value="${dealId}"/>
	  				<input type="hidden" name="orderId" value="${orderId}"/>
	  				<input type="hidden" name="qaId" value="${qaReport.qaId}">
	  				<div style="margin-top: 20px;height: 13px;" class="qaerror"><span id="qaDate_error${counter }" ></span></div>
					<div class="control-qa" >
							
						<label class="control-label" for="textfield">Date:<span class="error_msg">*</span></label>										    
						<input type="text" name="qaDate"  class="input-xlarge qa-input qa-datepicker" id="qaDate${counter}" style="width:220px !important;" value="<fmt:formatDate value='${qaReport.qaDate}' pattern='MM/dd/yyyy'/>"/>
										
					</div>
					<div style="margin-top:-13px; " class="qaerror"><span id="qaScore_error${counter}" style="float:right;margin-right:85px;margin-top:-15px;" ></span></div>
					<div class="control-qa" style="float:right;">
						
						<label class="control-label" for="textfield">Score:<span class="error_msg">*</span></label>										    
							<input type="text" name="qaScore"  class="input-xlarge qa-input" id="qaScore${counter}" value="${qaReport.qaScore}">
										
					</div>
					<div class="control-qa" >
						<label class="control-label" for="textfield">Agent Name:</label>										    
							<input type="text" name="agentName"  class="input-xlarge qa-input" id="agentName${counter}" value="${qaReport.order.createdAgent.firstName} ${qaReport.order.createdAgent.lastName}" readonly="readonly">			
					</div>
					<div  style="clear:both;">
						<label class="control-label" for="textfield">Business Name:</label>										    
							<input type="text" name="businessName"  class="input-xlarge qa-input" id="businessName${counter}" value="${qaReport.order.businessName}" readonly="readonly">			
					</div>
					
					<div class="control-qa" style="float:right;margin-top:2px;">
							<label class="control-label" for="textfield">Phone:</label>										    
							<input type="text" name="phoneNo"  class="input-xlarge qa-input" id="phone" value="${qaReport.order.taxId.phoneNo}" readonly="readonly">
										
					</div>
					<div class="checklist-main-div">
					 	<div class="checklist-main-leftdiv">
						<div>
							<div class="title" style="text-decoration: none;" >SALES</div>
							<div class="checklist-sub-div">
								<div class="checklist-sub-title">Checklist(Total 10 PossiblePoints)</div>
								<div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.brandsCall eq true }">
			                                      <input type="checkbox" name="brandsCall" checked="checked"  id="brandsCall" value="1" />
			                                   </c:when>
			                                   <c:otherwise>
			                                      <input type="checkbox"  name="brandsCall" id="brandsCall"  />
			                                  </c:otherwise>
                               				</c:choose>
											<label>Brands Call Properly</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:43px;">
											<c:choose>
	                                   			<c:when test="${qaReport.soundsConfident eq true }">
	                                     		 <input type="checkbox" name="soundsConfident" checked="checked"  id="soundsConfident" value="1" />
	                                   			</c:when>
	                                			<c:otherwise>
	                                      		<input type="checkbox"  name="soundsConfident" id="soundsConfident"  />
	                                			</c:otherwise>
                               				</c:choose>
											<label>Sounds Confident</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.positiveTone eq true }">
			                                      <input type="checkbox" name="positiveTone" checked="checked"  id="positiveTone" value="1" />
			                                   </c:when>
			                                	<c:otherwise>
			                                      <input type="checkbox"  name="positiveTone" id="positiveTone" />
			                                	</c:otherwise>
                               				</c:choose>
											<label>Positive Tone</label>
										</div>
										<div class="checklist-sub-rightdiv">
											<c:choose>
		                                   		<c:when test="${qaReport.reassuresSincerely eq true }">
		                                     		 <input type="checkbox" name="reassuresSincerely" checked="checked"  id="reassuresSincerely" value="1" />
		                                   		</c:when>
		                                		<c:otherwise>
		                                      		<input type="checkbox"  name="reassuresSincerely" id="reassuresSincerely"  />
		                                		</c:otherwise>
		                               		</c:choose>
											<label>Reassures Sincerely</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.soundsNatural eq true }">
			                                      <input type="checkbox" name="soundsNatural" checked="checked"  id="soundsNatural" value="1" />
			                                   </c:when>
			                                	<c:otherwise>
			                                      <input type="checkbox"  name="soundsNatural" id="soundsNatural" />
			                                	</c:otherwise>
			                               	</c:choose>
											<label>Sounds Natural</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:28px;">
											<c:choose>
		                                   		<c:when test="${qaReport.assumptiveSwagger eq true }">
		                                     		 <input type="checkbox" name="assumptiveSwagger" checked="checked"  id="assumptiveSwagger" value="1" />
		                                   		</c:when>
		                                		<c:otherwise>
		                                      		<input type="checkbox"  name="assumptiveSwagger" id="assumptiveSwagger" />
		                                		</c:otherwise>
		                               		</c:choose>
											<label>Assumptive Swagger</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.sticksPitch eq true }">
			                                      <input type="checkbox" name="sticksPitch" checked="checked"  id="sticksPitch" value="1" />
			                                   </c:when>
			                                	<c:otherwise>
			                                      <input type="checkbox"  name="sticksPitch" id="sticksPitch"  />
			                                	</c:otherwise>
			                               	</c:choose>
											<label>Sticks to the Pitch</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:62px;">
											<c:choose>
		                                   		<c:when test="${qaReport.fullDisclosure eq true }">
		                                     		 <input type="checkbox" name="fullDisclosure" checked="checked"  id="fullDisclosure" value="1" />
		                                   		</c:when>
		                                		<c:otherwise>
		                                      		<input type="checkbox"  name="fullDisclosure" id="fullDisclosure"  />
		                                		</c:otherwise>
		                               		</c:choose>
											<label>Full Disclosure</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.professionalism eq true }">
			                                      <input type="checkbox" name="professionalism" checked="checked"  id="professionalism" value="1" />
			                                   </c:when>
			                                	<c:otherwise>
			                                      <input type="checkbox"  name="professionalism" id="professionalism"  />
			                                	</c:otherwise>
			                               	</c:choose>
											<label>Professionalism</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:95px;">
											<c:choose>
		                                   		<c:when test="${qaReport.reDirect eq true }">
		                                     		 <input type="checkbox" name="reDirect" checked="checked"  id="reDirect" value="1" />
		                                   		</c:when>
		                                		<c:otherwise>
		                                      		<input type="checkbox"  name="reDirect" id="reDirect"  />
		                                		</c:otherwise>
		                               		</c:choose>
											<label>Re-Direct</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<c:choose>
			                                   <c:when test="${qaReport.transition eq true }">
			                                      <input type="checkbox" name="transition" checked="checked"  id="transition" value="1" />
			                                   </c:when>
			                                	<c:otherwise>
			                                      <input type="checkbox"  name="transition" id="transition"  />
			                                	</c:otherwise>
			                               	</c:choose>
											<label>Transition</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:100px;">
											<c:choose>
		                                   		<c:when test="${qaReport.wrapUp eq true }">
		                                     		 <input type="checkbox" name="wrapUp" checked="checked"  id="wrapUp" value="1" />
		                                   		</c:when>
		                                		<c:otherwise>
		                                      		<input type="checkbox"  name="wrapUp" id="wrapUp"  />
		                                		</c:otherwise>
		                               		</c:choose>
											<label>Wrap Up</label>
										</div>
									</div>
								</div>
							</div>
							<div class="sales_dropdown">
										<div class="checklist-sub-leftdiv">
											<label style="float: left;">Need Courtesy <br /> Call</label>
											<select  name="needCourtesyCall" id="needCourtesyCall" style="width: 55px;margin-left: 5px;" >
												<option value="no" ${qaReport.needCourtesyCall==false ? 'selected' : '' }>No</option>
				  								<option value="yes" ${qaReport.needCourtesyCall==true ? 'selected' : '' }>Yes</option>
				   	  						</select>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:05px;">
											<label style="float: left;">Courtesy Call <br /> Completed</label>
											<select  name="courtesyCallCompleted" id="courtesyCallCompleted" style="width: 55px;margin-left: 5px;margin-right:23px;">
				   	  							<option value="no" ${qaReport.courtesyCallCompleted==false ? 'selected' : '' }>No</option>
				  								<option value="yes" ${qaReport.courtesyCallCompleted==true ? 'selected' : '' }>Yes</option>
				   	  						</select>
										</div>
								</div>
						</div>
						<div class="qa-comments">
							<div class="title" style="float:left; ">Comments</div>
							<div>
								<textarea name="comments" id="comments" style="width:380px; height:165px;border:1px solid #ccc;overflow-y: scroll;">${qaReport.comments}</textarea>
							</div>
						</div>
					</div>
						<div class="checklist-main-rightdiv">
							<div class="title" style="text-decoration: none;" >TPV</div>
							<div class="checklist-sub-tpvdiv">
								<div class="title" style="float:right;margin-right:20px;font-size:12px;">PASS</div>
								<div class="mn_maindiv">
									<div class="mn_checklist">
										<label>Phone Number</label>
										<c:choose>
                                   			<c:when test="${qaReport.phoneNumber eq true }">
                                     		 	<input type="checkbox" name="phoneNumber" checked="checked"  id="phoneNumber" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="phoneNumber" id="phoneNumber"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									
									<div class="mn_checklist">
										<label>State</label>
										<c:choose>
                                   			<c:when test="${qaReport.state eq true }">
                                     		 	<input type="checkbox" name="state" checked="checked"  id="state" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="state" id="state"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Product ID</label>
										<c:choose>
                                   			<c:when test="${qaReport.productId eq true }">
                                     		 	<input type="checkbox" name="productId" checked="checked"  id="productId" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="productId" id="productId"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									
									<div class="mn_checklist">
										<!-- <label>#Of Accounts</label> -->
										<label>Number Of Accounts</label>
										<c:choose>
                                   			<c:when test="${qaReport.ofAccounts eq true }">
                                     		 	<input type="checkbox" name="ofAccounts" checked="checked"  id="ofAccounts" value="1" class="tpv-input"/>
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="ofAccounts" id="ofAccounts"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<!-- <label>Accnt #s</label> -->
										<label>Account Numbers</label>
										<c:choose>
                                   			<c:when test="${qaReport.accounts eq true }">
                                     		 	<input type="checkbox" name="accounts" checked="checked"  id="accounts" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="accounts" id="accounts"  class="tpv-input" >
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Rate/Term/Start date</label>
										<c:choose>
                                   			<c:when test="${qaReport.rate eq true }">
                                     		 	<input type="checkbox" name="rate" checked="checked"  id="rate" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="rate" id="rate"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<%-- <div class="mn_checklist">
										<label>TPV#</label>
										<c:choose>
                                   			<c:when test="${qaReport.tpv eq true }">
                                     		 	<input type="checkbox" name="tpv" checked="checked"  id="tpv" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="tpv" id="tpv"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br> --%>
									<div class="mn_checklist">
										<label>Confirmation Number</label>
										<c:choose>
                                   			<c:when test="${qaReport.confirmationNumber eq true }">
                                     		 	<input type="checkbox" name="confirmationNumber" checked="checked"  id="confirmationNumber" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="confirmationNumber" id="confirmationNumber"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									
									<div class="mn_checklist">
										<label>Permission to Record</label>
										<c:choose>
                                   			<c:when test="${qaReport.permission eq true }">
                                     		 	<input type="checkbox" name="permission" checked="checked"  id="permission" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="permission" id="permission"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<%-- <div class="mn_checklist">
										<label>Cust Name</label>
										<c:choose>
                                   			<c:when test="${qaReport.custName eq true }">
                                     		 	<input type="checkbox" name="custName" checked="checked"  id="custName" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="custName" id="custName"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br> --%>
									<div class="mn_checklist">
										<label>Company name as appears on bill</label>
										<c:choose>
                                   			<c:when test="${qaReport.companyName eq true }">
                                     		 	<input type="checkbox" name="companyName" checked="checked"  id="companyName" value="1" class="tpv-input" >
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="companyName" id="companyName"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Billing address</label>
										<c:choose>
                                   			<c:when test="${qaReport.billing eq true }">
                                     		 	<input type="checkbox" name="billing" checked="checked"  id="billing" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="billing" id="billing"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Full Name</label>
										<c:choose>
                                   			<c:when test="${qaReport.fullName eq true }">
                                     		 	<input type="checkbox" name="fullName" checked="checked"  id="fullName" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="fullName" id="fullName"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Title</label>
										<c:choose>
                                   			<c:when test="${qaReport.title eq true }">
                                     		 	<input type="checkbox" name="title" checked="checked"  id="checklist_title" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="title" id="title"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<!-- <label>Authorization/Title</label> -->
										<label>Authorization</label>
										<c:choose>
                                   			<c:when test="${qaReport.authorization eq true }">
                                     		 	<input type="checkbox" name="authorization" checked="checked"  id="authorization" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="authorization" id="authorization"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Account Number Verification</label>
										<c:choose>
                                   			<c:when test="${qaReport.accountNumberVerification eq true }">
                                     		 	<input type="checkbox" name="accountNumberVerification" checked="checked"  id="accountNumberVerification" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="accountNumberVerification" id="accountNumberVerification"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Pass through/ Variable product</label>
										<c:choose>
                                   			<c:when test="${qaReport.passThroughVaraibleProduct eq true }">
                                     		 	<input type="checkbox" name="passThroughVaraibleProduct" checked="checked"  id="passThroughVaraibleProduct" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="passThroughVaraibleProduct" id="passThroughVaraibleProduct"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Terms/ETFs</label>
										<c:choose>
                                   			<c:when test="${qaReport.termsETFs eq true }">
                                     		 	<input type="checkbox" name="termsETFs" checked="checked"  id="termsETFs" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="termsETFs" id="termsETFs"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<%-- <div class="mn_checklist">
										<label>Confir.Accnt #s/Term</label>
										<c:choose>
                                   			<c:when test="${qaReport.confirmAccounts eq true }">
                                     		 	<input type="checkbox" name="confirmAccounts" checked="checked"  id="confirmAccounts" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="confirmAccounts" id="confirmAccounts"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br> --%>
									<div class="mn_checklist">
										<label>Champion is a supplier</label>
										<c:choose>
                                   			<c:when test="${qaReport.championSupplier eq true }">
                                     		 	<input type="checkbox" name="championSupplier" checked="checked"  id="championSupplier" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="championSupplier" id="championSupplier"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Rescission Period</label>
										<c:choose>
                                   			<c:when test="${qaReport.rescission eq true }">
                                     		 	<input type="checkbox" name="rescission" checked="checked"  id="rescission" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="rescission" id="rescission"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<div class="mn_checklist">
										<label>Confirm Decision</label>
										<c:choose>
                                   			<c:when test="${qaReport.confirmDecission eq true }">
                                     		 	<input type="checkbox" name="confirmDecission" checked="checked"  id="confirmDecission" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="confirmDecission" id="confirmDecission"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									<%-- <div class="mn_checklist">
										<label>Proper Finish</label>
										<c:choose>
                                   			<c:when test="${qaReport.properFinish eq true }">
                                     		 	<input type="checkbox" name="properFinish" checked="checked"  id="properFinish" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="properFinish" id="properFinish"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br> --%>
									<div class="mn_checklist">
										<label>Complete TPV</label>
										<c:choose>
                                   			<c:when test="${qaReport.completeTPV eq true }">
                                     		 	<input type="checkbox" name="completeTPV" checked="checked"  id="completeTPV" value="1" class="tpv-input" />
                                   			</c:when>
                                			<c:otherwise>
                                      			<input type="checkbox"  name="completeTPV" id="completeTPV"  class="tpv-input" />
                                			</c:otherwise>
                               			</c:choose>
									</div><br>
									
								</div>
							</div>
						</div>
					</div>
					
					<div style="float:left;margin-top:10px;">
						<div class="qaerror"><span id="qaStatus_error${counter}" style="margin-left: 10px;"></span></div>
							<label class="qa-font" style="display: initial !important;float:left;">QA Status</label><span class="error_msg" style="float:left;">*</span>
							<select  name="qaStatus" id="qaStatus${counter}" style="border: 1px solid #ccc;border-radius: 3px;width:187px; margin-left:10px;"  >
				   	  				<option value="yes" ${qaReport.qaStatus=='1' ? 'selected' : '' }>PASS</option>
				   	  				<option value="no" ${qaReport.qaStatus=='0' ? 'selected' : '' }>FAIL</option>
				   	  		</select>
				   	</div>
				   	<div  style="float:right;margin-top:17px;">
				   	<div  class="qaerror"><span id="reportedBy_error${counter}" ></span></div>
							<label class="control-label" for="textfield" >Reported By<span class="error_msg">*</span></label>									    
							<input type="text" name="reportedBy"  class="input-xlarge qa-input" id="reportedBy${counter}" value="${qaReport.reportedBy}" >
										
					</div>
				   	<div style="margin-top:91px;text-align:center;">
							<button type="submit"  id="qaFormSubmit" class="btn-update btn" style="width: 70px;height:30px;">Submit</button>
					</div>
					
				</form:form>
		    </div>
       </div>
	</div> 
</div>