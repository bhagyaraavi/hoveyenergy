	<!-- Created By Bhagya On June 26th,2015
	Jsp for the adding QA FOrm -->
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="qaForm${counter}" class="qaForm">
   	<div id="content">
	  <div id="wrapper"> 
	  	<div class="data-input-form qa-main-div" style="height:600px;overflow-y:scroll;width:701px;background: white;border: 5px solid rgb(140, 177, 231);">
			<div class="title">Quality Assurance Report</div>
				<a href="#" id="close_popup${counter}" data-id="${counter}" onclick="closeQAForm(this.id);"><img src="/hoveyenergy/images/close_icon1.png" height="28px;" width="28px;" style="float:right;margin-right: -75px;margin-top: -37px;"></a>
	  			<form:form class="form-horizontal1 form-validate1" method="POST" action="/hoveyenergy/qa/addqa.do" id="hoveyQAForm" commandName="hoveyQA" onsubmit="return validateQAForm(${counter})">
	  				<input type="hidden" name="dealId" value="${dealId}"/>
	  				<input type="hidden" name="orderId" value="${orderId}"/>
	  				<div style="margin-top: 20px;height: 13px;" class="qaerror"><span id="qaDate_error${counter }" ></span></div>
					<div class="control-qa" >
							
						<label class="control-label" for="textfield">Date:<span class="error_msg">*</span></label>										    
							<input type="text" name="qaDate"  class="input-xlarge qa-input qa-datepicker" id="qaDate${counter}" style="width:220px !important;">
										
					</div>
					<div style="margin-top:-13px; " class="qaerror"><span id="qaScore_error${counter}" style="float:right;margin-right:85px;margin-top:-15px;" ></span></div>
					<div class="control-qa" style="float:right;">
						
						<label class="control-label" for="textfield">Score:<span class="error_msg">*</span></label>										    
							<input type="text" name="qaScore"  class="input-xlarge qa-input" id="qaScore${counter}" >
										
					</div>
					<div class="control-qa" >
						<label class="control-label" for="textfield">Agent Name:</label>										    
							<input type="text" name="agentName"  class="input-xlarge qa-input" id="agentName${counter}" value="${agentName}" readonly="readonly">			
					</div>
					<div  style="clear:both;">
						<label class="control-label" for="textfield">Business Name:</label>										    
							<input type="text" name="businessName"  class="input-xlarge qa-input" id="businessName${counter}" value="${businessName}" readonly="readonly">			
					</div>
					
					<div class="control-qa" style="float:right;margin-top:2px;">
							<label class="control-label" for="textfield">Phone:</label>										    
							<input type="text" name="phoneNo"  class="input-xlarge qa-input" id="phone" value="${phoneNo}" readonly="readonly">
										
					</div>
					<div class="checklist-main-div">
					<div class="checklist-main-leftdiv">
						<div>
							<div class="title" style="text-decoration: none;" >SALES</div>
							<div class="checklist-sub-div">
								<div class="checklist-sub-title">Checklist(Total 10 PossiblePoints)</div>
								<div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="brandsCall" id="brandsCall"/>
											<label>Brands Call Properly</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:43px;">
											<input type="checkbox"  name="soundsConfident" id="soundsConfident" />
											<label>Sounds Confident</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="positiveTone" id="positiveTone" />
											<label>Positive Tone</label>
										</div>
										<div class="checklist-sub-rightdiv">
											<input type="checkbox"  name="reassuresSincerely" id="reassuresSincerely"/>
											<label>Reassures Sincerely</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="soundsNatural" id="soundsNatural"/>
											<label>Sounds Natural</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:28px;">
											<input type="checkbox"  name="assumptiveSwagger" id="assumptiveSwagger" />
											<label>Assumptive Swagger</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="sticksPitch" id="sticksPitch"/>
											<label>Sticks to the Pitch</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:62px;">
											<input type="checkbox"  name="fullDisclosure" id="fullDisclosure"/>
											<label>Full Disclosure</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="professionalism" id="professionalism"/>
											<label>Professionalism</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:95px;">
											<input type="checkbox"  name="reDirect" id="reDirect"  />
											<label>Re-Direct</label>
										</div>
									</div>
									<div class="checklist_sales_row">
										<div class="checklist-sub-leftdiv">
											<input type="checkbox"  name="transition" id="transition"/>
											<label>Transition</label>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:100px;">
											<input type="checkbox"  name="wrapUp" id="wrapUp"  />
											<label>Wrap Up</label>
										</div>
									</div>
								</div>
							</div>
							<div class="sales_dropdown">
										<div class="checklist-sub-leftdiv">
											<label style="float: left;">Need Courtesy <br /> Call</label>
											<select  name="needCourtesyCall" id="needCourtesyCall" style="width: 55px;margin-left: 5px;" >
				   	  							<option value="no">No</option>
				   	  							<option value="yes">Yes</option>
				   	  						</select>
										</div>
										<div class="checklist-sub-rightdiv" style="margin-right:5px;">
											<label style="float: left;">Courtesy Call <br /> Completed</label>
											<select  name="courtesyCallCompleted" id="courtesyCallCompleted" style="width: 55px;margin-left: 5px;margin-right:23px;">
				   	  							<option value="no">No</option>
				   	  							<option value="yes">Yes</option>
				   	  						</select>
										</div>
							</div>
						</div>
							<div class="qa-comments">
								<div class="title" style="float:left; ">Comments</div>
								<div>
									<textarea name="comments" id="comments" style="width:380px; height:165px;border:1px solid #ccc;overflow-y: scroll;"></textarea>
								</div>
							</div>
			   			</div>
						<div class="checklist-main-rightdiv">
							<div class="title" style="text-decoration: none;" >TPV</div>
							<div class="checklist-sub-tpvdiv">
								<div class="title" style="float:right;margin-right:20px;font-size:12px;">PASS</div>
								<div class="mn_maindiv">
									<div class="mn_checklist">
										<label>Phone Number</label>
										<input type="checkbox"  name="phoneNumber" id="phoneNumber" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>State</label>
										<input type="checkbox"  name="state" id="state" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Product ID</label>
										<input type="checkbox"  name="productId" id="productId" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<!-- <label>#Of Accounts</label> -->
										<label>Number Of Accounts</label>
										<input type="checkbox"  name="ofAccounts" id="ofAccounts" class="tpv-input"/>
									</div><br>
									<div class="mn_checklist">
										<!-- <label>Accnt #s</label> -->
										<label>Account Numbers</label>
										<input type="checkbox"  name="accounts" id="accounts" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Rate/Term/Start date</label>
										<input type="checkbox"  name="rate" id="rate" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Confirmation Number</label>
										<input type="checkbox"  name="confirmationNumber" id="confirmationNumber" class="tpv-input" />
									</div><br>
									<!-- <div class="mn_checklist">
										<label>TPV#</label>
										<input type="checkbox"  name="tpv" id="tpv"  class="tpv-input" />
									</div><br> -->
									<div class="mn_checklist">
										<label>Permission to Record</label>
										<input type="checkbox"  name="permission" id="permission" class="tpv-input" />
									</div><br>
									<!-- <div class="mn_checklist">
										<label>Cust Name</label>
										<input type="checkbox"  name="custName" id="custName" class="tpv-input" />
									</div><br> -->
									<div class="mn_checklist">
										<label>Company name as appears on bill</label>
										<input type="checkbox"  name="companyName" id="companyName" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Billing address</label>
										<input type="checkbox"  name="billing" id="billing" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Full Name</label>
										<input type="checkbox"  name="fullName" id="fullName" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Title</label>
										<input type="checkbox"  name="title" id="checklist_title" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<!-- <label>Authorization/Title</label> -->
										<label>Authorization</label>
										<input type="checkbox"  name="authorization" id="authorization" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Account Number Verification</label>
										<input type="checkbox"  name="accountNumberVerification" id="accountNumberVerification" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Pass through/ Variable product</label>
										<input type="checkbox"  name="passThroughVaraibleProduct" id="passThroughVariableProduct" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Terms/ETFs</label>
										<input type="checkbox"  name="termsETFs" id="termsETFs" class="tpv-input" />
									</div><br>
									<!-- <div class="mn_checklist">
										<label>Confir.Accnt #s/Term</label>
										<input type="checkbox"  name="confirmAccounts" id="confirmAccounts" class="tpv-input" />
									</div><br> -->
									<div class="mn_checklist">
										<label>Champion is a Supplier</label>
										<input type="checkbox"  name="championSupplier" id="championSupplier" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Rescission Period</label>
										<input type="checkbox"  name="rescission" id="rescission" class="tpv-input" />
									</div><br>
									<div class="mn_checklist">
										<label>Confirm Decision</label>
										<input type="checkbox"  name="confirmDecission" id="confirmDecission" class="tpv-input" />
									</div><br>
									<!-- <div class="mn_checklist">
										<label>Proper Finish</label>
										<input type="checkbox"  name="properFinish" id="properFinish" class="tpv-input" />
									</div><br> -->
									<div class="mn_checklist">
										<label>Complete TPV</label>
										<input type="checkbox"  name="completeTPV" id="completeTPV" class="tpv-input" />
									</div><br>
								
								</div>
							</div>
						</div>
					</div>
					
					
				   	
				   	<div style="float:left;margin-top:10px;">
						<div class="qaerror"><span id="qaStatus_error${counter}" style="margin-left: 10px;"></span></div>
							<label class="qa-font" style="display: initial !important;float:left;">QA Status</label><span class="error_msg" style="float:left;">*</span>
							<select  name="qaStatus" id="qaStatus${counter}" style="border: 1px solid #ccc;border-radius: 3px;width:187px; margin-left:10px;"  >
				   	  				<option value="">--select--</option>	
				   	  				<option value="yes">PASS</option>
				   	  				<option value="no">FAIL</option>
				   	  		</select>
				   	</div>
				   	<div  style="float:right;margin-top:17px;">
				   	<div  class="qaerror"><span id="reportedBy_error${counter}" ></span></div>
							<label class="control-label" for="textfield" >Reported By<span class="error_msg">*</span></label>									    
							<input type="text" name="reportedBy"  class="input-xlarge qa-input" id="reportedBy${counter}" >
										
					</div>
				   	<div style="margin-top:91px;text-align:center;">
							<button type="submit"  id="qaFormSubmit" class="btn-update btn" style="width: 70px;height:30px;">Submit</button>
					</div>
					
				</form:form>
		    </div>
       </div>
	</div> 
</div>