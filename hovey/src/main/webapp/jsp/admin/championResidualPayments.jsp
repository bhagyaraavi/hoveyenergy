	<!-- Created By Bhagya On august 28th,2015
	Jsp for the Champion Residual Payments Details -->
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script>
$(".hasDatePicker").datepicker({
	showOn: "button",
	buttonImage: "../../hoveyenergy/images/calendar.gif",
	buttonImageOnly: true,
	onSelect: function(selected) {
          $(".hasDatePicker").datepicker("option", selected);
        }

});
/**
 * Function for updating the upfront commission and upfront paid date
 */
function updateUpfrontCommission(orderId,paymentId,month){
	 var upfrontCommission=$('#upfrontCommissionrcv'+paymentId).val();
		var upfrontPaidDate=$('#upfrontPaidDatercv'+paymentId).val();
		
	 $.ajax({
			url:"/hoveyenergy/admin/updateresidualpaymentsofchampion.do",
			data:	{orderId:orderId,month:month,upfrontCommission:upfrontCommission,upfrontPaidDate:upfrontPaidDate},							
			type: "GET",	
			success: function(data){
				alert(" Updated Succesfully For Month "+month);				
			},
			error: function(date){
			    alert('Error While Updating Data For Month '+month);
			 }
			 
	});		 
	
}

</script>
<div id="residualForm" class="residualForm">
   	<div id="content">
	  <div id="wrapper"> 
	  	<div class="data-input-form residual_champion-main-div" style="height:600px;overflow-y:scroll;width:701px;background: white;border: 5px solid rgb(140, 177, 231);">
			<div class="title">Residual Payments</div>
				<a href="" id="close_popup"  onclick="close_popup()"><img src="/hoveyenergy/images/close_icon1.png" height="28px;" width="28px;" style="float:right;margin-right: -75px;margin-top: -37px;"></a>
	  			<form:form class="form-horizontal1 form-validate1" method="POST" action="/hoveyenergy/admin/getresidualpaymentsofchampion.do" id="championResidualPayForm" commandName="championUpfrontCommissionsDto" onsubmit="return true">
	  				<input type="hidden" name="orderId" value="${orderId}"/>
	  					<div class="control-qa control-residual" >
							<label class="control-label-residual" for="textfield">Deal Start Date:</label>										    
							<input readonly type="text" name="dealStartDate"  id="dealStartDate" style="width:100px !important;" value="<fmt:formatDate value='${orderDto.dealStartDate}' pattern='MM/dd/yyyy'/>"/>
							
							<label class="control-label-residual left" for="textfield">Account Number:</label>										    
							<input readonly type="text" name="dealStartDate"  id="dealStartDate" style="width:200px !important;" value="${orderDto.accountNumber}"/>
							
							<label class="control-label-residual left" for="textfield">Term:</label>										    
							<input readonly type="text" name="dealStartDate"  id="dealStartDate" style="width:50px !important;" value="${orderDto.term}"/>
										
						</div>
	  					
	  					
	  					<div >
	  					
	  					<c:choose>
	  						<c:when test='${isHavingData eq true }'>
	  						
	  							<table id="new" style="border-collapse: collapse;width:100%" TABLE BORDER="2" CELLPADDING="1" CELLSPACING="1">
									<tr>
										<th>Month Number</th>
										<th>Upfront Commission Expected</th>
										<th>Upfront Paid Date Expected</th>
										<th>Upfront Commission Received</th>
										<th>Upfront Commission Received Date</th>
										<th>Options</th>				
									</tr>	
							
							<c:forEach var="residualPayments" items='${orderDto.championUpfrontCommissionsDtos}'>
								<tr>
									<td> <span id="month${residualPayments.id}">${residualPayments.installmentNumber}</span></td>
									<td><input type="text" readonly="readonly"  class="text-width left" name="upfrontCommissionexp${residualPayments.id}" id="upfrontCommissionexp${residualPayments.id}" value="<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${residualPayments.upfrontCommissionExpected}"/>"/></td>
									<td><input  type="text" readonly="readonly" class="text-width left " name="upfrontPaidDateexp${residualPayments.id}"  id="upfrontPaidDateexp${residualPayments.id}" value="<fmt:formatDate value='${residualPayments.upfrontCommissionExpectedDate}' pattern='MM/dd/yyyy'/>"/></td>
									<td><input type="text" class="text-width left" name="upfrontCommissionrcv${residualPayments.id}" id="upfrontCommissionrcv${residualPayments.id}" value="<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${residualPayments.upfrontCommissionReceived}"/>"/></td>
									<td><input  type="text" class="text-width left hasDatePicker" name="upfrontPaidDatercv${residualPayments.id}"  id="upfrontPaidDatercv${residualPayments.id}" value="<fmt:formatDate value='${residualPayments.upfrontCommissionReceivedDate}' pattern='MM/dd/yyyy'/>"/></td>
									 <td style="text-align:center;"><input type="button" value="Update" class=" btn-update btn"  onclick="updateUpfrontCommission(${orderDto.id},${residualPayments.id},${residualPayments.installmentNumber})"/> </td>
					  			</tr>
							</c:forEach>
							
					  		
							</table>
							
	  						</c:when>
	  						
	  						<c:otherwise>
	  								<div class="errorMsg">NO RESIDUAL PAYMENTS FOUND</div>
	  						</c:otherwise>
	  					</c:choose>
					
				</div>
	  					
				</form:form>
		    </div>
       </div>
	</div> 
</div>