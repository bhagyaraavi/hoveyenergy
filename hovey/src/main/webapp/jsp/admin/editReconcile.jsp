<!DOCTYPE html>
<html>
<head>

		 <link href="/hoveyenergy/css/common.css" rel="stylesheet" type="text/css">
		 <script src="https://code.jquery.com/jquery-latest.js"></script>
		<!-- <script type="text/javascript" src="/hoveyenergy/js/suppliers.js"></script>	 -->
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		  <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		  <link href="/hoveyenergy/css/championResidualPaymets.css" rel="stylesheet" type="text/css">
	<title>List Of All Commissions</title>
	 
	<script>
		
		
		$(document).ready(function() {
		    $("table tr:nth-child(even)").css('background-color', '#edeff0'); 
		    
		   $(".hasDatePicker").datepicker({
				showOn: "button",
				buttonImage: "/hoveyenergy/images/calendar.gif",
				buttonImageOnly: true,
				onSelect: function(selected) {
			          $(".hasDatePicker").datepicker("option", selected);
			        }
		   
		   });
		   
		   
		 //close popup
	  		$("#close_popup").click(function(){
	  			alert("inside close popup function");
	  			$("#residualForm").css('display','none');
	  			$("#residualCommissions").fadeOut(500);
	  		});
	  		
	  		
	  		  		
	  	  	//maintain the popup at center of the page when browser resized
	  	  	$(window).bind('resize',positionResidualPopup);
		    });  
		   
		   
			  /*  $('#editRecForm').submit(function(e){
				   e.preventDefault();
			         // update input value to have no commas
			         var val = $('input').val();
			         val = val.replace(/,/g, ' ');
			         $('input').val(val);
			         // let submit go through and submit
			         $(this).submit();
				}); */
			  		   
	  
		 
		function formatNumber(number){		
			number       = number.toString(); 
			var simpleNumber = '';  
			// Strips out the dollar sign and commas. 
			for (var i = 0; i < number.length; ++i) 
			{ 
				if ("0123456789.-".indexOf(number.charAt(i)) >= 0) 
					simpleNumber += number.charAt(i); 
			} 		 
			number = parseFloat(simpleNumber); 
			return number;
		}
		
		 function validateReconcileCommissions(){
			    var upCom1=$('#upCom1').val();
			    $('#upCom1').val(formatNumber($('#upCom1').val()));
		    	var upDate1=$('#upDate1').val();
		    	var upCom2=$('#upCom2').val();
		    	 $('#upCom2').val(formatNumber($('#upCom2').val()));
		    	var upDate2=$('#upDate2').val();
		    	var upCom3=$('#upCom3').val();
		    	 $('#upCom3').val(formatNumber($('#upCom3').val()));
		    	var upDate3=$('#upDate3').val();
		    	/* var upCom4=$('#upCom4').val();
		    	 $('#upCom4').val(formatNumber($('#upCom4').val()));
		    	var upDate4=$('#upDate4').val();
		    	var upCom5=$('#upCom5').val();
		    	alert("upfront commission 5 "+upCom5);
		    	 $('#upCom5').val(formatNumber($('#upCom5').val()));
		    	var upDate5=$('#upDate5').val(); */
		    	var i=true;
		    	var j=true;
		    	var k=true;
		    	var l=true;
		    	var m=true;
		    	var filter=/[^0-9.,]+/g;
		    	var filter1=/[^0-9.,]+/g;
		    	var filter2=/[^0-9.,]+/g;
		    	var filter3=/[^0-9.,]+/g;
		    	var filter4=/[^0-9.,]+/g;
		    	if(null!=upCom1 && upCom1!=0.0 && upCom1!=''){
		    		  if(filter.test(upCom1)){
		    			    i=false;
				    		$('#upCom1_error').html('*Should only have digits');
				    	  }
		    		    else if(null!=upDate1 && upDate1.trim()!=""){
		    			    i=true;
			    	  	    $('#upDate1_error').html('');	
			    	  	    $('#upCom1_error').html('');
		    			  }
		    	      else{
		    	    	  i=false;
			    	  	  $('#upDate1_error').html('*Cannot be empty');
		    		  	 }
		     	}
		    	if(null!=upCom2 && upCom2!=0.0 && upCom2!=''){
			    	  if(filter1.test(upCom2)){
			    			j=false;
					    	$('#upCom2_error').html('*Should only have digits');
					    	}
			    		 else if(null!=upDate2 && upDate2.trim()!=""){
			    			 j=true;
				    	  	  $('#upDate2_error').html('');	
				    	  	  $('#upCom2_error').html('');
			    			 }
			    	         else{
			    	    	    j=false;
				    	  	    $('#upDate2_error').html('*Cannot be empty');
			    		 	   }
			    	}
			   if(null!=upCom3 && upCom3!=0.0 && upCom3!=''){
				     if(filter2.test(upCom3)){
				    	k=false;
						$('#upCom3_error').html('*Should only have digits');
						}
				    else if(null!=upDate3 && upDate3.trim()!=""){
				    	 k=true;
					     $('#upDate3_error').html('');	
					     $('#upCom3_error').html('');
				    	}
				    	 else{
				    	    k=false;
					    	$('#upDate3_error').html('*Cannot be empty');
				    	 }  		 
			     }
			 /*   if(null!=upCom4 && upCom4!=0.0 && upCom4!=''){
				     if(filter3.test(upCom4)){
				    	l=false;
						$('#upCom4_error').html('*Should only have digits');
						}
				    else if(null!=upDate4 && upDate4.trim()!=""){
				    	 l=true;
					     $('#upDate4_error').html('');	
					     $('#upCom4_error').html('');
				    	}
				    	 else{
				    	    l=false;
					    	$('#upDate4_error').html('*Cannot be empty');
				    	 }  		 
			     }
			   if(null!=upCom5 && upCom5!=0.0 && upCom5!=''){
				     if(filter4.test(upCom5)){
				    	m=false;
						$('#upCom5_error').html('*Should only have digits');
						}
				    else if(null!=upDate5 && upDate5.trim()!=""){
				    	 m=true;
					     $('#upDate5_error').html('');	
					     $('#upCom5_error').html('');
				    	}
				    	 else{
				    	    m=false;
					    	$('#upDate5_error').html('*Cannot be empty');
				    	 }  		 
			     } */
		    	 
		    	if(i==true && j==true && k==true){
		    			return true;
		    		}
		    	else{
		    		  return false;
		    		}
		    }
		
		 /**
		 	Function for the popup of residual Payments deatils for the champion supplier
		 */
		 function residualCommissionsPopup(orderId){
			 
			 var $body=$('body');
				$body.addClass('loading'); 
				 $.ajax({
						url:"/hoveyenergy/admin/getresidualpaymentsofchampion.do",
						data:	{orderId:orderId},							
						type: "GET",	
						 beforeSend: function(){
							   $body.addClass('loading');
					        },
						success: function(data){
							$("#residualCommissions").append(data);
							$("#residualCommissions").fadeIn(1000);
							positionResidualPopup();
							
							
						},
						
						complete: function(){
						   $body.removeClass('loading');
				        } 
				});					
		 }
		 
		 
		//position the popup at the center of the page
			function positionResidualPopup(){
			  if(!$("#residualCommissions").is(':visible')){
			    return;
			  } 
			  $("#residualCommissions").css({
			      left: ($(window).width() - $('#residualCommissions').width()) / 2,
			      top: ($(window).width() - $('#residualCommissions').width()) / 21,
			      position:'absolute'
			  });
			}
		
			/* function closeQAForm(id){
				$("#qaForm").css('display','none');
			} */

	</script>
	
	<style>
		input[type="text"]{
				text-transform: none !important;
		}
	

	</style>
	<style>
	#residualCommissions{
				position: absolute;
				/* border: 5px solid rgb(140, 177, 231);*/
				padding: 10px;
				/* background: white;  */
				width: 701px;
				height: auto;
				border-radius: 33px;		   
			    behavior: url(/hoveyenergy/js/js/PIE.htc);
				z-index: 9999;
			}
		#loader {
			        width: 100%;
			        height: 100%;
			        position: fixed;
			        top: 0;
			        left:0;
			        z-index: -1;
			        display: none;
			    }
			
			    #loader img {
			    	position: fixed;  
			    	top: 50%;
			    	 left: 50%;
			    }
			
			    .loading #loader {
			    	z-index: 1001;background:  rgba(54, 25, 25, .5); display
			    	: inline;
			    }
	 </style>
</head>


<body class="firefox" >
   <%@ include file="header.jsp" %>
	<div id="content">
	
	  <div id="feedback2" style="text-align: center;display:left:0px;color:green;font-size: 20px;font-weight: bold;">
	  	${message}
	  
	  </div>
	  <br />
		
		<!-- <div class="title">Edit Customer</div> -->
		<br />
		
	  <div id="wrapper">
	  	
		<div class="data-input-form">
			<div class="sub-title" style="">
	  		<h1>List Of All Commissions</h1>
	  			<!-- Added Residual Payments By bhagya On august 25th, 2016 Because for champion paymnets are residual(monthly) payments -->
	  			<c:if test="${order.supplierName.supplierName eq 'Champion Energy'}">
	  				<a href="#" class="btn btn-success" style="margin-left:34%;margin-top:20px;"
						onclick="residualCommissionsPopup(${order.id})">Residual Payments</a>
				</c:if>
	  		</div>
	  		
			<form:form id="editRecForm" class="form-horizontal1 form-validate1" method="POST" action="/hoveyenergy/admin/editreconcile.do"  onsubmit="return validateReconcileCommissions();">
								<input type="hidden" name="id" value="${order.id}"/>
								<input type="hidden" name="customerId" value="${order.taxId.customerId}"/>
			
									<div class="control-group" style="margin-top: 60px;height: 13px;">
										<label class="control-label" for="textfield">Account Number:<span class="error_msg">*</span></label>										    
											<input name="accountNumber" class="input-xlarge" id="acc" value="${order.accountNumber}" readonly="readonly" type="text" />
												
																				
									</div>
									<div class="control-group">
										<label class="control-label" for="emailfield">Business Name:<span class="error_msg">*</span></label>
										
											<input type="text" name="businessName"  class="input-xlarge" id="bname" value="${order.businessName}" readonly="readonly" type="text">
											<span class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="cust_error"  ></span>
										
									</div>
									
									<div class="control-group">
										<label class="control-label" for="emailfield">Term<span class="error_msg">*</span></label>
										
											<input type="text" name="term"  class="input-xlarge" id="term" value="${order.term}" readonly="readonly" type="text" >
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;"  id="acc_error"></span>
										
									</div>
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontCommission1</label>
										
											<input type="text" name="upfrontCommission"  class="input-xlarge" id="upCom1" value='<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${order.upfrontCommission}"/>' >
											
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upCom1_error"></span>										
									</div>
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontPaidDate1</label>
										<input type="text" name="upfrontPaidDate"  class="hasDatePicker" id="upDate1" value='<fmt:formatDate  value="${order.upfrontPaidDate}" pattern="MM/dd/yyyy"/>'>
											
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upDate1_error"></span>
										
									</div>
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontCommission2</label>
										
											<input type="text" name="upfrontCommission2"  class="input-xlarge" id="upCom2" value='<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${order.upfrontCommission2}"/>'>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upCom2_error"></span>
										
									</div>
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontPaidDate2</label>
										
											<input type="text" name="upfrontPaidDate2"  class="hasDatePicker" id="upDate2" value='<fmt:formatDate  value="${order.upfrontPaidDate2}" pattern="MM/dd/yyyy"/>' />
											<span class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upDate2_error"></span>
										
									</div>
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontCommission3</label>										
											<input type="text" name="upfrontCommission3"  class="input-xlarge" id="upCom3" value='<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${order.upfrontCommission3}"/>'>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upCom3_error"></span>										
									</div>
									
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontPaidDate3</label>										
											<input type="text" name="upfrontPaidDate3"  class="hasDatePicker" id="upDate3" value='<fmt:formatDate  value="${order.upfrontPaidDate3}" pattern="MM/dd/yyyy"/>'/>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upDate3_error"></span>										
									</div>	
									<%-- <div class="control-group">
										<label class="control-label" for="emailfield">UpfrontCommission4</label>										
											<input type="text" name="upfrontCommission4"  class="input-xlarge" id="upCom4" value='<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${order.upfrontCommission4}"/>'>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upCom4_error"></span>										
									</div>
									
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontPaidDate4</label>										
											<input type="text" name="upfrontPaidDate4"  class="hasDatePicker" id="upDate4" value='<fmt:formatDate  value="${order.upfrontPaidDate4}" pattern="MM/dd/yyyy"/>'/>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upDate4_error"></span>										
									</div>	
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontCommission5</label>										
											<input type="text" name="upfrontCommission5"  class="input-xlarge" id="upCom5" value='<fmt:formatNumber  currencyCode="US" currencySymbol="$" maxFractionDigits="2" minFractionDigits="2" value="${order.upfrontCommission5}"/>'>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upCom5_error"></span>										
									</div>
									
									
									<div class="control-group">
										<label class="control-label" for="emailfield">UpfrontPaidDate5</label>										
											<input type="text" name="upfrontPaidDate5"  class="hasDatePicker" id="upDate5" value='<fmt:formatDate  value="${order.upfrontPaidDate5}" pattern="MM/dd/yyyy"/>'/>
											<span  class="help-block error" style="margin-top: 0px;margin-bottom: -20px;height: 25px;" id="upDate5_error"></span>										
									</div>			 --%>
									
									<div style="text-align: center;margin-bottom: 20px;clear: both;">									
										<button type="submit"  class="btn-update btn"  style="width: 100px;">Update</button>										
										<a href="/hoveyenergy/admin/editreconcile.do?orderId=${order.id}" class="btn btn-undo"  style="margin-left: 10px;">Undo</a>
										<a href="/hoveyenergy/admin/reconcile.do?customerId=${order.taxId.customerId}"  class="btn btn-back"  style="margin-left: 10px;">Back</a>
									</div>
				</form:form>
		       
          
                <div id="residualCommissions" style="display:none;"></div>
                
                
            </div>
                     
		</div>
	</div>
   <div id="loader" >
  			<img src="/hoveyenergy/images/loader.GIF" />
  		</div>
 </body>
</html>