<!DOCTYPE html>
<html>
<head>
	<title>Agent Deal Sheets</title>
	 <script src="https://code.jquery.com/jquery-latest.js"></script>
	 <link rel="stylesheet"
	href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="/hoveyenergy/js/filterorder.js"></script>
	  <link href="/hoveyenergy/css/common.css" rel="stylesheet" type="text/css">
	  <link href="/hoveyenergy/css/user_login.css" rel="stylesheet"
	type="text/css" />
	<script>
	$(document).ready(function() {
	    $("table tr:nth-child(even)").css('background-color', '#edeff0');




	    $(".datepicker")
		.datepicker(
				{
					changeMonth : true,
					changeYear : true,
					showButtonPanel : true,
					closeText : 'Clear',
					dateFormat : 'mm/dd/yy',
					yearRange : '2012:c+10',
					onSelect : function(dateText, inst) {
						date = 0;
						date = $(this).datepicker('getDate').getDate();
					},
					onClose : function(dateText, inst) {
						var month = $(
								"#ui-datepicker-div .ui-datepicker-month :selected")
								.val();
						var year = $(
								"#ui-datepicker-div .ui-datepicker-year :selected")
								.val();
						if (date > 0) {
							$(this).datepicker('setDate',
									new Date(year, month, date));
						} else {
							$(this).val('');
							$(this).datepicker('setDate', '');
						}
						date = 0;
						editpipeline(this);
					}

				
				});

		
	  //open popup
		$("#pop").click(function() {
			$("#overlay_form").fadeIn(1000);
			positionPopup();
		});

		//close popup
		$("#close").click(function() {
			$("#overlay_form").fadeOut(500);
		});
	});
	
	
	function deleteDeal(id){
		var x=confirm("Are you sure you want to Delete?");					
		if(x==true ){	
			window.location='/hoveyenergy/admin/deletedealsheet.do?dealId='+id;
		}
	}

	 /* Added by bhagay on june 25th,2015
	 Function for updating the QA Status for deals */
	 
	function editQAReady(dealId){
		var qaDealStatus;
		var id=jQuery('#'+dealId).attr('data_index'); 
		if($('#qaDealStatus'+id).is(':checked')){
			qaDealStatus=true;
		}
		else{
			qaDealStatus=false;
		}
	
		window.location='/hoveyenergy/admin/editdealbyqastatus.do?dealId='+dealId+"&qaDealStatus="+qaDealStatus;
			
	}
		

	//position the popup at the center of the page
	
	function positionPopup() {
		if (!$("#overlay_form").is(':visible')) {
			return;
		}
		$("#overlay_form").css({
			left : ($(window).width() - $('#overlay_form').width()) / 2,
			top : ($(window).width() - $('#overlay_form').width()) / 21,
			position : 'absolute'
		});
	}

	//maintain the popup at center of the page when browser resized
	$(window).bind('resize', positionPopup);

	function showPopup(id) {
		var selected = $('#' + id).find('option:selected');
		var status = selected.val();
		if (status == "rescinded") {

			alert("Please Enter a Reason in the Notes");
		}
	}
	
</script>
	<style>
	
		#overlay_form {
			position: absolute;
			border: 5px solid rgb(140, 177, 231);
			padding: 10px;
			background: white;
			width: 700px;
			height: auto;
			border-radius: 33px;
			behavior: url(/hoveyenergy/js/js/PIE.htc);
			z-index: 9999;
		}
		#pop {
			font-size: 15px;
			float: left;
			color: blue;
			text-decoration: underline;
		}
		.multisearcherror {
			color: red;
			font-size: 10px;
			height: 10px;
			margin-left: 32px;
		}
	</style>
	
	
</head>



<body class="firefox">
   <%@ include file="header.jsp" %>
    <c:set var="queryString" value="${requestScope['javax.servlet.forward.query_string']}" />
	 <c:set var="delimeter" value="&page=${page }" />
	<c:set var="query" value="${fn:replace(queryString,delimeter,'')}" />
	<c:set var="delimeter2" value="&sortby=${sortby}" />
	<c:set var="query2" value="${fn:replace(queryString,'&','')}" />
	<c:set var="query2" value="${fn:replace(queryString,delimeter2,'')}" />
   
   
	<div id="content">
	<div id="feedback2" style="text-align: center;color:green;font-size: 20px;font-weight: bold;height: 16px;margin-top: 10px;">
	  		<c:if test="${not empty param.message }">
	  					Deal Edited Successfully
	  		</c:if>
	  		
	  		<c:if test="${not empty param.deleteStatus }">
	  					Deal Deleted Successfully
	  		</c:if>
	  		<c:if test="${not empty param.qastatus }">
	  					Deal Updated Successfully To QA
	  		</c:if>
	 		<c:if test="${not empty message }">
	  					<div>${message}</div>
	  		</c:if>
	    </div>
		
	
		
		<!--  Advanced  Search UI, Added By Bhagya On sep 21st,2015 as per client requirement  -->
		<div id="fancybox-content"
			style="border-width: 10px; width: auto; height: auto;">

			<div id="main_container" class="container">
				<div class="cb">
					<div class="inner-page">

 						<form:form method="get" action="/hoveyenergy/admin/viewadvancedsearchdealsheets.do" commandName="dealSearch"
							id="overlay_form" style="display:none"
							onsubmit="return validateMultiSearch()">
							<div class="login-area">
								<div class="title">Advanced Search</div>
								<div class="lab_top" style="margin-top: -5px;">
									<div id="ordererror" class="multisearcherror"></div>
									<div class="fc-text">Order Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="startDate" id="startDate" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="endDate" id="endDate" />
									</div>
								</div>

								<div class="lab_top">
									<div id="dealerror" class="multisearcherror"></div>
									<div class="fc-text">Deal Start Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="dealStartDate1" id="dealStartDate1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="dealStartDate2" id="dealStartDate2" />
									</div>
								</div>

								<div class="lab_top">
									<div id="suppliererror" class="multisearcherror"></div>
									<div class="fc-text">Sent To Supplier Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="sentToSupplier1"
											id="sentToSupplier1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="sentToSupplier2"
											id="sentToSupplier2" />
									</div>
								</div>

								<div class="lab_top">
									<div id="upfronterror" class="multisearcherror"></div>
									<div class="fc-text">Upfront Paid Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="upfrontPaidDate1"
											id="upfrontPaidDate1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="upfrontPaidDate2"
											id="upfrontPaidDate2" />
									</div>
								</div>

								<div class="lab_top1">
									<div id="kwherror" class="multisearcherror"></div>
									<div class="fc-text">Kwh:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="kwh1" id="kwh1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="kwh2" id="kwh2" />
									</div>
								</div>

								<div class="lab_top">
									<div id="comerror" class="multisearcherror"></div>
									<div class="fc-text">Commission:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="commission1" id="commission1" />
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="commission2" id="commission2" />
									</div>
								</div>

								<div class="lab_top">
									<div id="termerror" class="multisearcherror"></div>
									<div class="fc-text">Term:</div>
									<div class="fc-text1">
										From<input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px; margin-left: 3px;"
											name="term1" id="term1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="term2" id="term2" />
									</div>
								</div>
								<div class="lab_top">
									<div id="installmenterror" class="multisearcherror"></div>
									<div class="fc-text">Installment Number:</div>
									<div class="fc-text1">
										From<input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px; margin-left: 3px;"
											name="installmentNumber1" id="installmentNumber1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											name="installmentNumber2" id="installmentNumber2" />
									</div>
								</div>
								<div class="lab_top">
									<div id="residualdateerror" class="multisearcherror"></div>
									<div class="fc-text">Residual Payment Expected Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="residualPaymentExpDate1"
											id="residualPaymentExpDate1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="residualPaymentExpDate2"
											id="residualPaymentExpDate2" />
									</div>
								</div>
								<div class="lab_top">
									<div id="residualpaiddateerror" class="multisearcherror"></div>
									<div class="fc-text">Residual Payment Paid Date:</div>
									<div class="fc-text1">
										From <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="residualPaymentPaidDate1"
											id="residualPaymentPaidDate1" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp; To <input type="text"
											style="border: 1px solid #ccc; height: 15px; width: 125px;"
											class="datepicker" name="residualPaymentPaidDate2"
											id="residualPaymentPaidDate2" />
									</div>
								</div>
								<div class="lab_top1" style="margin-top: 25px;">
									<div class="fc-text">Account #:</div>
									<div class="fc-text1">
										<input type="text"
											style="border: 1px solid #ccc; margin-left: 33px; height: 15px; width: 250px;"
											name="accountNumber" />
									</div>

									<div class="fc-text">Notes:</div>
									<div class="fc-text1">
										<input type="text"
											style="border: 1px solid #ccc; margin-left: 33px; height: 15px; width: 250px;"
											name="notes" />
									</div>
									<!-- Added by bhagya on June 11 ,2019 For TPV filter ,as per client requirement-->
										
										<div class="fc-text">TPV:</div>
									<div class="fc-text1">
										<input type="text"
											style="border: 1px solid #ccc; margin-left: 33px; height: 15px; width: 250px;"
											name="tpv" id="tpv"/>
											<span id="tpverror" style="color:red"></span>
									</div>
									<c:if test="${empty rescinded }">
										<div class="fc-text">Upfront Commission Status:</div>
										<div class="fc-text1">
											<select
												style="border: 1px solid #ccc; margin-left: 33px; height: 27px; width: 252px; margin-top: 5px; border-radius: 3px;"
												name="commissionStatus">
												<option value="">- select -</option>
												<option value="paid">PAID</option>
												<option value="unpaid">NOT PAID</option>
												<option value="equal">Equal to Expected Commission</option>
												<option value="less">Less Than Expected Commission</option>
												<option value="greater">Greater Than Expected
													Commission</option>
											</select> <span style="color: red">(* Only for First year
												Orders)</span>
										</div>
									</c:if>
									<!-- Modiified by Jeevan on July 18,2013 to add States and Utility for Dynamic Search -->
									<div class="fc-text" style="margin-top: 33px;">Supplier
										Name:</div>
									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; height: 25px; width: 125px; margin-top: 30px; float: left;"
											name="supplierName">
											<option value="">- select -</option>
											<c:forEach items="${suppliers }" var="supplier">
												<option value="${supplier.supplierName }">${supplier.supplierName }</option>
											</c:forEach>
										</select>

										<div class="fc-text"
											style="text-align: left; margin-left: 50px; margin-top: 33px; width: 60px;">Utility:</div>
										<select
											style="border: 1px solid #ccc; margin-left: 33px; height: 60px; width: 125px; margin-top: 27px; float: left"
											name="utility" multiple="multiple">
											<option value="">- select -</option>
											<c:forEach var="util" items="${utils }">
												<option value="${util.utility }">${util.utility }</option>
											</c:forEach>
										</select>

									</div>

									<div class="fc-text" style="margin-top: 15px;">Contract
										Type:</div>
									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; margin-top: 11px; height: 25px; width: 125px; float: left;"
											name="contractType">
											<option value="">- select -</option>
											<c:forEach var="contractTypes" items="${contractTypes}">
												<option value="${contractTypes.contractType }"
													${order.contractType eq contractTypes.contractType ? 'selected' : ''  }>
													${contractTypes.contractType}</option>
											</c:forEach>
										</select>

										<div class="fc-text"
											style="text-align: left; margin-left: 50px; margin-top: 15px; width: 60px;">State:</div>
										<select
											style="border: 1px solid #ccc; margin-left: 33px; height: 25px; width: 125px; margin-top: 11px; float: left;"
											name="state">
											<option value="">- select -</option>
											<c:forEach var="state" items="${states }">
												<option value="${state.state }">${state.state }</option>
											</c:forEach>
										</select>

									</div>

									<!-- Modiified by Jeevan on July 18,2013 to add States and Utility for Dynamic Search -->

									<div class="fc-text" style="margin-top: 15px; clear: both">Agent
										Name:</div>
									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; margin-top: 11px; height: 25px; width: 125px; float: left;"
											name="agentName">
											<option value="">- select -</option>
											<c:forEach var="agent" items="${agents }">
												<option value="${agent.agentNumber}">${agent.firstName}
													${agent.lastName}</option>
											</c:forEach>
										</select>
										<!-- Added by bhagya on December 11,2014 For service type filter ,as per client requirement-->
										<div class="fc-text"
											style="text-align: left; margin-left: 50px; margin-top: 15px; width: 60px;">SVC
											Type:</div>
										<select
											style="border: 1px solid #ccc; margin-left: 33px; height: 25px; width: 125px; margin-top: 11px; float: left;"
											name="serviceType">
											<option value="">- Select -</option>
											<option value="commercial"
												${serviceType eq 'commercial' ? 'selected' : ''}>Commercial</option>
											<option value="residential"
												${serviceType eq 'residential' ? 'selected' : ''}>Residential</option>
										</select>

									</div>
									<!-- Modiified by bhagya on April 30,2014 to add  Rescinded Order agent for Dynamic Search -->

									<div class="fc-text" style="margin-top: 15px; clear: both">Rescinded
										Order Agent:</div>
									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; margin-top: 11px; height: 25px; width: 125px; float: left;"
											name="resAgentName">
											<option value="">- select -</option>
											<c:forEach var="agent" items="${agents }">
												<option value="${agent.agentNumber}">${agent.firstName}
													${agent.lastName}</option>
											</c:forEach>
										</select>
									</div>

									<div class="fc-text" style="margin-top: 15px; clear: both">Business
										Name:</div>
									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; margin-top: 11px; height: 25px; width: 225px; float: left;"
											name="businessName">
											<option value="">- select -</option>
											<c:forEach var="businessName" items="${businessNames}">
												<option value="${businessName}">${businessName}</option>

											</c:forEach>
										</select>
									</div>



									<div class="fc-text" style="margin-top: 15px; clear: both">Status:</div>

									<div class="fc-text1">
										<select
											style="border: 1px solid #ccc; margin-left: 33px; margin-top: 11px; height: 50px; width: 125px; float: left;"
											name="status" multiple="multiple">
											<option value="">- select -</option>
											<option value="agent">Agent</option>
											<option value="under review">Under Review</option>
											<option value="approved">Approved</option>
											<c:if test="${empty rescinded }">
												<option value="rescinded">Rescinded</option>
											</c:if>
										</select>

										<div style="margin-top: 20px; color: red; margin-left: 10px;">
											* ctrl+click to select multiple status</div>
									</div>
								</div>



								<div class="su-menu">

									<span class="button"> <input name="submit"
										value="Search" id="submit_button" class="button80"
										style="height: 33px; width: 133px" tabindex="10" type="submit" />
									</span>
								</div>

								<div class="login-desc">
									<a href="#" id="close" style="color: #4B4D50;">Cancel</a>
								</div>

							</div>
						</form:form>
					</div>
				</div>

			</div>
		</div>


		<!-- Advanced  Search UI  -->
		
		
		
		<div class="title" style="padding-top: 15px;margin-bottom: -25px;">Saved Deal Sheets</div>
		<br />
		
		<div class="searchBox" >
		 				<form method="get" action="/hoveyenergy/admin/viewdealsheets.do" >
			 				 <div style="display: inline-block;">
				 				  <div style="display: block;float: left;">
				 					<label>Business Name:</label>
				 					<input type="text" name="searchBy" class="searchText"/>
				 				  </div>
			 					<div style="display: block;float: right;">
			 						<button type="submit" class="searchBtn" style="top:0px;" ></button>	
			 				  	</div>	
			 				</div>	 				
		 				</form>
		
		</div>
		<!-- Added Advanced Search By Bhagya On Sep 21st,2015 -->
		<div style="clear: left; float: right; width: 28%; margin: -38px auto;">
					<a href="#" id="pop">Advanced Search</a> 
					<a href="./viewdealsheets.do"
						style="font-size: 15px; color: blue; margin-left: 30px; float: left; text-decoration: underline;">Reset</a>
		</div>
		<c:if test="${ empty message }">
				<div>
				<div class="pagecount" style="float: left;margin-left: 20px;">
														 Page <b>${page +1}</b> of <b>${end }</b>
												</div>
					<ul class="pager" >
					    <div style="float: right;text-align: right;">
					      <c:choose>
					      	<c:when test="${page eq 0 && end ne 1 }">
					      		<li>	 <a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page+1}"><img src="/hoveyenergy/images/next.png" /></a></li>
					      	</c:when>
					      
					        <c:when test="${page eq end-1 && end ne 1 }">
					                <a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page-1}"><img src="/hoveyenergy/images/prev.png" /></a></li>
					        </c:when>
					        
					        <c:when test="${page eq end-1 && end eq 1 }">
					               
					        </c:when>
					        
					        <c:otherwise>
					        	<li>	<a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page-1}"><img src="/hoveyenergy/images/prev.png" /></a></li>
								<li> <a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page+1}"><img src="/hoveyenergy/images/next.png" /></a></li>
					        
					        </c:otherwise>
					      </c:choose>
					    </div>
					    <div class="pagecount"> Showing ${first}- ${last} of ${total } Deals	</div>
					</ul>
				</div>
				<div class="CSSTableGenerator" >
		                <table >
		                    <tr>
		                    	<td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=orderDate" >Order Date</a></td>
		                       <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=dealStartDate">Deal Start Date</a></td>
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=businessName">Business Name</a></td>
						      
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=agent">Agent Id</a></td>
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=agentName">Agent Name</a></td>
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=taxId.customerId">Tax Id</a></td>	
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=order_id">No of Orders</a></td>
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=kwh">Total kWh</a></td>
						        
								<!--  Added Fronter  By Bhagya On November 16th,2015..based on client requirement -->
				 
						        <td><a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&sortby=fronter">File Fronter</a></td>   
						          <!--  Added QA Ready by bhagya on june 25th,2015 for admin can allow the deals to qa -->
						        <td>QA Ready</td>			      
						    	<td>Edit</td>
						    	<td>Delete</td>
						    	<td>Print</td>
						    	
		                    </tr>
		                    <c:forEach var="order" items="${orders}">
		                    <input type="hidden" name="orderId" value="${order.id}" />
					   		 <tr>
					   		 	
					   		 	  <td><fmt:formatDate pattern="MM/dd/yyyy" value="${order.orderDate}" /></td>
						         <td><fmt:formatDate pattern="MM/dd/yyyy" value="${order.dealStartDate}" /></td>
						         <td>${order.businessName}</td>
						          <td>${order.createdAgent.agentNumber }
						           <td>${order.createdAgent.firstName } ${order.createdAgent.lastName }</td>
						         <td>${order.taxId.taxId}</td>
						         <td>${order.totalAccounts }</td>
						         <td><fmt:formatNumber groupingUsed="true" value="${order.totalKwh}" /> </td>
						         <!--  Added Fronter  By Bhagya On November 16th,2015..based on client requirement -->
						          <td>${order.taxId.fronter}</td>
						         <!--  Added QA Ready by bhagya on june 25th,2015 for admin can allow the deals to qa -->
						         <td align="center" bordercolor="#666666" >
                                     <c:choose>
                                         <c:when test="${order.transDto.qaDealStatus eq true }">
                                            <input type="checkbox" name="qaDealStatus" checked="checked"  id="qaDealStatus${order.id}" data_index="${order.id}"/>
                                            	<button class="btn-update btn"  id="${order.transDto.transactionId }" type="button" onclick="editQAReady(this.id)" data_index="${order.id}" >Update</button>
                                         </c:when>
                                         <c:otherwise>
                                         <input type="hidden" name="qaPartialReady" value="${order.isQAPartialReady}">
                                            <input type="checkbox"  name="qaDealStatus" id="qaDealStatus${order.id}" value="0"   data_index="${order.id}"/>
                                            	<button class="btn-update btn"  id="${order.transDto.transactionId }" type="button" onclick="editQAReady(this.id)"  data_index="${order.id}">Update</button><br/><br/>
                                            <c:if test="${order.isQAPartialReady eq true}">
                                            	<span style="background-color: #CDB405;padding: 3px;color: #932222;">Partially QA Ready</span>
                                            </c:if>	
                                          </c:otherwise>
                                      </c:choose>
                                                
                                </td>
						       <td><a href="/hoveyenergy/admin/editdealsheet.do?dealId=${order.transDto.transactionId }"><img src="/hoveyenergy/images/edit_deal.png" title="Edit Deal Sheet" class="icon"/></a></td>
						        
						        <td><a href="#"   id="${order.transDto.transactionId }" onClick="deleteDeal(this.id);"><img src="/hoveyenergy/images/delete.png" title="Delete Deal Sheet" class="icon"/></a></td> 
						          <td>
						         	<a href="#" onClick="window.open('/hoveyenergy/printdeal.do?dealId=${order.transDto.transactionId}','mywindow','width=1280px,height=800px,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes').print()">
												<img src="/hoveyenergy/images/print_deal.png" title="Print Deal Sheet" class="icon"/>
								   </a>
								 </td>								
					   		 </tr>
				   		 </c:forEach>
		                </table>
		                
		               
		            </div>
		             
		            
		           <div style="text-align: center;width: 620px;margin: 40px auto;">									
													<ul id="pagin">
													 <!--  For Previous -->	
													 
													 	
														<c:if test="${page ne 0 }">
														<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=0">First</a></li>
															<li>  <a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page-1}">Prev</a></li>
														</c:if>
													
														<c:choose>
														<c:when test="${end eq 1 }"></c:when>
																<c:when test="${end lt 10 }">
																	<c:forEach var="i" begin="0" end="${end-1 }">
																		<c:choose>
																			<c:when test="${page eq i }">
																				<li><a class="current" href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:when>
																			<c:otherwise>
																				<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:otherwise>
																		</c:choose>																	
																	</c:forEach>																
																</c:when>
																
																<c:when test="${ page  lt 5}">
																	<c:forEach var="i" begin="0" end="9">
																		<c:choose>
																			<c:when test="${page eq i }">
																				<li><a class="current" href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:when>
																			<c:otherwise>
																				<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:otherwise>
																		</c:choose>																	
																	</c:forEach>																
																</c:when>
																
																
																<c:when test="${page ge 5 && page+5 le end }">
																		<c:forEach var="i" begin="${page-5 }" end="${page+4 }">
																		<c:choose>
																			<c:when test="${page eq i }">
																				<li><a class="current" href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:when>
																			<c:otherwise>
																				<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:otherwise>
																		</c:choose>																	
																	</c:forEach>												
																</c:when>
																
																<c:otherwise>
																	<c:forEach var="i" begin="${end-10 }" end="${end-1 }">
																		<c:choose>
																			<c:when test="${page eq i }">
																				<li><a class="current" href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:when>
																			<c:otherwise>
																				<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${i}">${i+1 }</a></li>
																			</c:otherwise>
																		</c:choose>																	
																	</c:forEach>															
																</c:otherwise>								
														</c:choose>									
													 <!-- For Next -->
													  
													   <c:if test="${page lt end-1 }">
															<li>  <a href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${page+1}">Next</a></li>
														</c:if>
														<c:if test="${page ne end-1 }">
													  		<li><a  href="/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query}&page=${end-1}">Last</a></li>
														</c:if>
														
														
													</ul>					
												</div> 
		            
		            							<p style="clear:both;">
		            							</p>
		            						<br />
		            
		            
		            
		            
       </c:if>
       <!-- Added Export To Excel By Bhagya On Sep 30th,2015,as per client requirement -->
		 <div style="text-align: center; clear: both; margin-top: 55px;">
			<a href="#" class="btn btn-back" style="width: 180px;"
				onclick="window.location.href='/hoveyenergy${requestScope['javax.servlet.forward.servlet_path']}?${query2}&output=excel'">Export
				to Excel</a> 
		</div>
	
	</div>
	
	<c:if test="${not empty param.error }">
		<script type="text/javascript">
			alert("Problem While Editing Deal Sheet, Account# should be Unique among Orders.. Please Try Again..");
		</script>
	</c:if>
   
 </body>
</html>