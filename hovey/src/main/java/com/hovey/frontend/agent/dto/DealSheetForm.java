package com.hovey.frontend.agent.dto;

import java.util.Date;

import org.springframework.util.AutoPopulatingList;

/**
 * 
 * @author JEEVAN
 *
 */

//for Binding dealSheet.jsp
public class DealSheetForm {
	private CustomerDto customer=new CustomerDto();
   private AutoPopulatingList<OrdersDto> orders =new AutoPopulatingList<OrdersDto>(OrdersDto.class);
   private Date orderDate;
	public AutoPopulatingList<OrdersDto> getOrders() {
	return orders;
}
public void setOrders(AutoPopulatingList<OrdersDto> orders) {
	this.orders = orders;
}
	public CustomerDto getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	
	
	

}
