package com.hovey.frontend.agent.dto;

import java.util.Date;

/**
 * Created By bhagya On August 18th, 2016
 * 
 * Dto class For Champion Upfront commissions
 * 
 * If the deal months or term  greater than 24 means, those upfront commissions will save in this table.
 * If it is less than or equal to 1 means, we are saving that commission as upfrontcommission1 in orders table.
 */
public class ChampionUpfrontCommissionsDto{
	private Integer id;	
	private Integer orderId;
	private Integer installmentNumber;
	private Double upfrontCommissionExpected;
	private Date upfrontCommissionExpectedDate;
	private Double upfrontCommissionReceived;
	private Date upfrontCommissionReceivedDate;
	
	private Double upfrontCommission12;
	private Date upfrontPaidDate12;
	private Double upfrontCommission13;
	private Date upfrontPaidDate13;
	private Double upfrontCommission14;
	private Date upfrontPaidDate14;
	private Double upfrontCommission15;
	private Date upfrontPaidDate15;
	private Double upfrontCommission16;
	private Date upfrontPaidDate16;
	private Double upfrontCommission17;
	private Date upfrontPaidDate17;
	private Double upfrontCommission18;
	private Date upfrontPaidDate18;
	private Double upfrontCommission19;
	private Date upfrontPaidDate19;
	private Double upfrontCommission20;
	private Date upfrontPaidDate20;
	private Double upfrontCommission21;
	private Date upfrontPaidDate21;
	private Double upfrontCommission22;
	private Date upfrontPaidDate22;
	private Double upfrontCommission23;
	private Date upfrontPaidDate23;
	private Double upfrontCommission24;
	private Date upfrontPaidDate24;
	private Double upfrontCommission25;
	private Date upfrontPaidDate25;
	private Double upfrontCommission26;
	private Date upfrontPaidDate26;
	private Double upfrontCommission27;
	private Date upfrontPaidDate27;
	private Double upfrontCommission28;
	private Date upfrontPaidDate28;
	private Double upfrontCommission29;
	private Date upfrontPaidDate29;
	private Double upfrontCommission30;
	private Date upfrontPaidDate30;
	private Double upfrontCommission31;
	private Date upfrontPaidDate31;
	private Double upfrontCommission32;
	private Date upfrontPaidDate32;
	private Double upfrontCommission33;
	private Date upfrontPaidDate33;
	private Double upfrontCommission34;
	private Date upfrontPaidDate34;
	private Double upfrontCommission35;
	private Date upfrontPaidDate35;
	private Double upfrontCommission36;
	private Date upfrontPaidDate36;
	private Double upfrontCommission37;
	private Date upfrontPaidDate37;
	private Double upfrontCommission38;
	private Date upfrontPaidDate38;
	private Double upfrontCommission39;
	private Date upfrontPaidDate39;
	private Double upfrontCommission40;
	private Date upfrontPaidDate40;
	private Double upfrontCommission41;
	private Date upfrontPaidDate41;
	private Double upfrontCommission42;
	private Date upfrontPaidDate42;
	private Double upfrontCommission43;
	private Date upfrontPaidDate43;
	private Double upfrontCommission44;
	private Date upfrontPaidDate44;
	private Double upfrontCommission45;
	private Date upfrontPaidDate45;
	private Double upfrontCommission46;
	private Date upfrontPaidDate46;
	private Double upfrontCommission47;
	private Date upfrontPaidDate47;
	private Double upfrontCommission48;
	private Date upfrontPaidDate48;
	private Double upfrontCommission49;
	private Date upfrontPaidDate49;
	private Double upfrontCommission50;
	private Date upfrontPaidDate50;
	private Double upfrontCommission51;
	private Date upfrontPaidDate51;
	private Double upfrontCommission52;
	private Date upfrontPaidDate52;
	private Double upfrontCommission53;
	private Date upfrontPaidDate53;
	private Double upfrontCommission54;
	private Date upfrontPaidDate54;
	private Double upfrontCommission55;
	private Date upfrontPaidDate55;
	private Double upfrontCommission56;
	private Date upfrontPaidDate56;
	private Double upfrontCommission57;
	private Date upfrontPaidDate57;
	private Double upfrontCommission58;
	private Date upfrontPaidDate58;
	private Double upfrontCommission59;
	private Date upfrontPaidDate59;
	private Double upfrontCommission60;
	private Date upfrontPaidDate60;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getInstallmentNumber() {
		return installmentNumber;
	}
	public void setInstallmentNumber(Integer installmentNumber) {
		this.installmentNumber = installmentNumber;
	}
	public Double getUpfrontCommissionExpected() {
		return upfrontCommissionExpected;
	}
	public void setUpfrontCommissionExpected(Double upfrontCommissionExpected) {
		this.upfrontCommissionExpected = upfrontCommissionExpected;
	}
	public Date getUpfrontCommissionExpectedDate() {
		return upfrontCommissionExpectedDate;
	}
	public void setUpfrontCommissionExpectedDate(Date upfrontCommissionExpectedDate) {
		this.upfrontCommissionExpectedDate = upfrontCommissionExpectedDate;
	}
	public Double getUpfrontCommissionReceived() {
		return upfrontCommissionReceived;
	}
	public void setUpfrontCommissionReceived(Double upfrontCommissionReceived) {
		this.upfrontCommissionReceived = upfrontCommissionReceived;
	}
	public Date getUpfrontCommissionReceivedDate() {
		return upfrontCommissionReceivedDate;
	}
	public void setUpfrontCommissionReceivedDate(Date upfrontCommissionReceivedDate) {
		this.upfrontCommissionReceivedDate = upfrontCommissionReceivedDate;
	}
	public Double getUpfrontCommission12() {
		return upfrontCommission12;
	}
	public void setUpfrontCommission12(Double upfrontCommission12) {
		this.upfrontCommission12 = upfrontCommission12;
	}
	public Date getUpfrontPaidDate12() {
		return upfrontPaidDate12;
	}
	public void setUpfrontPaidDate12(Date upfrontPaidDate12) {
		this.upfrontPaidDate12 = upfrontPaidDate12;
	}
	public Double getUpfrontCommission13() {
		return upfrontCommission13;
	}
	public void setUpfrontCommission13(Double upfrontCommission13) {
		this.upfrontCommission13 = upfrontCommission13;
	}
	public Date getUpfrontPaidDate13() {
		return upfrontPaidDate13;
	}
	public void setUpfrontPaidDate13(Date upfrontPaidDate13) {
		this.upfrontPaidDate13 = upfrontPaidDate13;
	}
	public Double getUpfrontCommission14() {
		return upfrontCommission14;
	}
	public void setUpfrontCommission14(Double upfrontCommission14) {
		this.upfrontCommission14 = upfrontCommission14;
	}
	public Date getUpfrontPaidDate14() {
		return upfrontPaidDate14;
	}
	public void setUpfrontPaidDate14(Date upfrontPaidDate14) {
		this.upfrontPaidDate14 = upfrontPaidDate14;
	}
	public Double getUpfrontCommission15() {
		return upfrontCommission15;
	}
	public void setUpfrontCommission15(Double upfrontCommission15) {
		this.upfrontCommission15 = upfrontCommission15;
	}
	public Date getUpfrontPaidDate15() {
		return upfrontPaidDate15;
	}
	public void setUpfrontPaidDate15(Date upfrontPaidDate15) {
		this.upfrontPaidDate15 = upfrontPaidDate15;
	}
	public Double getUpfrontCommission16() {
		return upfrontCommission16;
	}
	public void setUpfrontCommission16(Double upfrontCommission16) {
		this.upfrontCommission16 = upfrontCommission16;
	}
	public Date getUpfrontPaidDate16() {
		return upfrontPaidDate16;
	}
	public void setUpfrontPaidDate16(Date upfrontPaidDate16) {
		this.upfrontPaidDate16 = upfrontPaidDate16;
	}
	public Double getUpfrontCommission17() {
		return upfrontCommission17;
	}
	public void setUpfrontCommission17(Double upfrontCommission17) {
		this.upfrontCommission17 = upfrontCommission17;
	}
	public Date getUpfrontPaidDate17() {
		return upfrontPaidDate17;
	}
	public void setUpfrontPaidDate17(Date upfrontPaidDate17) {
		this.upfrontPaidDate17 = upfrontPaidDate17;
	}
	public Double getUpfrontCommission18() {
		return upfrontCommission18;
	}
	public void setUpfrontCommission18(Double upfrontCommission18) {
		this.upfrontCommission18 = upfrontCommission18;
	}
	public Date getUpfrontPaidDate18() {
		return upfrontPaidDate18;
	}
	public void setUpfrontPaidDate18(Date upfrontPaidDate18) {
		this.upfrontPaidDate18 = upfrontPaidDate18;
	}
	public Double getUpfrontCommission19() {
		return upfrontCommission19;
	}
	public void setUpfrontCommission19(Double upfrontCommission19) {
		this.upfrontCommission19 = upfrontCommission19;
	}
	public Date getUpfrontPaidDate19() {
		return upfrontPaidDate19;
	}
	public void setUpfrontPaidDate19(Date upfrontPaidDate19) {
		this.upfrontPaidDate19 = upfrontPaidDate19;
	}
	public Double getUpfrontCommission20() {
		return upfrontCommission20;
	}
	public void setUpfrontCommission20(Double upfrontCommission20) {
		this.upfrontCommission20 = upfrontCommission20;
	}
	public Date getUpfrontPaidDate20() {
		return upfrontPaidDate20;
	}
	public void setUpfrontPaidDate20(Date upfrontPaidDate20) {
		this.upfrontPaidDate20 = upfrontPaidDate20;
	}
	public Double getUpfrontCommission21() {
		return upfrontCommission21;
	}
	public void setUpfrontCommission21(Double upfrontCommission21) {
		this.upfrontCommission21 = upfrontCommission21;
	}
	public Date getUpfrontPaidDate21() {
		return upfrontPaidDate21;
	}
	public void setUpfrontPaidDate21(Date upfrontPaidDate21) {
		this.upfrontPaidDate21 = upfrontPaidDate21;
	}
	public Double getUpfrontCommission22() {
		return upfrontCommission22;
	}
	public void setUpfrontCommission22(Double upfrontCommission22) {
		this.upfrontCommission22 = upfrontCommission22;
	}
	public Date getUpfrontPaidDate22() {
		return upfrontPaidDate22;
	}
	public void setUpfrontPaidDate22(Date upfrontPaidDate22) {
		this.upfrontPaidDate22 = upfrontPaidDate22;
	}
	public Double getUpfrontCommission23() {
		return upfrontCommission23;
	}
	public void setUpfrontCommission23(Double upfrontCommission23) {
		this.upfrontCommission23 = upfrontCommission23;
	}
	public Date getUpfrontPaidDate23() {
		return upfrontPaidDate23;
	}
	public void setUpfrontPaidDate23(Date upfrontPaidDate23) {
		this.upfrontPaidDate23 = upfrontPaidDate23;
	}
	public Double getUpfrontCommission24() {
		return upfrontCommission24;
	}
	public void setUpfrontCommission24(Double upfrontCommission24) {
		this.upfrontCommission24 = upfrontCommission24;
	}
	public Date getUpfrontPaidDate24() {
		return upfrontPaidDate24;
	}
	public void setUpfrontPaidDate24(Date upfrontPaidDate24) {
		this.upfrontPaidDate24 = upfrontPaidDate24;
	}
	public Double getUpfrontCommission25() {
		return upfrontCommission25;
	}
	public void setUpfrontCommission25(Double upfrontCommission25) {
		this.upfrontCommission25 = upfrontCommission25;
	}
	public Date getUpfrontPaidDate25() {
		return upfrontPaidDate25;
	}
	public void setUpfrontPaidDate25(Date upfrontPaidDate25) {
		this.upfrontPaidDate25 = upfrontPaidDate25;
	}
	public Double getUpfrontCommission26() {
		return upfrontCommission26;
	}
	public void setUpfrontCommission26(Double upfrontCommission26) {
		this.upfrontCommission26 = upfrontCommission26;
	}
	public Date getUpfrontPaidDate26() {
		return upfrontPaidDate26;
	}
	public void setUpfrontPaidDate26(Date upfrontPaidDate26) {
		this.upfrontPaidDate26 = upfrontPaidDate26;
	}
	public Double getUpfrontCommission27() {
		return upfrontCommission27;
	}
	public void setUpfrontCommission27(Double upfrontCommission27) {
		this.upfrontCommission27 = upfrontCommission27;
	}
	public Date getUpfrontPaidDate27() {
		return upfrontPaidDate27;
	}
	public void setUpfrontPaidDate27(Date upfrontPaidDate27) {
		this.upfrontPaidDate27 = upfrontPaidDate27;
	}
	public Double getUpfrontCommission28() {
		return upfrontCommission28;
	}
	public void setUpfrontCommission28(Double upfrontCommission28) {
		this.upfrontCommission28 = upfrontCommission28;
	}
	public Date getUpfrontPaidDate28() {
		return upfrontPaidDate28;
	}
	public void setUpfrontPaidDate28(Date upfrontPaidDate28) {
		this.upfrontPaidDate28 = upfrontPaidDate28;
	}
	public Double getUpfrontCommission29() {
		return upfrontCommission29;
	}
	public void setUpfrontCommission29(Double upfrontCommission29) {
		this.upfrontCommission29 = upfrontCommission29;
	}
	public Date getUpfrontPaidDate29() {
		return upfrontPaidDate29;
	}
	public void setUpfrontPaidDate29(Date upfrontPaidDate29) {
		this.upfrontPaidDate29 = upfrontPaidDate29;
	}
	public Double getUpfrontCommission30() {
		return upfrontCommission30;
	}
	public void setUpfrontCommission30(Double upfrontCommission30) {
		this.upfrontCommission30 = upfrontCommission30;
	}
	public Date getUpfrontPaidDate30() {
		return upfrontPaidDate30;
	}
	public void setUpfrontPaidDate30(Date upfrontPaidDate30) {
		this.upfrontPaidDate30 = upfrontPaidDate30;
	}
	public Double getUpfrontCommission31() {
		return upfrontCommission31;
	}
	public void setUpfrontCommission31(Double upfrontCommission31) {
		this.upfrontCommission31 = upfrontCommission31;
	}
	public Date getUpfrontPaidDate31() {
		return upfrontPaidDate31;
	}
	public void setUpfrontPaidDate31(Date upfrontPaidDate31) {
		this.upfrontPaidDate31 = upfrontPaidDate31;
	}
	public Double getUpfrontCommission32() {
		return upfrontCommission32;
	}
	public void setUpfrontCommission32(Double upfrontCommission32) {
		this.upfrontCommission32 = upfrontCommission32;
	}
	public Date getUpfrontPaidDate32() {
		return upfrontPaidDate32;
	}
	public void setUpfrontPaidDate32(Date upfrontPaidDate32) {
		this.upfrontPaidDate32 = upfrontPaidDate32;
	}
	public Double getUpfrontCommission33() {
		return upfrontCommission33;
	}
	public void setUpfrontCommission33(Double upfrontCommission33) {
		this.upfrontCommission33 = upfrontCommission33;
	}
	public Date getUpfrontPaidDate33() {
		return upfrontPaidDate33;
	}
	public void setUpfrontPaidDate33(Date upfrontPaidDate33) {
		this.upfrontPaidDate33 = upfrontPaidDate33;
	}
	public Double getUpfrontCommission34() {
		return upfrontCommission34;
	}
	public void setUpfrontCommission34(Double upfrontCommission34) {
		this.upfrontCommission34 = upfrontCommission34;
	}
	public Date getUpfrontPaidDate34() {
		return upfrontPaidDate34;
	}
	public void setUpfrontPaidDate34(Date upfrontPaidDate34) {
		this.upfrontPaidDate34 = upfrontPaidDate34;
	}
	public Double getUpfrontCommission35() {
		return upfrontCommission35;
	}
	public void setUpfrontCommission35(Double upfrontCommission35) {
		this.upfrontCommission35 = upfrontCommission35;
	}
	public Date getUpfrontPaidDate35() {
		return upfrontPaidDate35;
	}
	public void setUpfrontPaidDate35(Date upfrontPaidDate35) {
		this.upfrontPaidDate35 = upfrontPaidDate35;
	}
	public Double getUpfrontCommission36() {
		return upfrontCommission36;
	}
	public void setUpfrontCommission36(Double upfrontCommission36) {
		this.upfrontCommission36 = upfrontCommission36;
	}
	public Date getUpfrontPaidDate36() {
		return upfrontPaidDate36;
	}
	public void setUpfrontPaidDate36(Date upfrontPaidDate36) {
		this.upfrontPaidDate36 = upfrontPaidDate36;
	}
	public Double getUpfrontCommission37() {
		return upfrontCommission37;
	}
	public void setUpfrontCommission37(Double upfrontCommission37) {
		this.upfrontCommission37 = upfrontCommission37;
	}
	public Date getUpfrontPaidDate37() {
		return upfrontPaidDate37;
	}
	public void setUpfrontPaidDate37(Date upfrontPaidDate37) {
		this.upfrontPaidDate37 = upfrontPaidDate37;
	}
	public Double getUpfrontCommission38() {
		return upfrontCommission38;
	}
	public void setUpfrontCommission38(Double upfrontCommission38) {
		this.upfrontCommission38 = upfrontCommission38;
	}
	public Date getUpfrontPaidDate38() {
		return upfrontPaidDate38;
	}
	public void setUpfrontPaidDate38(Date upfrontPaidDate38) {
		this.upfrontPaidDate38 = upfrontPaidDate38;
	}
	public Double getUpfrontCommission39() {
		return upfrontCommission39;
	}
	public void setUpfrontCommission39(Double upfrontCommission39) {
		this.upfrontCommission39 = upfrontCommission39;
	}
	public Date getUpfrontPaidDate39() {
		return upfrontPaidDate39;
	}
	public void setUpfrontPaidDate39(Date upfrontPaidDate39) {
		this.upfrontPaidDate39 = upfrontPaidDate39;
	}
	public Double getUpfrontCommission40() {
		return upfrontCommission40;
	}
	public void setUpfrontCommission40(Double upfrontCommission40) {
		this.upfrontCommission40 = upfrontCommission40;
	}
	public Date getUpfrontPaidDate40() {
		return upfrontPaidDate40;
	}
	public void setUpfrontPaidDate40(Date upfrontPaidDate40) {
		this.upfrontPaidDate40 = upfrontPaidDate40;
	}
	public Double getUpfrontCommission41() {
		return upfrontCommission41;
	}
	public void setUpfrontCommission41(Double upfrontCommission41) {
		this.upfrontCommission41 = upfrontCommission41;
	}
	public Date getUpfrontPaidDate41() {
		return upfrontPaidDate41;
	}
	public void setUpfrontPaidDate41(Date upfrontPaidDate41) {
		this.upfrontPaidDate41 = upfrontPaidDate41;
	}
	public Double getUpfrontCommission42() {
		return upfrontCommission42;
	}
	public void setUpfrontCommission42(Double upfrontCommission42) {
		this.upfrontCommission42 = upfrontCommission42;
	}
	public Date getUpfrontPaidDate42() {
		return upfrontPaidDate42;
	}
	public void setUpfrontPaidDate42(Date upfrontPaidDate42) {
		this.upfrontPaidDate42 = upfrontPaidDate42;
	}
	public Double getUpfrontCommission43() {
		return upfrontCommission43;
	}
	public void setUpfrontCommission43(Double upfrontCommission43) {
		this.upfrontCommission43 = upfrontCommission43;
	}
	public Date getUpfrontPaidDate43() {
		return upfrontPaidDate43;
	}
	public void setUpfrontPaidDate43(Date upfrontPaidDate43) {
		this.upfrontPaidDate43 = upfrontPaidDate43;
	}
	public Double getUpfrontCommission44() {
		return upfrontCommission44;
	}
	public void setUpfrontCommission44(Double upfrontCommission44) {
		this.upfrontCommission44 = upfrontCommission44;
	}
	public Date getUpfrontPaidDate44() {
		return upfrontPaidDate44;
	}
	public void setUpfrontPaidDate44(Date upfrontPaidDate44) {
		this.upfrontPaidDate44 = upfrontPaidDate44;
	}
	public Double getUpfrontCommission45() {
		return upfrontCommission45;
	}
	public void setUpfrontCommission45(Double upfrontCommission45) {
		this.upfrontCommission45 = upfrontCommission45;
	}
	public Date getUpfrontPaidDate45() {
		return upfrontPaidDate45;
	}
	public void setUpfrontPaidDate45(Date upfrontPaidDate45) {
		this.upfrontPaidDate45 = upfrontPaidDate45;
	}
	public Double getUpfrontCommission46() {
		return upfrontCommission46;
	}
	public void setUpfrontCommission46(Double upfrontCommission46) {
		this.upfrontCommission46 = upfrontCommission46;
	}
	public Date getUpfrontPaidDate46() {
		return upfrontPaidDate46;
	}
	public void setUpfrontPaidDate46(Date upfrontPaidDate46) {
		this.upfrontPaidDate46 = upfrontPaidDate46;
	}
	public Double getUpfrontCommission47() {
		return upfrontCommission47;
	}
	public void setUpfrontCommission47(Double upfrontCommission47) {
		this.upfrontCommission47 = upfrontCommission47;
	}
	public Date getUpfrontPaidDate47() {
		return upfrontPaidDate47;
	}
	public void setUpfrontPaidDate47(Date upfrontPaidDate47) {
		this.upfrontPaidDate47 = upfrontPaidDate47;
	}
	public Double getUpfrontCommission48() {
		return upfrontCommission48;
	}
	public void setUpfrontCommission48(Double upfrontCommission48) {
		this.upfrontCommission48 = upfrontCommission48;
	}
	public Date getUpfrontPaidDate48() {
		return upfrontPaidDate48;
	}
	public void setUpfrontPaidDate48(Date upfrontPaidDate48) {
		this.upfrontPaidDate48 = upfrontPaidDate48;
	}
	public Double getUpfrontCommission49() {
		return upfrontCommission49;
	}
	public void setUpfrontCommission49(Double upfrontCommission49) {
		this.upfrontCommission49 = upfrontCommission49;
	}
	public Date getUpfrontPaidDate49() {
		return upfrontPaidDate49;
	}
	public void setUpfrontPaidDate49(Date upfrontPaidDate49) {
		this.upfrontPaidDate49 = upfrontPaidDate49;
	}
	public Double getUpfrontCommission50() {
		return upfrontCommission50;
	}
	public void setUpfrontCommission50(Double upfrontCommission50) {
		this.upfrontCommission50 = upfrontCommission50;
	}
	public Date getUpfrontPaidDate50() {
		return upfrontPaidDate50;
	}
	public void setUpfrontPaidDate50(Date upfrontPaidDate50) {
		this.upfrontPaidDate50 = upfrontPaidDate50;
	}
	public Double getUpfrontCommission51() {
		return upfrontCommission51;
	}
	public void setUpfrontCommission51(Double upfrontCommission51) {
		this.upfrontCommission51 = upfrontCommission51;
	}
	public Date getUpfrontPaidDate51() {
		return upfrontPaidDate51;
	}
	public void setUpfrontPaidDate51(Date upfrontPaidDate51) {
		this.upfrontPaidDate51 = upfrontPaidDate51;
	}
	public Double getUpfrontCommission52() {
		return upfrontCommission52;
	}
	public void setUpfrontCommission52(Double upfrontCommission52) {
		this.upfrontCommission52 = upfrontCommission52;
	}
	public Date getUpfrontPaidDate52() {
		return upfrontPaidDate52;
	}
	public void setUpfrontPaidDate52(Date upfrontPaidDate52) {
		this.upfrontPaidDate52 = upfrontPaidDate52;
	}
	public Double getUpfrontCommission53() {
		return upfrontCommission53;
	}
	public void setUpfrontCommission53(Double upfrontCommission53) {
		this.upfrontCommission53 = upfrontCommission53;
	}
	public Date getUpfrontPaidDate53() {
		return upfrontPaidDate53;
	}
	public void setUpfrontPaidDate53(Date upfrontPaidDate53) {
		this.upfrontPaidDate53 = upfrontPaidDate53;
	}
	public Double getUpfrontCommission54() {
		return upfrontCommission54;
	}
	public void setUpfrontCommission54(Double upfrontCommission54) {
		this.upfrontCommission54 = upfrontCommission54;
	}
	public Date getUpfrontPaidDate54() {
		return upfrontPaidDate54;
	}
	public void setUpfrontPaidDate54(Date upfrontPaidDate54) {
		this.upfrontPaidDate54 = upfrontPaidDate54;
	}
	public Double getUpfrontCommission55() {
		return upfrontCommission55;
	}
	public void setUpfrontCommission55(Double upfrontCommission55) {
		this.upfrontCommission55 = upfrontCommission55;
	}
	public Date getUpfrontPaidDate55() {
		return upfrontPaidDate55;
	}
	public void setUpfrontPaidDate55(Date upfrontPaidDate55) {
		this.upfrontPaidDate55 = upfrontPaidDate55;
	}
	public Double getUpfrontCommission56() {
		return upfrontCommission56;
	}
	public void setUpfrontCommission56(Double upfrontCommission56) {
		this.upfrontCommission56 = upfrontCommission56;
	}
	public Date getUpfrontPaidDate56() {
		return upfrontPaidDate56;
	}
	public void setUpfrontPaidDate56(Date upfrontPaidDate56) {
		this.upfrontPaidDate56 = upfrontPaidDate56;
	}
	public Double getUpfrontCommission57() {
		return upfrontCommission57;
	}
	public void setUpfrontCommission57(Double upfrontCommission57) {
		this.upfrontCommission57 = upfrontCommission57;
	}
	public Date getUpfrontPaidDate57() {
		return upfrontPaidDate57;
	}
	public void setUpfrontPaidDate57(Date upfrontPaidDate57) {
		this.upfrontPaidDate57 = upfrontPaidDate57;
	}
	public Double getUpfrontCommission58() {
		return upfrontCommission58;
	}
	public void setUpfrontCommission58(Double upfrontCommission58) {
		this.upfrontCommission58 = upfrontCommission58;
	}
	public Date getUpfrontPaidDate58() {
		return upfrontPaidDate58;
	}
	public void setUpfrontPaidDate58(Date upfrontPaidDate58) {
		this.upfrontPaidDate58 = upfrontPaidDate58;
	}
	public Double getUpfrontCommission59() {
		return upfrontCommission59;
	}
	public void setUpfrontCommission59(Double upfrontCommission59) {
		this.upfrontCommission59 = upfrontCommission59;
	}
	public Date getUpfrontPaidDate59() {
		return upfrontPaidDate59;
	}
	public void setUpfrontPaidDate59(Date upfrontPaidDate59) {
		this.upfrontPaidDate59 = upfrontPaidDate59;
	}
	public Double getUpfrontCommission60() {
		return upfrontCommission60;
	}
	public void setUpfrontCommission60(Double upfrontCommission60) {
		this.upfrontCommission60 = upfrontCommission60;
	}
	public Date getUpfrontPaidDate60() {
		return upfrontPaidDate60;
	}
	public void setUpfrontPaidDate60(Date upfrontPaidDate60) {
		this.upfrontPaidDate60 = upfrontPaidDate60;
	}
	
	
	
	
	
	
	
}