package com.hovey.frontend.agent.dto;

public class TransactionsDto {

	
	private int transactionId;
	
	/**
	 *added qaDealStatus by bhagya on june 25th,2015 for implementing qa module,
	 *this field we are using for admin deal management 
	 */
	private Boolean qaDealStatus;
	/**
	 * added qaOrderStatus by bhagya on july 01st,2015 for implementing qa module,
	 * this field we are using at hoveyQA user
	 */
	private Boolean qaOrderStatus;
	
	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Boolean getQaDealStatus() {
		return qaDealStatus;
	}

	public void setQaDealStatus(Boolean qaDealStatus) {
		this.qaDealStatus = qaDealStatus;
	}

	public Boolean getQaOrderStatus() {
		return qaOrderStatus;
	}

	public void setQaOrderStatus(Boolean qaOrderStatus) {
		this.qaOrderStatus = qaOrderStatus;
	}
	
	
}
