package com.hovey.frontend.common.utility;

import java.util.Calendar;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.hovey.frontend.agent.dto.OrdersDto;

/**
 * Created By bhagya On August 25th, 2016
 * 
 * Excel Report For champion Supplier Residual Payments Report
 * @author KNS-ACCONTS
 *
 */
public class ChampionExportView extends AbstractExcelView{
	private static Logger log=Logger.getLogger(ChampionExportView.class);

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("inside buildExcelDocument()");
		
		Map<String, OrdersDto> pipelineData=(Map<String, OrdersDto>) model.get("orders");
		HSSFSheet sheet = workbook.createSheet("Pipeline");
		
		 CellStyle style = workbook.createCellStyle();
	        Font font = workbook.createFont();
	        font.setColor(HSSFColor.BLACK.index);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        style.setFont(font);
		
	        CellStyle currency=workbook.createCellStyle();
			currency.setDataFormat((short)7);
		// Setting Headings..
		
		HSSFRow header = sheet.createRow(0);		
		header.createCell(0).setCellValue("Order Date");
		header.createCell(1).setCellValue("Supplier Name");
		header.createCell(2).setCellValue("Account Number");
		header.createCell(3).setCellValue("Business Name");
		header.createCell(4).setCellValue("DBA");
		header.createCell(5).setCellValue("Contract Type");
		header.createCell(6).setCellValue("KWH");
		header.createCell(7).setCellValue("Agent Name");
		header.createCell(8).setCellValue("Agent Number");
		header.createCell(9).setCellValue("Status");
		header.createCell(10).setCellValue("Sent To Supplier");
		header.createCell(11).setCellValue("Deal Start Date");
		header.createCell(12).setCellValue("Term");
		header.createCell(13).setCellValue("Upfront Commission Expected");
		
		
		
		header.createCell(14).setCellValue("Upfront Commission12");
		header.createCell(15).setCellValue("Upfront Paid Date12");
		header.createCell(16).setCellValue("Upfront Commission13");
		header.createCell(17).setCellValue("Upfront Paid Date13");
		header.createCell(18).setCellValue("Upfront Commission14");
		header.createCell(19).setCellValue("Upfront Paid Date14");
		header.createCell(20).setCellValue("Upfront Commission15");
		header.createCell(21).setCellValue("Upfront Paid Date15");
		header.createCell(22).setCellValue("Upfront Commission16");
		header.createCell(23).setCellValue("Upfront Paid Date16");
		header.createCell(24).setCellValue("Upfront Commission17");
		header.createCell(25).setCellValue("Upfront Paid Date17");
		header.createCell(26).setCellValue("Upfront Commission18");
		header.createCell(27).setCellValue("Upfront Paid Date18");
		header.createCell(28).setCellValue("Upfront Commission19");
		header.createCell(29).setCellValue("Upfront Paid Date19");
		header.createCell(30).setCellValue("Upfront Commission20");
		header.createCell(31).setCellValue("Upfront Paid Date20");
		header.createCell(32).setCellValue("Upfront Commission21");
		header.createCell(33).setCellValue("Upfront Paid Date21");
		header.createCell(34).setCellValue("Upfront Commission22");
		header.createCell(35).setCellValue("Upfront Paid Date22");
		header.createCell(36).setCellValue("Upfront Commission23");
		header.createCell(37).setCellValue("Upfront Paid Date23");
		header.createCell(38).setCellValue("Upfront Commission24");
		header.createCell(39).setCellValue("Upfront Paid Date24");
		header.createCell(40).setCellValue("Upfront Commission25");
		header.createCell(41).setCellValue("Upfront Paid Date25");
		header.createCell(42).setCellValue("Upfront Commission26");
		header.createCell(43).setCellValue("Upfront Paid Date26");
		header.createCell(44).setCellValue("Upfront Commission27");
		header.createCell(45).setCellValue("Upfront Paid Date27");
		header.createCell(46).setCellValue("Upfront Commission28");
		header.createCell(47).setCellValue("Upfront Paid Date28");
		header.createCell(48).setCellValue("Upfront Commission29");
		header.createCell(49).setCellValue("Upfront Paid Date29");
		header.createCell(50).setCellValue("Upfront Commission30");
		header.createCell(51).setCellValue("Upfront Paid Date30");
		header.createCell(52).setCellValue("Upfront Commission31");
		header.createCell(53).setCellValue("Upfront Paid Date31");
		header.createCell(54).setCellValue("Upfront Commission32");
		header.createCell(55).setCellValue("Upfront Paid Date32");
		header.createCell(56).setCellValue("Upfront Commission33");
		header.createCell(57).setCellValue("Upfront Paid Date33");
		header.createCell(58).setCellValue("Upfront Commission34");
		header.createCell(59).setCellValue("Upfront Paid Date34");
		header.createCell(60).setCellValue("Upfront Commission35");
		header.createCell(61).setCellValue("Upfront Paid Date35");
		header.createCell(62).setCellValue("Upfront Commission36");
		header.createCell(63).setCellValue("Upfront Paid Date36");
		header.createCell(64).setCellValue("Upfront Commission37");
		header.createCell(65).setCellValue("Upfront Paid Date37");
		header.createCell(66).setCellValue("Upfront Commission38");
		header.createCell(67).setCellValue("Upfront Paid Date38");
		header.createCell(68).setCellValue("Upfront Commission38");
		header.createCell(69).setCellValue("Upfront Paid Date39");
		header.createCell(70).setCellValue("Upfront Commission39");
		header.createCell(71).setCellValue("Upfront Paid Date40");
		header.createCell(72).setCellValue("Upfront Commission40");
		header.createCell(73).setCellValue("Upfront Paid Date41");
		header.createCell(74).setCellValue("Upfront Commission41");
		header.createCell(75).setCellValue("Upfront Paid Date42");
		header.createCell(76).setCellValue("Upfront Commission42");
		header.createCell(77).setCellValue("Upfront Paid Date43");
		header.createCell(78).setCellValue("Upfront Commission43");
		header.createCell(79).setCellValue("Upfront Paid Date44");
		header.createCell(80).setCellValue("Upfront Commission44");
		header.createCell(81).setCellValue("Upfront Paid Date45");
		header.createCell(82).setCellValue("Upfront Commission45");
		header.createCell(83).setCellValue("Upfront Paid Date46");
		header.createCell(84).setCellValue("Upfront Commission46");
		header.createCell(85).setCellValue("Upfront Paid Date47");
		header.createCell(86).setCellValue("Upfront Commission47");
		header.createCell(87).setCellValue("Upfront Paid Date48");
		header.createCell(88).setCellValue("Upfront Commission48");
		header.createCell(89).setCellValue("Upfront Paid Date49");
		header.createCell(90).setCellValue("Upfront Commission49");
		header.createCell(91).setCellValue("Upfront Paid Date50");
		header.createCell(92).setCellValue("Upfront Commission51");
		header.createCell(93).setCellValue("Upfront Paid Date51");
		header.createCell(94).setCellValue("Upfront Commission52");
		header.createCell(95).setCellValue("Upfront Paid Date52");
		header.createCell(96).setCellValue("Upfront Commission53");
		header.createCell(97).setCellValue("Upfront Paid Date53");
		header.createCell(98).setCellValue("Upfront Commission54");
		header.createCell(99).setCellValue("Upfront Paid Date54");
		header.createCell(100).setCellValue("Upfront Commission55");
		header.createCell(101).setCellValue("Upfront Paid Date55");
		header.createCell(102).setCellValue("Upfront Commission56");
		header.createCell(103).setCellValue("Upfront Paid Date56");
		header.createCell(104).setCellValue("Upfront Commission57");
		header.createCell(105).setCellValue("Upfront Paid Date57");
		header.createCell(106).setCellValue("Upfront Commission58");
		header.createCell(107).setCellValue("Upfront Paid Date58");
		header.createCell(108).setCellValue("Upfront Commission59");
		header.createCell(109).setCellValue("Upfront Paid Date59");
		header.createCell(110).setCellValue("Upfront Commission60");
		header.createCell(111).setCellValue("Upfront Paid Date60");
		
		
		header.createCell(112).setCellValue(" Rate Sold");
		header.createCell(113).setCellValue("Commission Rate");		
		header.createCell(114).setCellValue("fax Received");
		header.createCell(115).setCellValue("Note");
		header.createCell(116).setCellValue("Phone Number");
		header.createCell(117).setCellValue("Contact Name");
		header.createCell(118).setCellValue("Utility");
		
		/*added by bhagya on april 16th,2014*/
		header.createCell(119).setCellValue("Rescinded Order Agent");
		header.createCell(120).setCellValue("Fronter");
		header.createCell(121).setCellValue("State");
		
		
		header.setRowStyle(style);		
		int rowNum = 2;
		//Iterator iterator = pipelineData.entrySet().iterator();
		HSSFCellStyle dateStyle=workbook.createCellStyle();
		CreationHelper createHelper=workbook.getCreationHelper();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		//Added orderDate Style by bhagya On July 23rd,2015 based on client requirement
		HSSFCellStyle orderDateStyle=workbook.createCellStyle();
		CreationHelper OrderDateHelper=workbook.getCreationHelper();
		orderDateStyle.setDataFormat(OrderDateHelper.createDataFormat().getFormat("MM/dd/yyyy HH:mm:ss"));
		
		// Setting Data for Cells.
		for(Map.Entry<String, OrdersDto> entry: pipelineData.entrySet()){
			HSSFRow row = sheet.createRow(rowNum++);
			Cell cell=row.createCell(0);
			cell.setCellValue(entry.getValue().getOrderDate());
			cell.setCellStyle(orderDateStyle);			
			row.createCell(1).setCellValue(entry.getValue().getSupplierName().getSupplierName());
			row.createCell(2).setCellValue(entry.getValue().getAccountNumber());
			row.createCell(3).setCellValue(entry.getValue().getBusinessName());
			row.createCell(4).setCellValue(entry.getValue().getDba());
			row.createCell(5).setCellValue(entry.getValue().getContractType());
			row.createCell(6).setCellValue(entry.getValue().getKwh());
			StringBuilder name=new StringBuilder();
			name.append(entry.getValue().getCreatedAgent().getFirstName());
			name.append(" ");
			name.append(entry.getValue().getCreatedAgent().getLastName());
			row.createCell(7).setCellValue(name.toString());
			row.createCell(8).setCellValue(entry.getValue().getCreatedAgent().getAgentNumber());
			row.createCell(9).setCellValue(entry.getValue().getStatus());
			Cell cell2=row.createCell(10);
			if(null!=entry.getValue().getSentToSupplier()){
				cell2.setCellValue(entry.getValue().getSentToSupplier());
			}
			cell2.setCellStyle(dateStyle);
			Cell cell3=row.createCell(11);
			if(null!=entry.getValue().getDealStartDate()){
				cell3.setCellValue(entry.getValue().getDealStartDate());
			}			
			cell3.setCellStyle(dateStyle);
			row.createCell(12).setCellValue(entry.getValue().getTerm());
			
			row.createCell(13).setCellValue(new Double(entry.getValue().getCommission()));		
			row.getCell(13).setCellStyle(currency);
						
			
			
			
			row.createCell(14).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission12()));		
			row.getCell(14).setCellStyle(currency);
			
			Cell cell5=row.createCell(15);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate12()){
				cell5.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate12());
			}
			cell5.setCellStyle(dateStyle);
			
			row.createCell(16).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission13()));		
			row.getCell(16).setCellStyle(currency);
			
			Cell cell6=row.createCell(17);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate13()){
				cell6.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate13());
			}
			cell6.setCellStyle(dateStyle);
			
			row.createCell(18).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission14()));		
			row.getCell(18).setCellStyle(currency);
			
			Cell cell7=row.createCell(19);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate14()){
				cell7.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate14());
			}
			cell7.setCellStyle(dateStyle);
			
			row.createCell(20).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission15()));		
			row.getCell(20).setCellStyle(currency);
			
			Cell cell8=row.createCell(21);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate15()){
				cell8.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate15());
			}
			cell8.setCellStyle(dateStyle);
			
			row.createCell(22).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission16()));		
			row.getCell(22).setCellStyle(currency);
			
			Cell cell9=row.createCell(23);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate16()){
				cell9.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate16());
			}
			cell9.setCellStyle(dateStyle);
			
			row.createCell(24).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission17()));		
			row.getCell(24).setCellStyle(currency);
			
			Cell cell10=row.createCell(25);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate17()){
				cell10.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate17());
			}
			cell10.setCellStyle(dateStyle);
			
			row.createCell(26).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission18()));		
			row.getCell(26).setCellStyle(currency);
			
			Cell cell11=row.createCell(27);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate18()){
				cell11.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate18());
			}
			cell11.setCellStyle(dateStyle);
			
			row.createCell(28).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission19()));		
			row.getCell(28).setCellStyle(currency);
			
			Cell cell12=row.createCell(29);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate19()){
				cell12.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate19());
			}
			cell12.setCellStyle(dateStyle);
			
			row.createCell(30).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission20()));		
			row.getCell(30).setCellStyle(currency);
			
			Cell cell13=row.createCell(31);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate20()){
				cell13.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate20());
			}
			cell13.setCellStyle(dateStyle);
			
			row.createCell(32).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission21()));		
			row.getCell(32).setCellStyle(currency);
			
			Cell cell14=row.createCell(33);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate21()){
				cell14.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate21());
			}
			cell14.setCellStyle(dateStyle);
			
			row.createCell(34).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission22()));		
			row.getCell(34).setCellStyle(currency);
			
			Cell cell15=row.createCell(35);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate22()){
				cell15.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate22());
			}
			cell15.setCellStyle(dateStyle);
			
			row.createCell(36).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission23()));		
			row.getCell(36).setCellStyle(currency);
			
			Cell cell16=row.createCell(37);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate23()){
				cell16.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate23());
			}
			cell16.setCellStyle(dateStyle);
			
			row.createCell(38).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission24()));		
			row.getCell(38).setCellStyle(currency);
			
			Cell cell17=row.createCell(39);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate24()){
				cell17.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate24());
			}
			cell17.setCellStyle(dateStyle);
			
			row.createCell(40).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission25()));		
			row.getCell(40).setCellStyle(currency);
			
			Cell cell18=row.createCell(41);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate25()){
				cell18.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate25());
			}
			cell18.setCellStyle(dateStyle);
			
			row.createCell(42).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission26()));		
			row.getCell(42).setCellStyle(currency);
			
			Cell cell19=row.createCell(43);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate26()){
				cell19.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate26());
			}
			cell19.setCellStyle(dateStyle);
			
			row.createCell(44).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission27()));		
			row.getCell(44).setCellStyle(currency);
			
			Cell cell20=row.createCell(45);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate27()){
				cell20.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate27());
			}
			cell20.setCellStyle(dateStyle);
			
			row.createCell(46).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission28()));		
			row.getCell(46).setCellStyle(currency);
			
			Cell cell21=row.createCell(47);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate28()){
				cell21.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate28());
			}
			cell21.setCellStyle(dateStyle);
			
			row.createCell(48).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission29()));		
			row.getCell(48).setCellStyle(currency);
			
			Cell cell22=row.createCell(49);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate29()){
				cell22.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate29());
			}
			cell22.setCellStyle(dateStyle);
			
			row.createCell(50).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission30()));		
			row.getCell(50).setCellStyle(currency);
			
			Cell cell23=row.createCell(51);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate30()){
				cell23.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate30());
			}
			cell23.setCellStyle(dateStyle);
			
			row.createCell(52).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission31()));		
			row.getCell(52).setCellStyle(currency);
			
			Cell cell24=row.createCell(53);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate31()){
				cell24.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate31());
			}
			cell24.setCellStyle(dateStyle);
			
			row.createCell(54).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission32()));		
			row.getCell(54).setCellStyle(currency);
			
			Cell cell25=row.createCell(55);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate32()){
				cell25.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate32());
			}
			cell25.setCellStyle(dateStyle);
			
			row.createCell(56).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission33()));		
			row.getCell(56).setCellStyle(currency);
			
			Cell cell26=row.createCell(57);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate33()){
				cell26.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate33());
			}
			cell26.setCellStyle(dateStyle);
			
			row.createCell(58).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission34()));		
			row.getCell(58).setCellStyle(currency);
			
			Cell cell27=row.createCell(59);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate34()){
				cell27.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate34());
			}
			cell27.setCellStyle(dateStyle);
			
			row.createCell(60).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission35()));		
			row.getCell(60).setCellStyle(currency);
			
			Cell cell28=row.createCell(61);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate35()){
				cell28.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate35());
			}
			
			cell28.setCellStyle(dateStyle);
			
			row.createCell(62).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission36()));		
			row.getCell(62).setCellStyle(currency);
			
			Cell cell29=row.createCell(63);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate36()){
				cell29.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate36());
			}
			cell29.setCellStyle(dateStyle);
			
			row.createCell(64).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission37()));		
			row.getCell(64).setCellStyle(currency);
			
			Cell cell30=row.createCell(65);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate37()){
				cell30.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate37());
			}
			cell30.setCellStyle(dateStyle);
			
			row.createCell(66).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission38()));		
			row.getCell(66).setCellStyle(currency);
			
			Cell cell31=row.createCell(67);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate38()){
				cell31.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate38());
			}
			cell31.setCellStyle(dateStyle);
			
			row.createCell(68).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission39()));		
			row.getCell(68).setCellStyle(currency);
			
			Cell cell32=row.createCell(69);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate39()){
				cell32.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate39());
			}
			cell32.setCellStyle(dateStyle);
			
			row.createCell(70).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission40()));		
			row.getCell(70).setCellStyle(currency);
			
			Cell cell33=row.createCell(71);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate40()){
				cell33.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate40());
			}
			cell33.setCellStyle(dateStyle);
			
			row.createCell(72).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission41()));		
			row.getCell(72).setCellStyle(currency);
			
			Cell cell34=row.createCell(73);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate41()){
				cell34.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate41());
			}
			cell34.setCellStyle(dateStyle);
			
			row.createCell(74).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission42()));		
			row.getCell(74).setCellStyle(currency);
			
			Cell cell35=row.createCell(75);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate42()){
				cell35.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate42());
			}
			cell35.setCellStyle(dateStyle);
			
			row.createCell(76).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission43()));		
			row.getCell(76).setCellStyle(currency);
			
			Cell cell36=row.createCell(77);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate43()){
				cell36.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate43());
			}
			cell36.setCellStyle(dateStyle);
			
			row.createCell(78).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission44()));		
			row.getCell(78).setCellStyle(currency);
			
			Cell cell37=row.createCell(79);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate44()){
				cell37.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate44());
			}
			cell37.setCellStyle(dateStyle);
			
			row.createCell(80).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission45()));		
			row.getCell(80).setCellStyle(currency);
			
			Cell cell38=row.createCell(81);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate45()){
				cell38.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate45());
			}
			cell38.setCellStyle(dateStyle);
			
			row.createCell(82).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission46()));		
			row.getCell(82).setCellStyle(currency);
			
			Cell cell39=row.createCell(83);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate46()){
				cell39.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate46());
			}
			cell39.setCellStyle(dateStyle);
			
			row.createCell(84).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission47()));		
			row.getCell(84).setCellStyle(currency);
			
			Cell cell40=row.createCell(85);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate47()){
				cell40.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate47());
			}
			cell40.setCellStyle(dateStyle);
			
			row.createCell(86).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission48()));		
			row.getCell(86).setCellStyle(currency);
			
			Cell cell41=row.createCell(87);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate48()){
				cell5.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate48());
			}
			cell41.setCellStyle(dateStyle);
			
			row.createCell(88).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission49()));		
			row.getCell(88).setCellStyle(currency);
			
			Cell cell42=row.createCell(89);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate49()){
				cell42.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate49());
			}
			cell42.setCellStyle(dateStyle);
			
			row.createCell(90).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission50()));		
			row.getCell(90).setCellStyle(currency);
			
			Cell cell43=row.createCell(91);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate50()){
				cell43.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate50());
			}
			cell43.setCellStyle(dateStyle);
			
			row.createCell(92).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission51()));		
			row.getCell(92).setCellStyle(currency);
			
			Cell cell44=row.createCell(93);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate51()){
				cell44.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate51());
			}
			cell44.setCellStyle(dateStyle);
			
			row.createCell(94).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission52()));		
			row.getCell(94).setCellStyle(currency);
			
			Cell cell45=row.createCell(95);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate52()){
				cell45.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate52());
			}
			cell45.setCellStyle(dateStyle);
			
			row.createCell(96).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission53()));		
			row.getCell(96).setCellStyle(currency);
			
			Cell cell46=row.createCell(97);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate53()){
				cell46.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate53());
			}
			cell46.setCellStyle(dateStyle);
			
			row.createCell(98).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission54()));		
			row.getCell(98).setCellStyle(currency);
			
			Cell cell47=row.createCell(99);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate54()){
				cell47.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate54());
			}
			cell47.setCellStyle(dateStyle);
			
			row.createCell(100).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission55()));		
			row.getCell(100).setCellStyle(currency);
			
			Cell cell48=row.createCell(101);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate55()){
				cell48.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate55());
			}
			cell48.setCellStyle(dateStyle);
			
			row.createCell(102).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission56()));		
			row.getCell(102).setCellStyle(currency);
			
			Cell cell49=row.createCell(103);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate56()){
				cell49.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate56());
			}
			cell49.setCellStyle(dateStyle);
			
			row.createCell(104).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission57()));		
			row.getCell(104).setCellStyle(currency);
			
			Cell cell50=row.createCell(105);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate57()){
				cell50.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate57());
			}
			cell50.setCellStyle(dateStyle);
			
			row.createCell(106).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission58()));		
			row.getCell(106).setCellStyle(currency);
			
			Cell cell51=row.createCell(107);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate58()){
				cell51.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate58());
			}
			cell51.setCellStyle(dateStyle);
			
			row.createCell(108).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission59()));		
			row.getCell(108).setCellStyle(currency);
			
			Cell cell52=row.createCell(109);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate59()){
				cell52.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate59());
			}
			cell52.setCellStyle(dateStyle);
			
			row.createCell(110).setCellValue(new Double(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontCommission60()));		
			row.getCell(110).setCellStyle(currency);
			
			Cell cell53=row.createCell(111);
			if(null!=entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate60()){
				cell53.setCellValue(entry.getValue().getChampionUpfrontCommissionsDto().getUpfrontPaidDate60());
			}
			cell53.setCellStyle(dateStyle);
			
			
			
			
			row.createCell(112).setCellValue(new Double(entry.getValue().getRate()));	
			Double comRate=entry.getValue().getCommissionRate();
			if(null!=comRate)
			row.createCell(113).setCellValue(comRate);	
			
			
			
			if(entry.getValue().isFaxReceived()){
				row.createCell(114).setCellValue("yes");
			}
			else{
				row.createCell(114).setCellValue("no");
			}			
			row.createCell(115).setCellValue(entry.getValue().getNotes());
			row.createCell(116).setCellValue(entry.getValue().getTaxId().getPhoneNo());
			row.createCell(117).setCellValue(entry.getValue().getTaxId().getFirstName()+" "+entry.getValue().getTaxId().getLastName());
			row.createCell(118).setCellValue(entry.getValue().getUtility().getUtility());
			
			/*added by bhagya on april 16th,2014*/
			
			if(null!=entry.getValue().getResAgent()){
				StringBuilder resName=new StringBuilder();
				resName.append(entry.getValue().getResAgent().getFirstName());
				resName.append(" ");
				resName.append(entry.getValue().getResAgent().getLastName());
			row.createCell(119).setCellValue(resName.toString());
			}	
			row.createCell(120).setCellValue(entry.getValue().getTaxId().getFronter());
			
			//added on June 04, 2014
			row.createCell(121).setCellValue(entry.getValue().getServiceState().getState());
		}		
		for(int i=0;i<=122;i++){
			sheet.autoSizeColumn(i);
		}
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		
		 response.setHeader("Content-Disposition", "inline; filename="+"Champion_Residual_Payments_"+(cal.get(Calendar.MONTH)+1)+"_"+cal.get(Calendar.DATE)+"_"+cal.get(Calendar.YEAR)+".xls");
		  // Make sure to set the correct content type
		  response.setContentType("application/vnd.ms-excel");
		//workbook.write(response.getOutputStream());
		sheet.getWorkbook().write(response.getOutputStream());
		response.getOutputStream().flush();
	}
	
	
	
	
	
}
