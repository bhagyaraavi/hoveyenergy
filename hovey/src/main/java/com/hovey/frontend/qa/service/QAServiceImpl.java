package com.hovey.frontend.qa.service;
/**
 * Created By Bhagya On June 26th,2015
 * Service Implementation Class for QA Services
 */
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transaction;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hovey.backend.admin.dao.AdminDao;
import com.hovey.backend.agent.dao.DealSheetDao;
import com.hovey.backend.agent.exception.OrderNotFoundException;
import com.hovey.backend.agent.model.Orders;
import com.hovey.backend.agent.model.Transactions;
import com.hovey.backend.qa.dao.QADao;
import com.hovey.backend.qa.exception.HoveyQAReportNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotSavedException;
import com.hovey.backend.qa.exception.QATransactionNotFoundException;
import com.hovey.backend.qa.model.HoveyQA;
import com.hovey.frontend.admin.service.AdminService;
import com.hovey.frontend.agent.dto.OrdersDto;
import com.hovey.frontend.agent.dto.TransactionsDto;
import com.hovey.frontend.qa.dto.HoveyQADto;
import com.hovey.frontend.qa.dto.PieChartDto;
import com.hovey.frontend.qa.dto.QAReportDto;
import com.hovey.frontend.supplier.dto.SupplierDto;
import com.hovey.frontend.user.dto.HoveyUserDto;


@Transactional
@Service("qaService")
public class QAServiceImpl implements QAService{
	
	private static Logger log=Logger.getLogger(QAServiceImpl.class);
	
	@Resource(name="qaDao")
	private QADao qaDao;
	
	@Resource(name="adminService")
	private AdminService adminService;
	
	@Resource(name="dealSheetDao")
	private DealSheetDao dealSheetDao;
	
	@Resource(name="adminDao")
	private AdminDao adminDao;
	
	/**
	 * Created By Bhagya On June 26th,2015
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the QA Dealsheets
	 */
	
	public ArrayList<OrdersDto> gettingQADealSheetsFromDao(int pageNo,int pageSize,String sortBy,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter) throws Exception{
		log.info("inside gettingQADealSheetsFromDao()");
		ArrayList<Transactions> transactions=this.qaDao.getQATotalDealSheets(pageNo, pageSize, sortBy, searchBy,completedDeals,adminQADeals,statusFilter);
		ArrayList<OrdersDto> transactionOrderDtos=this.adminService.getOrdersDtoByTransactions(transactions);
		ArrayList<OrdersDto> orderDtos=this.getDealSheets(transactions, transactionOrderDtos);
		return orderDtos;
	}
	
	
		/**
		 * Created By Bhagya On June 25th,2015
		 * @param pageSize
		 * @param searchBy
		 * @return
		 * @throws Exception
		 *  method returns the total no of  QA OderPages..
		 *  
		 */
	
		public Integer findTotalNoOfQADealPages(int pageSize,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter) throws Exception{
			log.info("inside findTotalNoOfQADealPages()");
			int totalRecords=this.qaDao.getTotalNoofQADealSheets(searchBy,completedDeals,adminQADeals,statusFilter);
			int result=totalRecords/pageSize;
			int pagesNeeded;
			if(totalRecords%pageSize>0){
				pagesNeeded=result+1;
			}
			else{
				pagesNeeded=result;
			}
			return pagesNeeded;
		}
		/**
		 * Created By Bhagya On June 26th,2015
		 * @param searchBy
		 * @return
		 * @throws Exception
		 * Method For getting the count of total Qa deal sheets
		 */
		public Integer getTotalQADealSheetsCount(String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception{
			log.info("inside getTotalDealSheetsCount()");
			int totalQADeals=this.qaDao.getTotalNoofQADealSheets(searchBy,completedDeals,adminQADeals,statusFilter);
			return totalQADeals;
		}
		/**
		 * Created By Bhagya On July 02nd,2015
		 * @param dealId
		 * @return
		 * @throws Exception
		 * 
		 * Method For getting the qa report information for deal based on deal Id
		 * 1.getting list of orders exist under deal
		 * 2.Getting the QA Report For the first order in that deal
		 * 3.return the QA Report Dto
		 */
		public HoveyQADto gettingQAReportForDeal(Integer dealId) throws HoveyQAReportNotFoundException, Exception{
			log.info("inside gettingQAReportForDeal()");
			 ArrayList<Orders> orders=this.dealSheetDao.getOrdersofATrandsaction(dealId);
			 HoveyQADto hoveyQADto=null;
			 HoveyQA hoveyQAReport =this.qaDao.getQAReportDetailsByOrder(orders.get(0));
			 if(null!=hoveyQAReport){
				 hoveyQADto=HoveyQADto.populateQAFormDto(hoveyQAReport);
			 }
			 else{
				 throw new HoveyQAReportNotFoundException();
			 }
			 return hoveyQADto;
		}
		/**
		 * Created By bhagya On july 02nd,2015
		 * @param orderId
		 * @return
		 * @throws Exception
		 * Method for getting the qa report information by orderId
		 */
		public HoveyQADto gettingQAReportForOrder(Integer orderId) throws HoveyQAReportNotFoundException,Exception{
			log.info("inside gettingQAReportForOrder()");
			 Orders order=this.adminDao.getOrderByOrderId(orderId);
			 HoveyQADto hoveyQADto=null;
			 HoveyQA hoveyQAReport =this.qaDao.getQAReportDetailsByOrder(order);
			
			 if(null!=hoveyQAReport){
				 hoveyQADto=HoveyQADto.populateQAFormDto(hoveyQAReport);
			 }
			 else{
				 throw new HoveyQAReportNotFoundException();
			 }
			return hoveyQADto;
		}
	/**
	 * Created By Bhagya On July 02nd,2015	
	 * @param dealId
	 * @param orderId
	 * @param hoveyQADto
	 * @return
	 * @throws Exception
	 * 
	 * Method for saving the qa deal and order for the user hoveyQA
	 * 1.Getting list of all orders for deal,saving the qa information to all orders which exist in deal by iterating the orders
	 * 2.for individual order
	 * 		getting the order
	 * 		setting the qaOrderStaus at transaction table and saving it
	 * 		saving the hovey qa Details
	 */
	public Integer saveQAReportDetails(Integer dealId,Integer orderId,HoveyQADto hoveyQADto) throws Exception{
			log.info("inside saveQAReportDetails()");
			Integer savedResult=0;
			if(dealId!=0){
				 ArrayList<Orders> orders=this.dealSheetDao.getOrdersofATrandsaction(dealId);
				 for(Orders order:orders){
					 if(null!=hoveyQADto.getQaId()){
						 HoveyQA hoveyQAReport =this.qaDao.getQAReportDetailsByOrder(order);
						 hoveyQADto.setQaId(hoveyQAReport.getQaId());
					 }
				savedResult=this.SavingHoveyQAtoDB(order, hoveyQADto);
				 }
			}
			if(orderId!=0){
				Orders order=this.adminDao.getOrderByOrderId(orderId);
				Transactions trans=order.getTransactionId();
				 ArrayList<Orders> orders=this.dealSheetDao.getOrdersofATrandsaction(trans.getId());
				 if(orders.size()>1){
					 trans.setQaOrderStatus(true);
				 }
				 this.dealSheetDao.saveTransactionToDB(trans);
				savedResult=this.SavingHoveyQAtoDB(order, hoveyQADto);
				try{
					ArrayList<Orders> qaNotReadyOrders=this.qaDao.getQAPendingNotReadyOrdersByDealId(trans.getId());
					for(Orders nrOrders:qaNotReadyOrders){
						nrOrders.setQaStatus("NOT READY");
					}
					this.dealSheetDao.saveDealSheetOrdersToDB(qaNotReadyOrders);
				}
				catch(Exception e){
					
				}
			}
		return savedResult;
	}
	/**
	 * Created By Bhagya On July 02nd,2015	
	 * @param order
	 * @param hoveyQADto
	 * @return
	 * @throws Exception 
	 */
	public Integer SavingHoveyQAtoDB(Orders order,HoveyQADto hoveyQADto) throws Exception{
		log.info("inside savingHoveyQAtoDB()");
		 HoveyQA hoveyQA=null;
		 if(null!=hoveyQADto.getQaId()){
			hoveyQA=this.qaDao.getQAReportDetailsByQAId(hoveyQADto.getQaId());
		 }
		 else{
			hoveyQA=new HoveyQA();
		 }
		 hoveyQA.setOrder(order);
		 hoveyQA.setQaDate(hoveyQADto.getQaDate());
		 hoveyQA.setQaScore(hoveyQADto.getQaScore());
		 if(null!=hoveyQADto.getBrandsCall()){
			 hoveyQA.setBrandsCall(hoveyQADto.getBrandsCall());
		 }
		 else{
			 hoveyQA.setBrandsCall(false);
		 }
		 if(null!=hoveyQADto.getPositiveTone()){
			 hoveyQA.setPositiveTone(hoveyQADto.getPositiveTone());
		 }
		 else{
			 hoveyQA.setPositiveTone(false);
		 }
		 if(null!=hoveyQADto.getSoundsNatural()){
			 hoveyQA.setSoundsNatural(hoveyQADto.getSoundsNatural());
		 }
		 else{
			 hoveyQA.setSoundsNatural(false);
		 }
		 if(null!=hoveyQADto.getSticksPitch()){
			 hoveyQA.setSticksPitch(hoveyQADto.getSticksPitch());
		 }
		 else{
			 hoveyQA.setSticksPitch(false);
		 }
		 if(null!=hoveyQADto.getProfessionalism()){
			 hoveyQA.setProfessionalism(hoveyQADto.getProfessionalism());
		 }
		 else{
			 hoveyQA.setProfessionalism(false);
		 }
		 if(null!=hoveyQADto.getSoundsConfident()){
			 hoveyQA.setSoundsConfident(hoveyQADto.getSoundsConfident());
		 }
		 else{
			 hoveyQA.setSoundsConfident(false);
		 }
		 if(null!=hoveyQADto.getReassuresSincerely()){
			 hoveyQA.setReassuresSincerely(hoveyQADto.getReassuresSincerely());
		 }
		 else{
			 hoveyQA.setReassuresSincerely(false);
		 }
		 if(null!=hoveyQADto.getAssumptiveSwagger()){
			 hoveyQA.setAssumptiveSwagger(hoveyQADto.getAssumptiveSwagger());
		 }
		 else{
			 hoveyQA.setAssumptiveSwagger(false);
		 }
		 if(null!=hoveyQADto.getFullDisclosure()){
			 hoveyQA.setFullDisclosure(hoveyQADto.getFullDisclosure());
		 }
		 else{
			 hoveyQA.setFullDisclosure(false);
		 }
		 if(null!=hoveyQADto.getReDirect()){
			 hoveyQA.setReDirect(hoveyQADto.getReDirect());
		 }
		 else{
			 hoveyQA.setReDirect(false);
		 }
		 if(null!=hoveyQADto.getOfAccounts()){
			 hoveyQA.setOfAccounts(hoveyQADto.getOfAccounts());
		 }
		 else{
			 hoveyQA.setOfAccounts(false);
		 }
		 if(null!=hoveyQADto.getAccounts()){
			 hoveyQA.setAccounts(hoveyQADto.getAccounts());
		 }
		 else{
			 hoveyQA.setAccounts(false);
		 }
		 if(null!=hoveyQADto.getRate()){
			 hoveyQA.setRate(hoveyQADto.getRate());
		 }
		 else{
			 hoveyQA.setRate(false);
		 }
		 if(null!=hoveyQADto.getTpv()){
			 hoveyQA.setTpv(hoveyQADto.getTpv());
		 }
		 else{
			 hoveyQA.setTpv(false);
		 }
		 if(null!=hoveyQADto.getPermission()){
			 hoveyQA.setPermission(hoveyQADto.getPermission());
		 }
		 else{
			 hoveyQA.setPermission(false);
		 }
		 if(null!=hoveyQADto.getCustName()){
			 hoveyQA.setCustName(hoveyQADto.getCustName());
		 }
		 else{
			 hoveyQA.setCustName(false);
		 }
		 if(null!=hoveyQADto.getCompanyName()){
			 hoveyQA.setCompanyName(hoveyQADto.getCompanyName());
		 }
		 else{
			 hoveyQA.setCompanyName(false);
		 }
		 if(null!=hoveyQADto.getBilling()){
			 hoveyQA.setBilling(hoveyQADto.getBilling());
		 }
		 else{
			 hoveyQA.setBilling(false);
		 }
		 if(null!=hoveyQADto.getAuthorization()){
			 hoveyQA.setAuthorization(hoveyQADto.getAuthorization());
		 }
		 else{
			 hoveyQA.setAuthorization(false);
		 }
		 if(null!=hoveyQADto.getConfirmAccounts()){
			 hoveyQA.setConfirmAccounts(hoveyQADto.getConfirmAccounts());
		 }
		 else{
			 hoveyQA.setConfirmAccounts(false);
		 }
		 if(null!=hoveyQADto.getChampionSupplier()){
			 hoveyQA.setChampionSupplier(hoveyQADto.getChampionSupplier());
		 }
		 else{
			 hoveyQA.setChampionSupplier(false);
		 }
		 if(null!=hoveyQADto.getRescission()){
			 hoveyQA.setRescission(hoveyQADto.getRescission());
		 }
		 else{
			 hoveyQA.setRescission(false);
		 }
		 if(null!=hoveyQADto.getConfirmDecission()){
			 hoveyQA.setConfirmDecission(hoveyQADto.getConfirmDecission());
		 }
		 else{
			 hoveyQA.setConfirmDecission(false);
		 }
		 if(null!=hoveyQADto.getProperFinish()){
			 hoveyQA.setProperFinish(hoveyQADto.getProperFinish());
		 }
		 else{
			 hoveyQA.setProperFinish(false);
		 }
		 /**
		  * Added transition,wrapup,courtesy call fields  by bhagya on august 05th,2015
		  * As per client Requirement
		  */
		 if(null!=hoveyQADto.getTransition()){
			 hoveyQA.setTransition(hoveyQADto.getTransition());
		 }
		 else{
			 hoveyQA.setTransition(false);
		 }
		 if(null!=hoveyQADto.getWrapUp()){
			 hoveyQA.setWrapUp(hoveyQADto.getWrapUp());
		 }
		 else{
			 hoveyQA.setWrapUp(false);
		 }
		 if(null!=hoveyQADto.getNeedCourtesyCall()){
			 hoveyQA.setNeedCourtesyCall(hoveyQADto.getNeedCourtesyCall());
		 }
		 else{
			 hoveyQA.setNeedCourtesyCall(false);
		 }
		 if(null!=hoveyQADto.getCourtesyCallCompleted()){
			 hoveyQA.setCourtesyCallCompleted(hoveyQADto.getCourtesyCallCompleted());
		 }
		 else{
			 hoveyQA.setCourtesyCallCompleted(false);
		 }
		 hoveyQA.setComments(hoveyQADto.getComments());
		 hoveyQA.setQaStatus(hoveyQADto.getQaStatus());
		 //setting the reportedby value - added by bhagya on march 23rd,2020 as per client requirement
		 hoveyQA.setReportedBy(hoveyQADto.getReportedBy());
		 
		 // setting the following fields by bhagya on June 01st,2021 as per client requirement
		 
		 if(null!=hoveyQADto.getPhoneNumber()){
			 hoveyQA.setPhoneNumber(hoveyQADto.getPhoneNumber());
		 }
		 else{
			 hoveyQA.setPhoneNumber(false);
		 }
		 if(null!=hoveyQADto.getState()){
			 hoveyQA.setState(hoveyQADto.getState());
		 }
		 else{
			 hoveyQA.setState(false);
		 }
		 if(null!=hoveyQADto.getProductId()){
			 hoveyQA.setProductId(hoveyQADto.getProductId());
		 }
		 else{
			 hoveyQA.setProductId(false);
		 }
		
		 if(null!=hoveyQADto.getConfirmationNumber()){
			 hoveyQA.setConfirmationNumber(hoveyQADto.getConfirmationNumber());
		 }
		 else{
			 hoveyQA.setConfirmationNumber(false);
		 }
		 
		 if(null!=hoveyQADto.getFullName()){
			 hoveyQA.setFullName(hoveyQADto.getFullName());
		 }
		 else{
			 hoveyQA.setFullName(false);
		 }
		 if(null!=hoveyQADto.getTitle()){
			 hoveyQA.setTitle(hoveyQADto.getTitle());
		 }
		 else{
			 hoveyQA.setTitle(false);
		 }
		
		 if(null!=hoveyQADto.getAccountNumberVerification()){
			 hoveyQA.setAccountNumberVerification(hoveyQADto.getAccountNumberVerification());
		 }
		 else{
			 hoveyQA.setAccountNumberVerification(false);
		 }
		 if(null!=hoveyQADto.getPassThroughVaraibleProduct()){
			 hoveyQA.setPassThroughVaraibleProduct(hoveyQADto.getPassThroughVaraibleProduct());
		 }
		 else{
			 hoveyQA.setPassThroughVaraibleProduct(false);
		 }
		 if(null!=hoveyQADto.getTermsETFs()){
			 hoveyQA.setTermsETFs(hoveyQADto.getTermsETFs());
		 }
		 else{
			 hoveyQA.setTermsETFs(false);
		 }
		
		 if(null!=hoveyQADto.getCompleteTPV()){
			 hoveyQA.setCompleteTPV(hoveyQADto.getCompleteTPV());
		 }
		 else{
			 hoveyQA.setCompleteTPV(false);
		 }
		 /* End of modification on June 01st, 2021*/
		 
		 if(hoveyQADto.getQaStatus()==true){
			 order.setQaStatus("pass");
		 }
		 else{
			 order.setQaStatus("fail");
		 }
		
		 this.adminDao.editPipelineData(order);
		 Integer savedResult=this.qaDao.saveOrUpdateHoveyQADetails(hoveyQA);
		return savedResult;
	}
	/**
	 * Created By Bhagya On July 02nd,2015 
	 * @param dealId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the order by deal ID
	 */
	public OrdersDto getOrderDtoByDealId(Integer dealId) throws Exception{
		log.info("inside getOrderDtoByDealId()");
		ArrayList<Orders> orders=this.dealSheetDao.getOrdersofATrandsaction(dealId);
		OrdersDto orderDto=OrdersDto.populateOrderDto(orders.get(0));
		return orderDto;
	}
	/**
	 * Created By Bhagya On July 02nd,2015
	 * @param orderId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting order by orderId
	 */
	public OrdersDto getOrderDtoByOrderId(Integer orderId) throws Exception{
		log.info("inside getOrderDtoByDealId()");
		Orders order=this.adminDao.getOrderByOrderId(orderId);
		OrdersDto orderDto=OrdersDto.populateOrderDto(order);
		return orderDto;
	}
	/**
	 * Created By BHagya On JUly 06th,2015
	 * @param dealId
	 * @return
	 * @throws QATransactionNotFoundException
	 * 
	 * Populating the transaction dto and returning that dto
	 */
	public TransactionsDto getQATransactionDtoByDealId(Integer dealId) throws QATransactionNotFoundException{
		log.info("inside getQATransactionDtoByDealId()");
		Transactions transaction=this.qaDao.getQATransactionByDealId(dealId);
		TransactionsDto transactionDto=new TransactionsDto();
		if(null!= transaction.getId()){
			transactionDto.setTransactionId(transaction.getId());
		}
		if(null!=transaction.getQaDealStatus()){
			transactionDto.setQaDealStatus(transaction.getQaDealStatus());
		}
		if(null!=transaction.getQaOrderStatus()){
			transactionDto.setQaOrderStatus(transaction.getQaOrderStatus());
		}
		return transactionDto;
	}
	/***
	 * Created By Bhagya On july 06th,2015
	 * @param transactionId
	 * @return
	 * @throws Exception
	 * 
	 * Method For getting the qa deal sheets by deal id
	 * In order dto here we are setting hovey qa dto of order
	 */
	public ArrayList<OrdersDto> getQADealSheetByDealId(int transactionId)throws Exception{
			log.info("inside getDealSheetByTransactionIdFromDB()");
			ArrayList<Orders> orders=this.dealSheetDao.getOrdersofATrandsaction(transactionId);
			ArrayList<OrdersDto> orderDtos=new ArrayList<OrdersDto>();
			
		    if(!orders.isEmpty()){
		    	Boolean isQAPartialReady=false;
		    	for(Orders order:orders){
		    	 	OrdersDto orderDto=OrdersDto.populateOrderDto(order);
		    	 	HoveyQA hoveyQAReport =this.qaDao.getQAReportDetailsByOrder(order);
					if( hoveyQAReport!=null){
		    	 		HoveyQADto hoveyQADto=HoveyQADto.populateQAFormDto(hoveyQAReport);
		    	 		orderDto.setHoveyQA(hoveyQADto);
		    	 	}
					if(null==order.getQaReady()){
						if(null==order.getQaStatus() || order.getQaStatus().equalsIgnoreCase("NOT READY")){
							isQAPartialReady=true;
						}
					}
					else{
						isQAPartialReady=false;
					}
					
					orderDto.setIsQAPartialReady(isQAPartialReady);
					orderDtos.add(orderDto);
		    	}
		    }
		    return orderDtos;
		}
	
	/**
	 * Created By bhagya On June 26th,2015
	 * @param transactions
	 * @param orders
	 * @return
	
	 * Modified By Bhagya On July 09th,2015
	 * 	Setting the QA status for deal, In a deal if any order status is fail means,setting the qaDealStatus As Fail
	 * 	SETTING the qaPerform status for deal,in a deal if any order QA is not Done means,setting it as "no"
	 * 
	 * Modified on 26th August 2015
	 * Setting dealIsNotRescinded for deal, In a deal if any order is not equal to rescinded means ,the dealIsNotRescinded As true
	 * If dealIsNotRescinded eq true means then only we add it orders dto
	 * 
	 */
	public ArrayList<OrdersDto> getDealSheets(ArrayList<Transactions> transactions,ArrayList<OrdersDto> orders){
		log.info("inside getDealSheets()");
		ArrayList<OrdersDto> orderDtos=new ArrayList<OrdersDto>();
		for(Transactions transaction:transactions){	
			ArrayList<OrdersDto> tempOrders=new ArrayList<OrdersDto>();
			Long totalKwh=0L;
			String qaDealStatus=null;
			String qaDealPerform=null;
			Boolean isQAPartialReady=false;
			Boolean dealIsNotRescinded=false;
			for(OrdersDto order:orders){
				if(transaction.getId().equals(order.getTransDto().getTransactionId())){						
					tempOrders.add(order);		
					totalKwh+=order.getKwh();
					if(null!=order.getQaStatus()){
						if(order.getQaStatus().equalsIgnoreCase("fail")){
							qaDealStatus="fail";
						}
					}
					else{
						qaDealPerform="no";
					}
					if(null!=order.getTransDto().getQaDealStatus() && order.getTransDto().getQaDealStatus()==false){
						if(null!=order.getQaReady() && order.getQaReady()==true){
							isQAPartialReady=true;
						}
					}
					if(null!=order.getStatus() && !order.getStatus().equalsIgnoreCase("Rescinded")){
						dealIsNotRescinded=true;
					}
					
				}			
			}				
			OrdersDto ordersDto=tempOrders.get(0);
			ordersDto.setTotalAccounts(tempOrders.size());	
			ordersDto.setTotalKwh(totalKwh);
			ordersDto.setIsQAPartialReady(isQAPartialReady);
			if(null!=qaDealStatus){
			ordersDto.setQaDealStatus(qaDealStatus);
			}
			if(null!=qaDealPerform){
				ordersDto.setQaDealPerform(qaDealPerform);
			}
			if(dealIsNotRescinded==true){
				orderDtos.add(ordersDto);
			}
			
		}	
			
		return orderDtos;
	
	}
	
	
	/**
	 * Created By Bhagya On July 13th,2015
	 * @param dealStartDate
	 * @param dealEndDate
	 * @return
	 * @throws OrderNotFoundException
	 * 
	 * Method For getting the Orders Between StartDate and End Date,and calculating the percentage for pass
	 */
	
	public ArrayList<QAReportDto> getOrdersDetailsForReportsBetweenStartDateAndEndDate(Date startDate,Date endDate) throws OrderNotFoundException{
		log.info("inside getOrdersDetailsForReportsBetweenStartDateAndEndDate()");
		ArrayList<Orders> orders=this.qaDao.getAllOrdersBetweenStartDateAndEndDate(startDate, endDate);
		ArrayList<QAReportDto> qaReportDtos=new ArrayList<QAReportDto>();
	
		Integer passCount=0;
		Integer failCount=0;
		Integer pendingCount=0;
		Double passPercentage,failPercentage=0.0;
		Double formatPassPerc=0.0;
		Double formatFailPerc=0.0;
		for(Orders order:orders){
			QAReportDto qaReportDto=new QAReportDto();
			qaReportDto.setOrderId(order.getOrderId());
			qaReportDto.setOrderDate(order.getOrderDate());
			qaReportDto.setAccountNumber(order.getAccountNumber());
			qaReportDto.setDealStartDate(order.getDealStartDate());
			qaReportDto.setBusinessName(order.getBusinessName());
			qaReportDto.setCreatedAgent(HoveyUserDto.populateHoveyUserDto(order.getAgent()));
			qaReportDto.setSupplierName(SupplierDto.populateSupplier(order.getSupplierName()));
			
			
		
			
			if(null!=order.getQaStatus()){
				if(order.getQaStatus().equalsIgnoreCase("pass")){
					passCount=passCount+1;
					
				}
				if(order.getQaStatus().equalsIgnoreCase("fail")){
					failCount=failCount+1;
					
				}
				qaReportDto.setQaStatus(order.getQaStatus());
			}
			else{
				pendingCount=pendingCount+1;
				qaReportDto.setQaStatus("pending");
			}
			Integer	totalValue=passCount+failCount;
			if(passCount==0 && failCount==0 ){
				passPercentage=0.0;
			}
			else{
			 passPercentage=(passCount/totalValue.doubleValue())*100;
			}
			qaReportDto.setPassCount(passCount);
			qaReportDto.setFailCount(failCount);
			qaReportDto.setPendingCount(pendingCount);
			DecimalFormat df = new DecimalFormat("###.##");
			qaReportDto.setPassPercentage(Double.valueOf(df.format(passPercentage)));
			
			formatPassPerc=qaReportDto.getPassPercentage();
			failPercentage=-100.00-qaReportDto.getPassPercentage();
			DecimalFormat df2 = new DecimalFormat("###.##");
			qaReportDto.setFailPercentage(Double.valueOf(df2.format(failPercentage)));
			formatFailPerc=qaReportDto.getFailPercentage();
			qaReportDto.setReportStartDate(startDate);
			qaReportDto.setReportEndDate(endDate);
			// Added ReportedBy column by bhagya on march 24th, 2020.. based on client requirement
			HoveyQA hoveyqa=qaDao.getQAReportDetailsByOrder(order);
			if(null!=hoveyqa){
				if(null!=hoveyqa.getReportedBy()){
					qaReportDto.setReportedBy(hoveyqa.getReportedBy());
				}
				else{
					qaReportDto.setReportedBy("");
				}
			}
			else{
					qaReportDto.setReportedBy("");
			}
			qaReportDtos.add(qaReportDto);
		}
		List<PieChartDto> pieChartDtos=new ArrayList<PieChartDto>();
		PieChartDto p1=new PieChartDto();
		p1.setQa("PASS");
		p1.setCount(passCount);
		pieChartDtos.add(p1);
		
		PieChartDto p2= new PieChartDto();
		p2.setQa("FAIL");
		p2.setCount(failCount);
		pieChartDtos.add(p2);
		
		qaReportDtos.get(qaReportDtos.size()-1).setPieData(pieChartDtos);
		
		
		return qaReportDtos;
	}
	
}