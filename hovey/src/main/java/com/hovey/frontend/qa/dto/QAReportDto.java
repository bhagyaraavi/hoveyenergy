package com.hovey.frontend.qa.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hovey.frontend.supplier.dto.SupplierDto;
import com.hovey.frontend.user.dto.HoveyUserDto;

/**
 *Created By Bhagya On July 13th,2015 
 *Dto For The QA Report
 */
public class QAReportDto{
	private Integer orderId;
	private Integer passCount;
	private Integer failCount;
	private Integer pendingCount;
	private Double passPercentage;
	private Double failPercentage;
	private Date dealStartDate;
	private Date orderDate;
	private String businessName;
	private String accountNumber;
	private HoveyUserDto createdAgent;
	private SupplierDto supplierName;
	private Date reportStartDate;
	private Date reportEndDate;
	private String qaStatus;
	
	
	private List<PieChartDto> pieData;
	private Integer count;
	
	// Added the Reported By field by bhagya on march24th,2020 bASED ON client requirement
	private String reportedBy;
	
	public Integer getPassCount() {
		return passCount;
	}
	public void setPassCount(Integer passCount) {
		this.passCount = passCount;
	}
	public Integer getFailCount() {
		return failCount;
	}
	public void setFailCount(Integer failCount) {
		this.failCount = failCount;
	}
	public Double getPassPercentage() {
		return passPercentage;
	}
	public void setPassPercentage(Double passPercentage) {
		this.passPercentage = passPercentage;
	}
	
	public Double getFailPercentage() {
		return failPercentage;
	}
	public void setFailPercentage(Double failPercentage) {
		this.failPercentage = failPercentage;
	}
	public Date getDealStartDate() {
		return dealStartDate;
	}
	public void setDealStartDate(Date dealStartDate) {
		this.dealStartDate = dealStartDate;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public HoveyUserDto getCreatedAgent() {
		return createdAgent;
	}
	public void setCreatedAgent(HoveyUserDto createdAgent) {
		this.createdAgent = createdAgent;
	}
	public SupplierDto getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(SupplierDto supplierName) {
		this.supplierName = supplierName;
	}
	public Date getReportStartDate() {
		return reportStartDate;
	}
	public void setReportStartDate(Date reportStartDate) {
		this.reportStartDate = reportStartDate;
	}
	public Date getReportEndDate() {
		return reportEndDate;
	}
	public void setReportEndDate(Date reportEndDate) {
		this.reportEndDate = reportEndDate;
	}
	public String getQaStatus() {
		return qaStatus;
	}
	public void setQaStatus(String qaStatus) {
		this.qaStatus = qaStatus;
	}
	public Integer getPendingCount() {
		return pendingCount;
	}
	public void setPendingCount(Integer pendingCount) {
		this.pendingCount = pendingCount;
	}
	
	
	
	
	public List<PieChartDto> getPieData() {
		return pieData;
	}
	public void setPieData(List<PieChartDto> pieData) {
		this.pieData = pieData;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getReportedBy() {
		return reportedBy;
	}
	public void setReportedBy(String reportedBy) {
		this.reportedBy = reportedBy;
	}
	
	
}