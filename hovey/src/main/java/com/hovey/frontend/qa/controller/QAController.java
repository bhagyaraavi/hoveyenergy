package com.hovey.frontend.qa.controller;
/**
 * Created By bhagya On June 26th,2015
 * Controller For the QA Module
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.AutoPopulatingList;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hovey.backend.agent.exception.OrderNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotFoundException;
import com.hovey.backend.qa.exception.QATransactionNotFoundException;
import com.hovey.backend.qa.model.HoveyQA;
import com.hovey.frontend.admin.service.AdminService;
import com.hovey.frontend.agent.dto.BillingStateDto;
import com.hovey.frontend.agent.dto.CustomerDto;
import com.hovey.frontend.agent.dto.DealSheetForm;
import com.hovey.frontend.agent.dto.OrdersDto;
import com.hovey.frontend.agent.dto.StateDto;
import com.hovey.frontend.agent.dto.TransactionsDto;
import com.hovey.frontend.agent.dto.UtilityDto;
import com.hovey.frontend.agent.service.AgentService;
import com.hovey.frontend.agent.service.DealSheetService;
import com.hovey.frontend.qa.dto.HoveyQADto;
import com.hovey.frontend.qa.dto.QAReportDto;
import com.hovey.frontend.qa.service.QAService;
import com.hovey.frontend.supplier.dto.SupplierDto;
import com.hovey.frontend.supplier.service.SupplierService;
import com.hovey.frontend.user.dto.HoveyUserDto;
import com.hovey.frontend.user.service.UserService;

@Controller
public class QAController{
	private static Logger log=Logger.getLogger(QAController.class);
	
	@Resource(name="qaService")
	private QAService qaService;
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="adminService")
	private AdminService adminService;
	
	@Resource(name="dealSheetService")
	private DealSheetService dealSheetService;
	
	@Resource(name="supplierService")
	private SupplierService supplierService;
	
	@Resource(name="agentService")
	private AgentService agentService;
	
	
	/*
	 * 
	 *  for Handling conditions when Date may be null, useful while editing a Pipeline, where date may be entered for certain Orders..
	 *  Comes Handy to accept Null Meter Read Valuez
	 *  */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    dateFormat.setLenient(false);

	    // true passed to CustomDateEditor constructor means convert empty String to null
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/**
	 * Created By Bhagya On June 26th,2015	
	 * @param map
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @return
	 * 
	 * Method for getting the QA deal sheets
	 * At services ,we are passing the completedDeal value as false,because here we are
	 * getting the QA not done deals
	 */
			
	@RequestMapping(value="/qa/viewdealsheets.do",method=RequestMethod.GET)		
	public String getQADealSheets(Map<String,Object> map,@RequestParam(value="page",required=false)Integer pageNumber,@RequestParam(value="range",
			defaultValue="20",required=false)Integer pageSize,@RequestParam(value="sortby",required=false,defaultValue="orderDate")String sortBy,
			@RequestParam(value="searchBy",required=false) String searchBy){
		log.info("inside getQADealSheets()");
		int pagesNeeded;
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();				
			String user=auth.getName();					
			HoveyUserDto userDto=this.userService.getUserByUsername(user);
			map.put("user",userDto);
			if(null==pageNumber){
				pageNumber=0;
			}
			pagesNeeded=this.qaService.findTotalNoOfQADealPages(pageSize, searchBy,false,false,null);
			ArrayList<OrdersDto> orderDtos=this.qaService.gettingQADealSheetsFromDao(pageNumber, pageSize, sortBy, searchBy,false,false,null);
			int firstDeal=pageSize*pageNumber+1;;
			int totalDeals=this.qaService.getTotalQADealSheetsCount(searchBy,false,false,null);
			int lastDeal;
			if(!orderDtos.isEmpty()){
				lastDeal=this.getOrdersOfLastPageFromRangeandTotalOrders(pageSize, totalDeals, firstDeal);
				map.put("first", firstDeal);
				map.put("last", lastDeal);
				map.put("total", totalDeals);
				map.put("orders", orderDtos);
				map.put("page",pageNumber);
				map.put("end", pagesNeeded);
				map.put("sortby", sortBy);
				map.put("searchBy", searchBy);
				return "qa/viewQADealSheets";
			}
			else{
				throw new OrderNotFoundException();
			}
			
		}
		catch(OrderNotFoundException e){
			log.error("QA Orders Not Found for Current Agent");
			String message="No QA Deals Found";
			map.put("message", message);
			map.put("searchBy", searchBy);
			return "qa/viewQADealSheets";
		}
		catch(Exception e){
			log.error("Error while getting QA DealSheets"+e.toString());
			String message="Error in getting QA DealSheets ";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On June 26th,2015
	 * 
	 * @param range
	 * @param totalOrders
	 * @param firstOrder
	 * @return
	 * for Calculating Deals of LAst Page Based on Range and Total Records
	 */
	private int getOrdersOfLastPageFromRangeandTotalOrders(int range,int totalDeals,int firstDeal){
		log.info("inside getOrdersOfLastPageFromRangeandTotalOrders()	");
		int lastDeal=firstDeal+range-1;
		if(lastDeal>totalDeals){
			lastDeal=(totalDeals%range)+firstDeal-1;
		}
		return lastDeal;
	}
	
	
	/**
	 * Created By Bhagya On June 30th,2015
	 * @return
	 * Setting Deal Sheet as model Attribute..
	 */
	 
		@ModelAttribute("dealSheet")
	    public DealSheetForm getDealSheetForm()
	    {
			DealSheetForm dealSheet=new DealSheetForm();
			dealSheet.setOrders(new AutoPopulatingList<OrdersDto>(OrdersDto.class));		
			return dealSheet;
	    }
	/**
	 * Created By Bhagya On June 30th,2015
	 * @param map
	 * @param request
	 * @param transactionId
	 * @param error
	 * @return
	 * 
	 * Displaying a page to view the Deal Sheet, it Will be used by QA for adding QA information for each order..
	 */
	
    @SuppressWarnings("unchecked")
	@RequestMapping(value="/qa/viewdealwithqa.do",method=RequestMethod.GET)
  public String displayDealSheetToView(Map<String,Object> map,HttpServletRequest request,@RequestParam("dealId") Integer transactionId,
		  @RequestParam(value="error",required=false)String error){
	  log.info("inside displayDealSheetToView()");
	  try{	
		  Authentication auth=SecurityContextHolder.getContext().getAuthentication();				
		  String user=auth.getName();		
		  HoveyUserDto userDto=this.userService.getUserByUsername(user);
		  map.put("user",userDto);
		  ArrayList<OrdersDto> orderDtos=this.dealSheetService.getDealSheetByTransactionIdFromDB(transactionId);				
		  ArrayList<CustomerDto> customerDtos=this.dealSheetService.getCustomersFromDb();			 
		  ArrayList<UtilityDto> utilDtos=this.dealSheetService.getUtilitiesFromDb();
		  ArrayList<StateDto> stateDtos=this.dealSheetService.getStatesFromDao();
		  ArrayList<BillingStateDto> billingStateDtos=this.dealSheetService.getBillingStatesFromDao();
		  ArrayList<SupplierDto> supplierDtos=this.supplierService.getSuppliersFromDAO();
		  ArrayList<HoveyUserDto> hoveyUserDtos=this.agentService.getAgentsFromDao("", null, null);
		  Map<String, Object> customerMap=this.dealSheetService.getSortedCustomerDetails(customerDtos);
		  ArrayList<String> firstNames=(ArrayList<String>) customerMap.get("firstNames");
		  ArrayList<String> lastNames=(ArrayList<String>) customerMap.get("lastNames");
		  ArrayList<String> taxIds=(ArrayList<String>) customerMap.get("taxIds");
		  ArrayList<String> phoneNos=(ArrayList<String>) customerMap.get("phoneNos");
		  ArrayList<String>businessNames=this.adminService.getBusinessNames();
		  map.put("businessNames", businessNames);				
		  map.put("firstNames", firstNames);
		  map.put("lastNames", lastNames);
		  map.put("taxIds", taxIds);
		  map.put("phoneNos", phoneNos);
		  map.put("agents",hoveyUserDtos);
		  map.put("suppliers", supplierDtos);
		  map.put("billingStates", billingStateDtos);
		  map.put("states", stateDtos);
		  map.put("utilities",utilDtos);
		  map.put("customers", customerDtos);
		  map.put("orders", orderDtos);
		  map.put("dealId", transactionId);
		  if(null!=error && error!=""){
			  map.put("error", error);
		  }
		  return "qa/viewDealAndQA";			  
	  }
	  catch(OrderNotFoundException e){
		  String message="Error While Viewing Deal Sheet, No Deal Found";
		  log.info(message+e.toString());
		  map.put("message", message);
		  return "error";
	  }
	  catch(Exception e){
		  e.printStackTrace();
		  String message="Error While Viewing Deal Sheet";
		  log.info(message+e.toString());
		  map.put("message", message);
		  return "error";
	  } 
  }

    /**
	 *Created By Bhagya On June 29th,2015
	 *Method for getting the add QA Form and Edit QA Form
     * @throws Exception 
     * 1.if dealId is not equal to zero means,getting the hovey qa report by dealId
     * 2.if orderId is not equal to zero means,getting the hovey qa report by orderId
     * 3.If hovey QA Report is not yet filled by qa user or not exists in db means,it throws exception as qa report found
     * 		In HoveyQA Report not found catch condition we are handling 
	 */
	
	@RequestMapping(value="/qa/addqa.do",method=RequestMethod.GET)
	public String getQAForm(Map<String,Object> map,@RequestParam(value="dealId",required=false) Integer dealId,@RequestParam(value="orderId",required=false) Integer orderId,
			@ModelAttribute("hoveyQA") HoveyQADto hoveyQADto, @RequestParam(value="counter",defaultValue="1",required=false)Integer counter) throws Exception{
		log.info("inside getQAForm()");
		map.put("counter", counter);
		try{
			HoveyQADto qaReportDto=null;
			
			if(dealId!=0){
				 qaReportDto=this.qaService.gettingQAReportForDeal(dealId);
			}
			if(orderId!=0){
				 qaReportDto=this.qaService.gettingQAReportForOrder(orderId);
			}
			map.put("dealId", dealId);
			map.put("orderId", orderId);
			map.put("qaReport", qaReportDto);
			return"qa/editQAForm";
			
		}
		catch(HoveyQAReportNotFoundException e){
			OrdersDto orderDto=null;
			if(dealId!=0){
				 orderDto=this.qaService.getOrderDtoByDealId(dealId);
			}
			else{
				orderDto=this.qaService.getOrderDtoByOrderId(orderId);
			}
			map.put("dealId", dealId);
			map.put("orderId", orderId);
			map.put("agentName",orderDto.getCreatedAgent().getFirstName()+" "+orderDto.getCreatedAgent().getLastName());
			map.put("businessName", orderDto.getBusinessName());
			map.put("phoneNo", orderDto.getTaxId().getPhoneNo());
			return "qa/addQAForm";
		}
		catch(Exception e){
			log.error("Error while getting QA Form"+e.toString());
			String message="Error while getting QA Form ";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
	}
	/**
	 * Created By Bhagya On July 01st,2015
	 * @param map
	 * @param QAForm
	 * @return
	 * 
	 * Method For Saving the QA Form Details
	 */
	@RequestMapping(value="/qa/addqa.do",method=RequestMethod.POST)
	public String saveQAForm(Map<String,Object> map,@ModelAttribute("hoveyQA") HoveyQADto hoveyQADto,HttpServletRequest request){
		log.info("inside saveQAForm()");
		try{
			String dealIdValue=request.getParameter("dealId");
			String orderIdValue=request.getParameter("orderId");
			Integer dealId=Integer.parseInt(dealIdValue);
			Integer orderId=Integer.parseInt(orderIdValue);
			Integer savedQAResult=this.qaService.saveQAReportDetails(dealId, orderId, hoveyQADto);
			if(dealId!=0){
				if(null!=hoveyQADto.getQaId()){
				map.put("status", "QA Status Submited Sucessfully For Deal");
				return "redirect:/qa/viewcompleteddealsheets.do";
				}
				else{
					map.put("status", "QA Status Submited Sucessfully For Deal");
					return "redirect:/qa/viewdealsheets.do";
				}
			}
			else{
				OrdersDto orderDto=this.qaService.getOrderDtoByOrderId(orderId);
				map.put("dealId", orderDto.getTransDto().getTransactionId());
				map.put("message", "QA Status Submitted Sucessfully For Order");
				return "redirect:/qa/viewdealwithqa.do";
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while Saving QA Form"+e.toString());
			String message="Error while Saving QA Form ";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On july 06th,2016
	 * @return
	 * Method to Print Qa Deal
	 * This Method is used for printing the qa deal for both qa user and admin
	 * 
	 */
	@RequestMapping(value={"/qa/printdealqaform.do", "/admin/printdealqaform.do"},method=RequestMethod.GET)
	public String printDealQAForm(@RequestParam("dealId") int dealId,Map<String, Object> map){
		log.info("inside printDealQAForm()");
		try{
			ArrayList<OrdersDto> orderDtos=this.qaService.getQADealSheetByDealId(dealId);
			map.put("orders", orderDtos);
			TransactionsDto transactionsDto=this.qaService.getQATransactionDtoByDealId(dealId);
			if(transactionsDto.getQaOrderStatus()==false){
				HoveyQADto qaDto=this.qaService.gettingQAReportForDeal(dealId);
				map.put("qaReport", qaDto);
			}
			return "qa/printDealQAForm";
		}
		catch(QATransactionNotFoundException e){
			String message="Error While Print QA Deal Sheet, No Deal Found";
			log.info(message+e.toString());
			map.put("message", message);
			return "error";
		}
		catch(HoveyQAReportNotFoundException e){
			try{
			ArrayList<OrdersDto> orderDtos=this.qaService.getQADealSheetByDealId(dealId);
			map.put("orders", orderDtos);
			}
			catch(Exception e1){
			}
			
			map.put("qamessage", "QA Status Not yet Performed");
			return "qa/printDealQAForm";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Failed to Print QA Deal Sheet "+e.toString());
			String message="Error in Printing QA Deal Sheet";
			map.put("message", message);
			return "error";
		}
		
	}
	
	
	/**
	 * Created By Bhagya On July 09th,2015	
	 * @param map
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @return
	 * 
	 * Method for getting the Completed QA deal sheets
	 * 
	 * At services ,we are passing completedDeal boolean value as a true,Because Here
	 * we are getting the QA Completed or done Deals
	 */
			
	@RequestMapping(value="/qa/viewcompleteddealsheets.do",method=RequestMethod.GET)		
	public String getQACompletedDealSheets(Map<String,Object> map,@RequestParam(value="page",required=false)Integer pageNumber,@RequestParam(value="range",
			defaultValue="20",required=false)Integer pageSize,@RequestParam(value="sortby",required=false,defaultValue="orderDate")String sortBy,
			@RequestParam(value="searchBy",required=false) String searchBy,@RequestParam(value="statusFilter",required=false) String statusFilter){
		log.info("inside getQACompletedDealSheets()");
		int pagesNeeded;
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();				
			String user=auth.getName();					
			HoveyUserDto userDto=this.userService.getUserByUsername(user);
			map.put("user",userDto);
			if(null==pageNumber){
				pageNumber=0;
			}
			pagesNeeded=this.qaService.findTotalNoOfQADealPages(pageSize, searchBy,true,false,statusFilter);
			ArrayList<OrdersDto> orderDtos=this.qaService.gettingQADealSheetsFromDao(pageNumber, pageSize, sortBy, searchBy,true,false,statusFilter);
			int firstDeal=pageSize*pageNumber+1;;
			int totalDeals=this.qaService.getTotalQADealSheetsCount(searchBy,true,false,statusFilter);
			int lastDeal;
			if(!orderDtos.isEmpty()){
				lastDeal=this.getOrdersOfLastPageFromRangeandTotalOrders(pageSize, totalDeals, firstDeal);
				map.put("first", firstDeal);
				map.put("last", lastDeal);
				map.put("total", totalDeals);
				map.put("orders", orderDtos);
				map.put("page",pageNumber);
				map.put("end", pagesNeeded);
				map.put("sortby", sortBy);
				map.put("searchBy", searchBy);
				map.put("statusFilter", statusFilter);
				return "qa/viewCompletedQADealSheets";
			}
			else{
				throw new OrderNotFoundException();
			}
			
		}
		catch(OrderNotFoundException e){
			log.error("QA Deal Not Found");
			String message="No QA Deals Found";
			map.put("message", message);
			map.put("searchBy", searchBy);
			map.put("statusFilter", statusFilter);
			return "qa/viewCompletedQADealSheets";
		}
		catch(Exception e){
			log.error("Error while getting QA DealSheets"+e.toString());
			String message="Error in getting QA DealSheets ";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On July 09th,2015
	 * @param map
	 * @param pageNumber
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @return
	 * 
	 * Method for Getting the QA Deals for Admin
	 * In this page he can view the qa pending deals,pass and fail deals
	 */
	
	@RequestMapping(value="/admin/viewqadealsheets.do",method=RequestMethod.GET)		
	public String getQADealSheetsForAdmin(Map<String,Object> map,@RequestParam(value="page",required=false)Integer pageNumber,@RequestParam(value="range",
			defaultValue="20",required=false)Integer pageSize,@RequestParam(value="sortby",required=false,defaultValue="orderDate")String sortBy,
			@RequestParam(value="searchBy",required=false) String searchBy,@RequestParam(value="statusFilter",required=false) String statusFilter){
		log.info("inside getQACompletedDealSheets()");
		int pagesNeeded;
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();				
			String user=auth.getName();					
			HoveyUserDto userDto=this.userService.getUserByUsername(user);
			map.put("user",userDto);
			if(null==pageNumber){
				pageNumber=0;
			}
			pagesNeeded=this.qaService.findTotalNoOfQADealPages(pageSize, searchBy,null,true,statusFilter);
			ArrayList<OrdersDto> orderDtos=this.qaService.gettingQADealSheetsFromDao(pageNumber, pageSize, sortBy, searchBy,null,true,statusFilter);
			int firstDeal=pageSize*pageNumber+1;;
			int totalDeals=this.qaService.getTotalQADealSheetsCount(searchBy,null,true,statusFilter);
			int lastDeal;
			if(!orderDtos.isEmpty()){
				lastDeal=this.getOrdersOfLastPageFromRangeandTotalOrders(pageSize, totalDeals, firstDeal);
				map.put("first", firstDeal);
				map.put("last", lastDeal);
				map.put("total", totalDeals);
				map.put("orders", orderDtos);
				map.put("page",pageNumber);
				map.put("end", pagesNeeded);
				map.put("sortby", sortBy);
				map.put("statusFilter", statusFilter);
				map.put("searchBy", searchBy);
				return "admin/adminQADealSheets";
			}
			else{
				throw new OrderNotFoundException();
			}
			
		}
		catch(OrderNotFoundException e){
			log.error("QA Orders Not Found for Current Agent");
			String message="No QA Deals Found";
			map.put("message", message);
			map.put("sortby", sortBy);
			map.put("statusFilter", statusFilter);
			map.put("searchBy", searchBy);
			return "admin/adminQADealSheets";
		}
		catch(Exception e){
			log.error("Error while getting QA DealSheets"+e.toString());
			String message="Error in getting QA DealSheets ";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On July 13th,2015
	 * @param map
	 * @return
	 * 
	 * Method For Initiating the QA Report
	 */
	@RequestMapping(value="/qa/qareport.do",method=RequestMethod.GET)
	public String initiateQAReport(Map<String, Object> map){
		log.info("inside initiateQAReport()");
		try{
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();				
			String user=auth.getName();					
			HoveyUserDto userDto=this.userService.getUserByUsername(user);
			map.put("user",userDto);
			return "reports/qaReport";			
		}
		catch(Exception e){
			String message="Error in Initiating QA Reports";
			map.put("message", message);
			return "error";
		}
	}
	/**
	 * Created By bhagya On July 13th,2015
	 * @param map
	 * @param dealStartDate
	 * @param dealEndDate
	 * @return
	 * 
	 * Method For Getting the QA Orders Details For Generating the Report
	 */
	
	@RequestMapping(value="/qa/qareport.do",method=RequestMethod.POST)
	public String getOrdersDetailsForReports(Map<String,Object> map,@RequestParam("startDate") Date startDate,@RequestParam("endDate") Date endDate,
			@RequestParam(value="export",required=false)String export){
		log.info("inside getOrdersDeatailsForReports()");
		try{
		ArrayList<QAReportDto> qaReportDtos=this.qaService.getOrdersDetailsForReportsBetweenStartDateAndEndDate(startDate, endDate);
		if(null!=export && export.equalsIgnoreCase("excel")){
			Map<String, QAReportDto> qaOrdersData=new LinkedHashMap<String, QAReportDto>();
			int i=0;
			for(QAReportDto order:qaReportDtos){	
				
				qaOrdersData.put("order"+i, order);
				i++;
			}
			map.put("qaOrders", qaOrdersData);				
			return "qaOrdersXlsReport";
		}
		else{
			JRDataSource jrDataSource=new JRBeanCollectionDataSource(qaReportDtos);
			map.put("datasource", jrDataSource);
			return "qaOrdersReport";		
		}
		
		}
		catch(OrderNotFoundException e){
			log.error("QA Orders Not Found for Current Agent");
			String message="No QA Deals Found";
			map.put("message", message);
			return "reports/qaReport";
		}
		catch(Exception e){
			log.error("Error while getting QA DealSheets Report"+e.toString());
			String message="Error in getting QA DealSheets Report";
			map.put("message", message);
			e.printStackTrace();
			return "error";
		}
		
	}
}