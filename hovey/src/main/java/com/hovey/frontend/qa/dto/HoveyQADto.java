package com.hovey.frontend.qa.dto;

import java.util.Date;





import com.hovey.backend.qa.model.HoveyQA;
import com.hovey.frontend.agent.dto.OrdersDto;

/**
 * Created By Bhagya On June 30th,2015
 *	Dto for QA Checklist screen
 */
public class HoveyQADto{
	private Integer qaId;
	private OrdersDto order;
	private Date qaDate;
	private String qaScore;
	private Boolean brandsCall;
	private Boolean positiveTone;
	private Boolean soundsNatural;
	private Boolean sticksPitch;
	private Boolean professionalism;
	private Boolean soundsConfident;
	private Boolean reassuresSincerely;
	private Boolean assumptiveSwagger;
	private Boolean fullDisclosure;
	private Boolean reDirect;
	private Boolean ofAccounts; // number of accounts
	private Boolean accounts;
	private Boolean rate;
	private Boolean tpv;
	private Boolean permission;
	private Boolean custName;
	private Boolean companyName;
	private Boolean billing;
	private Boolean authorization;
	private Boolean confirmAccounts;
	private Boolean championSupplier;
	private Boolean rescission;
	private Boolean confirmDecission;
	private Boolean properFinish;
	private String comments;
	private Boolean qaStatus;
	
	/**
	 * Added By Bhagya On August 05th,2015
	 * AS per client requirement added these fields
	 */
	private Boolean transition;
	private Boolean wrapUp;
	private Boolean needCourtesyCall;
	private Boolean courtesyCallCompleted;
	
	/**
	 * Added By Bhagya on March 23rd, 2020
	 * As per client requirement added the  reportedBy field*/
	private String reportedBy;
	
	/**
	 * Added By Bhagya on June 01st,2021
	 * As per client requirement added the following fields
	 */
	private Boolean phoneNumber;
	private Boolean state;
	private Boolean productId;
	private Boolean confirmationNumber;
	private Boolean fullName;
	private Boolean title;
	private Boolean accountNumberVerification;
	private Boolean passThroughVaraibleProduct;
	private Boolean termsETFs;
	private Boolean completeTPV;
	
	public Integer getQaId() {
		return qaId;
	}
	public void setQaId(Integer qaId) {
		this.qaId = qaId;
	}
		
	public OrdersDto getOrder() {
		return order;
	}
	public void setOrder(OrdersDto order) {
		this.order = order;
	}
	public Date getQaDate() {
		return qaDate;
	}
	public void setQaDate(Date qaDate) {
		this.qaDate = qaDate;
	}
	public String getQaScore() {
		return qaScore;
	}
	public void setQaScore(String qaScore) {
		this.qaScore = qaScore;
	}
	public Boolean getBrandsCall() {
		return brandsCall;
	}
	public void setBrandsCall(Boolean brandsCall) {
		this.brandsCall = brandsCall;
	}
	public Boolean getPositiveTone() {
		return positiveTone;
	}
	public void setPositiveTone(Boolean positiveTone) {
		this.positiveTone = positiveTone;
	}
	public Boolean getSoundsNatural() {
		return soundsNatural;
	}
	public void setSoundsNatural(Boolean soundsNatural) {
		this.soundsNatural = soundsNatural;
	}
	public Boolean getSticksPitch() {
		return sticksPitch;
	}
	public void setSticksPitch(Boolean sticksPitch) {
		this.sticksPitch = sticksPitch;
	}
	public Boolean getProfessionalism() {
		return professionalism;
	}
	public void setProfessionalism(Boolean professionalism) {
		this.professionalism = professionalism;
	}
	public Boolean getSoundsConfident() {
		return soundsConfident;
	}
	public void setSoundsConfident(Boolean soundsConfident) {
		this.soundsConfident = soundsConfident;
	}
	public Boolean getReassuresSincerely() {
		return reassuresSincerely;
	}
	public void setReassuresSincerely(Boolean reassuresSincerely) {
		this.reassuresSincerely = reassuresSincerely;
	}
	public Boolean getAssumptiveSwagger() {
		return assumptiveSwagger;
	}
	public void setAssumptiveSwagger(Boolean assumptiveSwagger) {
		this.assumptiveSwagger = assumptiveSwagger;
	}
	public Boolean getFullDisclosure() {
		return fullDisclosure;
	}
	public void setFullDisclosure(Boolean fullDisclosure) {
		this.fullDisclosure = fullDisclosure;
	}
	public Boolean getReDirect() {
		return reDirect;
	}
	public void setReDirect(Boolean reDirect) {
		this.reDirect = reDirect;
	}
	public Boolean getOfAccounts() {
		return ofAccounts;
	}
	public void setOfAccounts(Boolean ofAccounts) {
		this.ofAccounts = ofAccounts;
	}
	public Boolean getAccounts() {
		return accounts;
	}
	public void setAccounts(Boolean accounts) {
		this.accounts = accounts;
	}
	public Boolean getRate() {
		return rate;
	}
	public void setRate(Boolean rate) {
		this.rate = rate;
	}
	public Boolean getTpv() {
		return tpv;
	}
	public void setTpv(Boolean tpv) {
		this.tpv = tpv;
	}
	public Boolean getPermission() {
		return permission;
	}
	public void setPermission(Boolean permission) {
		this.permission = permission;
	}
	public Boolean getCustName() {
		return custName;
	}
	public void setCustName(Boolean custName) {
		this.custName = custName;
	}
	public Boolean getCompanyName() {
		return companyName;
	}
	public void setCompanyName(Boolean companyName) {
		this.companyName = companyName;
	}
	public Boolean getBilling() {
		return billing;
	}
	public void setBilling(Boolean billing) {
		this.billing = billing;
	}
	public Boolean getAuthorization() {
		return authorization;
	}
	public void setAuthorization(Boolean authorization) {
		this.authorization = authorization;
	}
	public Boolean getConfirmAccounts() {
		return confirmAccounts;
	}
	public void setConfirmAccounts(Boolean confirmAccounts) {
		this.confirmAccounts = confirmAccounts;
	}
	public Boolean getChampionSupplier() {
		return championSupplier;
	}
	public void setChampionSupplier(Boolean championSupplier) {
		this.championSupplier = championSupplier;
	}
	public Boolean getRescission() {
		return rescission;
	}
	public void setRescission(Boolean rescission) {
		this.rescission = rescission;
	}
	public Boolean getConfirmDecission() {
		return confirmDecission;
	}
	public void setConfirmDecission(Boolean confirmDecission) {
		this.confirmDecission = confirmDecission;
	}
	public Boolean getProperFinish() {
		return properFinish;
	}
	public void setProperFinish(Boolean properFinish) {
		this.properFinish = properFinish;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Boolean getQaStatus() {
		return qaStatus;
	}
	public void setQaStatus(Boolean qaStatus) {
		this.qaStatus = qaStatus;
	}
	
	
	public Boolean getTransition() {
		return transition;
	}
	public void setTransition(Boolean transition) {
		this.transition = transition;
	}
	public Boolean getWrapUp() {
		return wrapUp;
	}
	public void setWrapUp(Boolean wrapUp) {
		this.wrapUp = wrapUp;
	}
	public Boolean getNeedCourtesyCall() {
		return needCourtesyCall;
	}
	public void setNeedCourtesyCall(Boolean needCourtesyCall) {
		this.needCourtesyCall = needCourtesyCall;
	}
	public Boolean getCourtesyCallCompleted() {
		return courtesyCallCompleted;
	}
	public void setCourtesyCallCompleted(Boolean courtesyCallCompleted) {
		this.courtesyCallCompleted = courtesyCallCompleted;
	}
	public String getReportedBy() {
		return reportedBy;
	}
	public void setReportedBy(String reportedBy) {
		this.reportedBy = reportedBy;
	}
	
	public Boolean getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(Boolean phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public Boolean getProductId() {
		return productId;
	}
	public void setProductId(Boolean productId) {
		this.productId = productId;
	}
	
	public Boolean getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(Boolean confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	
	public Boolean getFullName() {
		return fullName;
	}
	public void setFullName(Boolean fullName) {
		this.fullName = fullName;
	}
	public Boolean getTitle() {
		return title;
	}
	public void setTitle(Boolean title) {
		this.title = title;
	}
	
	public Boolean getAccountNumberVerification() {
		return accountNumberVerification;
	}
	public void setAccountNumberVerification(Boolean accountNumberVerification) {
		this.accountNumberVerification = accountNumberVerification;
	}
	public Boolean getPassThroughVaraibleProduct() {
		return passThroughVaraibleProduct;
	}
	public void setPassThroughVaraibleProduct(Boolean passThroughVaraibleProduct) {
		this.passThroughVaraibleProduct = passThroughVaraibleProduct;
	}
	public Boolean getTermsETFs() {
		return termsETFs;
	}
	public void setTermsETFs(Boolean termsETFs) {
		this.termsETFs = termsETFs;
	}
	
	public Boolean getCompleteTPV() {
		return completeTPV;
	}
	public void setCompleteTPV(Boolean completeTPV) {
		this.completeTPV = completeTPV;
	}
	public static HoveyQADto populateQAFormDto(HoveyQA hoveyQA){
		HoveyQADto hoveyQADto=new HoveyQADto();
		if(null!=hoveyQA.getQaId()){
			hoveyQADto.setQaId(hoveyQA.getQaId());
		}
		if(null!=hoveyQA.getOrder()){
			hoveyQADto.setOrder(OrdersDto.populateOrderDto(hoveyQA.getOrder()));
		}
		if(null!=hoveyQA.getQaDate()){
			hoveyQADto.setQaDate(hoveyQA.getQaDate());
		}
		hoveyQADto.setQaScore(hoveyQA.getQaScore());
		if(null!=hoveyQA.getBrandsCall()){
			hoveyQADto.setBrandsCall(hoveyQA.getBrandsCall());
		}
		if(null!=hoveyQA.getPositiveTone()){
			hoveyQADto.setPositiveTone(hoveyQA.getPositiveTone());
		}
		if(null!=hoveyQA.getSoundsNatural()){
			hoveyQADto.setSoundsNatural(hoveyQA.getSoundsNatural());
		}
		if(null!=hoveyQA.getSticksPitch()){
			hoveyQADto.setSticksPitch(hoveyQA.getSticksPitch());
		}
		if(null!=hoveyQA.getProfessionalism()){
			hoveyQADto.setProfessionalism(hoveyQA.getProfessionalism());
		}
		if(null!=hoveyQA.getSoundsConfident()){
			hoveyQADto.setSoundsConfident(hoveyQA.getSoundsConfident());
		}
		if(null!=hoveyQA.getReassuresSincerely()){
			hoveyQADto.setReassuresSincerely(hoveyQA.getReassuresSincerely());
		}
		if(null!=hoveyQA.getAssumptiveSwagger()){
			hoveyQADto.setAssumptiveSwagger(hoveyQA.getAssumptiveSwagger());
		}
		if(null!=hoveyQA.getFullDisclosure()){
			hoveyQADto.setFullDisclosure(hoveyQA.getFullDisclosure());
		}
		if(null!=hoveyQA.getReDirect()){
			hoveyQADto.setReDirect(hoveyQA.getReDirect());
		}
		if(null!=hoveyQA.getOfAccounts()){
			hoveyQADto.setOfAccounts(hoveyQA.getOfAccounts());
		}
		if(null!=hoveyQA.getAccounts()){
			hoveyQADto.setAccounts(hoveyQA.getAccounts());
		}
		if(null!=hoveyQA.getRate()){
			hoveyQADto.setRate(hoveyQA.getRate());
		}
		if(null!=hoveyQA.getTpv()){
			hoveyQADto.setTpv(hoveyQA.getTpv());
		}
		if(null!=hoveyQA.getPermission()){
			hoveyQADto.setPermission(hoveyQA.getPermission());
		}
		if(null!=hoveyQA.getCustName()){
			hoveyQADto.setCustName(hoveyQA.getCustName());
		}
		if(null!=hoveyQA.getCompanyName()){
			hoveyQADto.setCompanyName(hoveyQA.getCompanyName());
		}
		if(null!=hoveyQA.getBilling()){
			hoveyQADto.setBilling(hoveyQA.getBilling());
		}
		if(null!=hoveyQA.getAuthorization()){
			hoveyQADto.setAuthorization(hoveyQA.getAuthorization());
		}
		if(null!=hoveyQA.getConfirmAccounts()){
			hoveyQADto.setConfirmAccounts(hoveyQA.getConfirmAccounts());
		}
		if(null!=hoveyQA.getChampionSupplier()){
			hoveyQADto.setChampionSupplier(hoveyQA.getChampionSupplier());
		}
		if(null!=hoveyQA.getRescission()){
			hoveyQADto.setRescission(hoveyQA.getRescission());
		}
		if(null!=hoveyQA.getConfirmDecission()){
			hoveyQADto.setConfirmDecission(hoveyQA.getConfirmDecission());
		}
		if(null!=hoveyQA.getProperFinish()){
			hoveyQADto.setProperFinish(hoveyQA.getProperFinish());
		}
		if(null!=hoveyQA.getComments()){
			hoveyQADto.setComments(hoveyQA.getComments());
		}
		if(null!=hoveyQA.getQaStatus()){
			hoveyQADto.setQaStatus(hoveyQA.getQaStatus());
		}
		if(null!=hoveyQA.getTransition()){
			hoveyQADto.setTransition(hoveyQA.getTransition());
		}
		if(null!=hoveyQA.getWrapUp()){
			hoveyQADto.setWrapUp(hoveyQA.getWrapUp());
		}
		if(null!=hoveyQA.getNeedCourtesyCall()){
			hoveyQADto.setNeedCourtesyCall(hoveyQA.getNeedCourtesyCall());
		}
		if(null!=hoveyQA.getCourtesyCallCompleted()){
			hoveyQADto.setCourtesyCallCompleted(hoveyQA.getCourtesyCallCompleted());
		}
		if(null!=hoveyQA.getReportedBy()){
			hoveyQADto.setReportedBy(hoveyQA.getReportedBy());
		}
		if(null!=hoveyQA.getPhoneNumber()) {
			hoveyQADto.setPhoneNumber(hoveyQA.getPhoneNumber());
		}
		if(null!=hoveyQA.getState()) {
			hoveyQADto.setState(hoveyQA.getState());
		}
		if(null!=hoveyQA.getProductId()) {
			hoveyQADto.setProductId(hoveyQA.getProductId());
		}
		if(null!=hoveyQA.getConfirmationNumber()) {
			hoveyQADto.setConfirmationNumber(hoveyQA.getConfirmationNumber());
		}
		if(null!=hoveyQA.getFullName()) {
			hoveyQADto.setFullName(hoveyQA.getFullName());
		}
		if(null!=hoveyQA.getTitle()) {
			hoveyQADto.setTitle(hoveyQA.getTitle());
		}
		
		if(null!=hoveyQA.getAccountNumberVerification()) {
			hoveyQADto.setAccountNumberVerification(hoveyQA.getAccountNumberVerification());
		}
		if(null!=hoveyQA.getPassThroughVaraibleProduct()) {
			hoveyQADto.setPassThroughVaraibleProduct(hoveyQA.getPassThroughVaraibleProduct());
		}
		if(null!=hoveyQA.getTermsETFs()) {
			hoveyQADto.setTermsETFs(hoveyQA.getTermsETFs());
		}
		if(null!=hoveyQA.getCompleteTPV()) {
			hoveyQADto.setCompleteTPV(hoveyQA.getCompleteTPV());
		}
		
		return hoveyQADto;
	}
}