package com.hovey.frontend.qa.service;
/**
 * Created By Bhagya On June 26th, 2015
 * Service Class for QA
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.hovey.backend.agent.exception.OrderNotFoundException;
import com.hovey.backend.agent.model.Orders;
import com.hovey.backend.agent.model.Transactions;
import com.hovey.backend.qa.exception.HoveyQAReportNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotSavedException;
import com.hovey.backend.qa.exception.QATransactionNotFoundException;
import com.hovey.frontend.agent.dto.OrdersDto;
import com.hovey.frontend.agent.dto.TransactionsDto;
import com.hovey.frontend.qa.dto.HoveyQADto;
import com.hovey.frontend.qa.dto.QAReportDto;

public interface QAService{
	public ArrayList<OrdersDto> gettingQADealSheetsFromDao(int pageNo,int pageSize,String sortBy,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter) throws Exception;
	public Integer findTotalNoOfQADealPages(int pageSize,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter) throws Exception;
	public Integer getTotalQADealSheetsCount(String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception;
	public HoveyQADto gettingQAReportForDeal(Integer dealId) throws HoveyQAReportNotFoundException,Exception;
	public HoveyQADto gettingQAReportForOrder(Integer orderId) throws HoveyQAReportNotFoundException,Exception;
	public Integer saveQAReportDetails(Integer dealId,Integer orderId,HoveyQADto hoveyQADto) throws Exception;
	public Integer SavingHoveyQAtoDB(Orders order,HoveyQADto hoveyQADto) throws Exception;
	public OrdersDto getOrderDtoByDealId(Integer dealId) throws Exception;
	public OrdersDto getOrderDtoByOrderId(Integer orderId) throws Exception;
	public TransactionsDto getQATransactionDtoByDealId(Integer dealId) throws QATransactionNotFoundException;
	public ArrayList<OrdersDto> getQADealSheetByDealId(int transactionId)throws Exception;
	public ArrayList<OrdersDto> getDealSheets(ArrayList<Transactions> transactions,ArrayList<OrdersDto> orders);
	public ArrayList<QAReportDto> getOrdersDetailsForReportsBetweenStartDateAndEndDate(Date startDate,Date endDate) throws OrderNotFoundException;
	
}