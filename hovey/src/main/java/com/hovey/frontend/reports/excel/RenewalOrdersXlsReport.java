package com.hovey.frontend.reports.excel;


import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;


import com.hovey.frontend.agent.dto.OrdersDto;


/*
 * Created By Bhagya On april 05th, 2016
 * Utility class to create Excel Files
 * it will b intiated on "Renewal Orders Excel" return
 */

	
	
	
public class RenewalOrdersXlsReport extends AbstractExcelView{

	private static Logger log=Logger.getLogger(RenewalOrdersXlsReport.class);

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("inside buildExcelDocument()");
		
		Map<String, OrdersDto> renewalData=(Map<String,OrdersDto>) model.get("renewals");
		HSSFSheet sheet = workbook.createSheet("Renewal Orders Report");
		
		 CellStyle style = workbook.createCellStyle();
	        Font font = workbook.createFont();
	        font.setColor(HSSFColor.BLACK.index);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        style.setFont(font);
		
	        
	        CellStyle currency=workbook.createCellStyle();
			currency.setDataFormat((short)7);
			CellStyle num=workbook.createCellStyle();
			num.setDataFormat((short) 3);
		// Setting Headings..
		
		HSSFRow header = sheet.createRow(0);		
	
		header.createCell(0).setCellValue("Order Date");
		header.createCell(1).setCellValue("Deal Start Date");
		header.createCell(2).setCellValue("Deal End Date");
		header.createCell(3).setCellValue("Account#");
		header.createCell(4).setCellValue("Customer Name");
		header.createCell(5).setCellValue("Supplier Name");
		header.createCell(6).setCellValue("Term");
		header.createCell(7).setCellValue("Status");
		header.createCell(8).setCellValue("Agent Name");
		header.createCell(9).setCellValue("Kwh");
		header.createCell(10).setCellValue("Rate");
		header.createCell(11).setCellValue("State");
		
		
		header.setRowStyle(style);
		
		int rowNum = 2;
		//Iterator iterator = pipelineData.entrySet().iterator();
		
		HSSFCellStyle dateStyle=workbook.createCellStyle();
		CreationHelper createHelper=workbook.getCreationHelper();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		// Setting Data for Cells.
		for(Map.Entry<String, OrdersDto> entry:renewalData.entrySet()){
			HSSFRow row = sheet.createRow(rowNum++);
			Cell cell0=row.createCell(0);
			if(null!=entry.getValue().getOrderDate()){
				cell0.setCellValue(entry.getValue().getOrderDate());
			}
			cell0.setCellStyle(dateStyle);
			
			Cell cell1=row.createCell(1);
			if(null!=entry.getValue().getDealStartDate()){
				cell1.setCellValue(entry.getValue().getDealStartDate());
			}
			cell1.setCellStyle(dateStyle);
			
			Cell cell2=row.createCell(2);
			if(null!=entry.getValue().getDealEndDate()){
				cell2.setCellValue(entry.getValue().getDealEndDate());
			}
			cell2.setCellStyle(dateStyle);
			
			row.createCell(3).setCellValue(entry.getValue().getAccountNumber());
			row.createCell(4).setCellValue(entry.getValue().getBusinessName());	
			row.createCell(5).setCellValue(entry.getValue().getSupplierName().getSupplierName());
			row.createCell(6).setCellValue(entry.getValue().getTerm());
			row.createCell(7).setCellValue(entry.getValue().getStatus());
			row.createCell(8).setCellValue(entry.getValue().getCreatedAgent().getFirstName() +" "+entry.getValue().getCreatedAgent().getLastName());
			row.createCell(9).setCellValue(new Double(entry.getValue().getKwh()));
			row.getCell(9).setCellStyle(num);			
			row.createCell(10).setCellValue(new Double(entry.getValue().getRate()));
			row.createCell(11).setCellValue(entry.getValue().getServiceState().getState());
			
					
		}
		
		
	
		
		for(int i=0;i<=5;i++){
			sheet.autoSizeColumn(i);
		}
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		
		
		 response.setHeader("Content-Disposition", "inline; filename=Renewal_Orders_Report_"+(cal.get(Calendar.MONTH)+1)+"_"+cal.get(Calendar.DATE)+"_"+cal.get(Calendar.YEAR)+".xls");
		  // Make sure to set the correct content type
		  response.setContentType("application/vnd.ms-excel");
		//workbook.write(response.getOutputStream());
		sheet.getWorkbook().write(response.getOutputStream());
		response.getOutputStream().flush();
	}
	
	
}




	

