package com.hovey.frontend.reports.excel;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.hovey.frontend.agent.dto.OrdersDto;


/***
 * Created By Bhagya On sep 30th,2015
 * 
 * Excel REport class for the deals
 *
 */
	
	
	
public class DealsXlsReport extends AbstractExcelView{

	private static Logger log=Logger.getLogger(CommissionXlsReport.class);

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("inside buildExcelDocument()");
		
		Map<String, OrdersDto> dealsData=(Map<String, OrdersDto>) model.get("deals");
		HSSFSheet sheet = workbook.createSheet("Deal Report");
		
		 CellStyle style = workbook.createCellStyle();
	        Font font = workbook.createFont();
	        font.setColor(HSSFColor.BLACK.index);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	        style.setFont(font);
		
	        
	        CellStyle currency=workbook.createCellStyle();
			currency.setDataFormat((short)7);
		// Setting Headings..
			
		
		HSSFRow header = sheet.createRow(0);		
	
		header.createCell(0).setCellValue("Order Date");
		header.createCell(1).setCellValue("Deal Start Date");
		header.createCell(2).setCellValue("Business Name");
		header.createCell(3).setCellValue("Agent Id");
		header.createCell(4).setCellValue("Agent Name");
		header.createCell(5).setCellValue("Tax Id");
		header.createCell(6).setCellValue("No. of Orders");
		header.createCell(7).setCellValue("Total Kwh");
		header.createCell(8).setCellValue("Deal Status");
		header.createCell(9).setCellValue("Deal Type");
		header.createCell(10).setCellValue("Commission Paid");
		header.createCell(11).setCellValue("Total Commission Expected");
		header.createCell(12).setCellValue("Total Commission Recieved");
		header.createCell(13).setCellValue("File Fronter");
		header.setRowStyle(style);
		
		int rowNum = 2;
	
		HSSFCellStyle dateStyle=workbook.createCellStyle();
		CreationHelper createHelper=workbook.getCreationHelper();
		dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MM/dd/yyyy"));
		// Setting Data for Cells.
		
		for(Map.Entry<String, OrdersDto> entry: dealsData.entrySet()){
			HSSFRow row = sheet.createRow(rowNum++);
			Cell cell0=row.createCell(0);
			if(null!=entry.getValue().getOrderDate()){
				cell0.setCellValue(entry.getValue().getOrderDate());
			}
			cell0.setCellStyle(dateStyle);
			Cell cell1=row.createCell(1);
			if(null!=entry.getValue().getDealStartDate()){
				cell1.setCellValue(entry.getValue().getDealStartDate());
			}			
			cell1.setCellStyle(dateStyle);
			row.createCell(2).setCellValue(entry.getValue().getBusinessName());
			row.createCell(3).setCellValue(entry.getValue().getCreatedAgent().getAgentNumber());
			row.createCell(4).setCellValue(entry.getValue().getCreatedAgent().getFirstName()+" "+entry.getValue().getCreatedAgent().getLastName());
			row.createCell(5).setCellValue(entry.getValue().getTaxId().getTaxId());
			row.createCell(6).setCellValue(entry.getValue().getTotalAccounts());
			row.createCell(7).setCellValue(NumberFormat.getNumberInstance(Locale.US).format(entry.getValue().getTotalKwh()));
			row.createCell(8).setCellValue(entry.getValue().getStatus());
			row.createCell(9).setCellValue(entry.getValue().getContractType());
			DecimalFormat df = new DecimalFormat("#.00");
			Double formattedTotalDealUpfrontCommission=Double.parseDouble(df.format(entry.getValue().getTotalDealUpfrontCommission()));
			Double formattedTotalDealCommission=Double.parseDouble(df.format(entry.getValue().getTotalDealCommission()));
			if(formattedTotalDealUpfrontCommission>=formattedTotalDealCommission)
			{
				row.createCell(10).setCellValue("Yes");
			}
			else{
				row.createCell(10).setCellValue("No");
			}
			row.createCell(11).setCellValue(formattedTotalDealUpfrontCommission);
			row.createCell(12).setCellValue(formattedTotalDealCommission);
			row.createCell(13).setCellValue(entry.getValue().getTaxId().getFronter());
		
		}
		
	
	
		for(int i=0;i<=8;i++){
			sheet.autoSizeColumn(i);
		}
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		
		 response.setHeader("Content-Disposition", "inline; filename="+"Deals_Report_"+(cal.get(Calendar.MONTH)+1)+"_"+cal.get(Calendar.DATE)+"_"+cal.get(Calendar.YEAR)+".xls");
		  // Make sure to set the correct content type
		  response.setContentType("application/vnd.ms-excel");
		//workbook.write(response.getOutputStream());
		sheet.getWorkbook().write(response.getOutputStream());
		response.getOutputStream().flush();
	}
	
	
}




	

