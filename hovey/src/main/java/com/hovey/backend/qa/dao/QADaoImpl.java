package com.hovey.backend.qa.dao;
/**
 * Created By Bhagya On June 26th,2015
 * Dao Implementation classes for QADao interface 
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;






import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hovey.backend.admin.exception.AgentCommissionNotFoundException;
import com.hovey.backend.agent.exception.OrderNotFoundException;
import com.hovey.backend.agent.model.AgentCommissions;
import com.hovey.backend.agent.model.OrderBySqlFormula;
import com.hovey.backend.agent.model.Orders;
import com.hovey.backend.agent.model.Transactions;
import com.hovey.backend.qa.exception.HoveyQAOrdersNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotSavedException;
import com.hovey.backend.qa.exception.QATransactionNotFoundException;
import com.hovey.backend.qa.model.HoveyQA;
import com.hovey.backend.user.exception.HoveyUserNotFoundException;

@Transactional
@Repository("qaDao")
public class QADaoImpl implements QADao{
	
	private static Logger log=Logger.getLogger(QADaoImpl.class);
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	
	/**
	 * Created By Bhagya On June 26th,2015
	 * @param pageNo
	 * @param pageSize
	 * @param sortBy
	 * @param searchBy
	 * @return
	 * @throws Exception
	 * 
	 * Method for total qa dealsheets of length pageSize
	 *  Here we applying the restrictions based on completedDeal Boolean value
	 * if completedDeal is false means,applying the restriction for getting the QA Not done deals
	 * else getting the QA CompletedDeals
	 */
	
		@SuppressWarnings("unchecked")
		public ArrayList<Transactions> getQATotalDealSheets(int pageNo,int pageSize,String sortBy,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception{
			log.info("inside getQATotalDealSheets()");
			ArrayList<Transactions> transactions= new ArrayList<Transactions>();		
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Orders.class);
			
			//projection..
			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("transactionId")));
			
			if(null!=completedDeals){
				if(completedDeals==false){
					criteria.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.isNull("qaStatus")));
				}
				else{
					criteria.add(Restrictions.eq("qaReady", true));
					
					DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
					dc.add(Restrictions.isNull("qaStatus"));
					dc.setProjection(Projections.property("transactionId"));
					criteria.add(Subqueries.propertyNotIn("transactionId", dc));
				}
			}
			if(adminQADeals==true){
				criteria.add(Restrictions.eq("qaReady", true));
			}
			
			if(null!=statusFilter){
				if(statusFilter.equalsIgnoreCase("pending")){
					criteria.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.isNull("qaStatus")));
					
				}
				else if(statusFilter.equalsIgnoreCase("pass")){
					criteria.add(Restrictions.eq("qaReady", true));
					
					DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
					dc.add(Restrictions.or(Restrictions.eq("qaStatus", "fail"),Restrictions.isNull("qaStatus")));
					dc.setProjection(Projections.property("transactionId"));
					criteria.add(Subqueries.propertyNotIn("transactionId", dc));
					
				}
				else if(statusFilter.equalsIgnoreCase("fail")){
					/**
					 * Condition Should be.
					 * 
					 *  Get Orders where qaReady=true and qaStatus=fail == ALL FAIL ORDERS
					 *  Get Orders where qaReady is NULL and qa Status Not REady === QA Not Ready Orders
					 *             MINUS
					 *   Get ORders where qaStatus is Null ==   TO FILTER PENDING ORDERS DEALS (QA status will be null only for Pending Deals)
					 *  
					 */
					
					Disjunction or=Restrictions.disjunction();
					
					or.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.eq("qaStatus", "fail")));
					or.add(Restrictions.and(Restrictions.isNull("qaReady"), Restrictions.eq("qaStatus", "NOT READY")));
					
					criteria.add(or);
					
					DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
					dc.add(Restrictions.isNull("qaStatus"));
					dc.setProjection(Projections.property("transactionId"));
					criteria.add(Subqueries.propertyNotIn("transactionId", dc));
					
				}
				
				else if(statusFilter.equalsIgnoreCase("courtesy call")){
					
					DetachedCriteria dc=DetachedCriteria.forClass(HoveyQA.class);
					dc.add(Restrictions.eq("needCourtesyCall", true));
					dc.createAlias("order", "o").setProjection(Projections.property("o.transactionId"));
					criteria.add(Subqueries.propertyIn("transactionId", dc));
					
				}
				
				
			}
			
			
			//searching
			if(null!=searchBy ){
				criteria.add(Restrictions.ilike("businessName", "%"+searchBy+"%"));
			}
					
			//sorting..
			if(sortBy.equalsIgnoreCase("orderDate") || sortBy.equalsIgnoreCase("dealStartDate") ){
				criteria.addOrder(Order.desc(sortBy));
			}
			else if(sortBy.equalsIgnoreCase("order_id")){
				criteria.addOrder(OrderBySqlFormula.sqlFormula("count(order_id) desc"));
			}
			else if(sortBy.equalsIgnoreCase("agent") || sortBy.equalsIgnoreCase("agentName")){
				if(sortBy.equalsIgnoreCase("agent")){
					criteria.createCriteria("agent").addOrder(Order.asc("agentNumber"));
				}
				else{
					criteria.createCriteria("agent").addOrder(Order.asc("firstName"));
				}
			}	
			else if(sortBy.equalsIgnoreCase("kwh")){
				criteria.addOrder(OrderBySqlFormula.sqlFormula("sum(kwh) desc"));
			}
			else{
				criteria.addOrder(Order.asc(sortBy));
			}		
			
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);		
			transactions=(ArrayList<Transactions>) criteria.list();
					
			if(!transactions.isEmpty()){
				return transactions;
			}
			else{ 
				throw new OrderNotFoundException();
			}
		}
		
		
		/**
		 * Created By Bhagya On June 26th,2015
		 * @param searchBy
		 * @return
		 * @throws Exception
		 * 
		 * Method for Finds out Total No of Deal Sheets to be used to decide no of pages needed
		 * Here we applying the restrictions based on completedDeal Boolean value
		 * if completedDeal is false means,applying the restriction for getting the QA Not done deals
		 * else getting the QA CompletedDeals
		 */
		public Integer getTotalNoofQADealSheets(String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception{
				log.info("inside getTotalNoofQADealSheets()");	
				Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Orders.class);
				//projection..
				criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("transactionId")));
				
				if(null!=completedDeals){
					if(completedDeals==false){
						criteria.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.isNull("qaStatus")));
					}
					else{
						/*criteria.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.isNotNull("qaStatus")));*/
						criteria.add(Restrictions.eq("qaReady", true));
						
						DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
						dc.add(Restrictions.isNull("qaStatus"));
						dc.setProjection(Projections.property("transactionId"));
						criteria.add(Subqueries.propertyNotIn("transactionId", dc));
					}
				}
				if(adminQADeals==true){
					criteria.add(Restrictions.eq("qaReady", true));
				}
				if(null!=statusFilter){
					if(statusFilter.equalsIgnoreCase("pending")){
						criteria.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.isNull("qaStatus")));
					}
					else if(statusFilter.equalsIgnoreCase("pass")){
						criteria.add(Restrictions.eq("qaReady", true));
						
						DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
						dc.add(Restrictions.or(Restrictions.eq("qaStatus", "fail"),Restrictions.isNull("qaStatus")));
						dc.setProjection(Projections.property("transactionId"));
						criteria.add(Subqueries.propertyNotIn("transactionId", dc));
					}
					else if(statusFilter.equalsIgnoreCase("fail")){
						/**
						 * Condition Should be.
						 * 
						 *  Get Orders where qaReady=true and qaStatus=fail == ALL FAIL ORDERS
						 *  Get Orders where qaReady is NULL and qa Status Not REady === QA Not Ready Orders
						 *             MINUS
						 *   Get ORders where qaStatus is Null ==   TO FILTER PENDING ORDERS DEALS (QA status will be null only for Pending Deals)
						 *  
						 */
						
						Disjunction or=Restrictions.disjunction();
						
						or.add(Restrictions.and(Restrictions.eq("qaReady", true), Restrictions.eq("qaStatus", "fail")));
						or.add(Restrictions.and(Restrictions.isNull("qaReady"), Restrictions.eq("qaStatus", "NOT READY")));
						
						criteria.add(or);
						
						DetachedCriteria dc=DetachedCriteria.forClass(Orders.class);
						dc.add(Restrictions.isNull("qaStatus"));
						dc.setProjection(Projections.property("transactionId"));
						criteria.add(Subqueries.propertyNotIn("transactionId", dc));
					
					}
					
					else if(statusFilter.equalsIgnoreCase("courtesy call")){
						
						DetachedCriteria dc=DetachedCriteria.forClass(HoveyQA.class);
						dc.add(Restrictions.eq("needCourtesyCall", true));
						dc.createAlias("order", "o").setProjection(Projections.property("o.transactionId"));
						criteria.add(Subqueries.propertyIn("transactionId", dc));
						
					}
					
				}
				
				//searching
				if(null!=searchBy ){
					criteria.add(Restrictions.ilike("businessName", "%"+searchBy+"%"));
				}				
				
			
				Integer transactionsCount= criteria.list().size();				
				return transactionsCount;
		}
		
		
		public ArrayList<Orders> getQAPendingNotReadyOrdersByDealId(Integer dealId)throws Exception{
			log.info("inside getQAPendingNotReadyOrdersByDealId " );
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Orders.class)
					.add(Restrictions.eq("transactionId.id", dealId));
			criteria.add(Restrictions.and(Restrictions.isNull("qaStatus"), Restrictions.isNull("qaReady")));
			ArrayList<Orders> orders=(ArrayList<Orders>) criteria.list();
			if(!orders.isEmpty()){
				return orders;
			}
			else{
				throw new OrderNotFoundException();
			}
		}
		
		
		
		/**
		 * Created By Bhagya On July 02nd,2015
		 * @param order
		 * @return
		 * @throws HoveyQAReportNotFoundException
		 * 
		 * Method for getting qa report by order
		 */
		@SuppressWarnings("unchecked")
		public HoveyQA getQAReportDetailsByOrder(Orders order) {
			log.info("inside getQAReportDetailsByOrder()");
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(HoveyQA.class);
			criteria.add(Restrictions.eq("order", order));
			ArrayList<HoveyQA> hoveyQA=(ArrayList<HoveyQA>) criteria.list();
			HoveyQA hoveyQAResult=null;
			if(!hoveyQA.isEmpty()){
				hoveyQAResult= hoveyQA.get(0);
			}
			return hoveyQAResult;
			
		}
		/**
		 * Created By Bhagya On July 02nd,2015
		 * @param hoveyQAReport
		 * @return
		 * 
		 * Method For saving or updating the hovey qa Report
		 */
		public Integer saveOrUpdateHoveyQADetails(HoveyQA hoveyQAReport)throws HoveyQAReportNotSavedException{
			log.info("inside saveOrUpdateHoveyQADetails()");
			hibernateTemplate.saveOrUpdate(hoveyQAReport);
			hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		    return hoveyQAReport.getQaId();
		}
		/**
		 * Created By Bhagya On junly 06th,2015
		 * @param dealId
		 * @return
		 * @throws QATransactionNotFoundException
		 * 
		 * Method for getting the qa deal By Id
		 */
		@SuppressWarnings("unchecked")
		public Transactions getQATransactionByDealId(Integer dealId) throws QATransactionNotFoundException{
			log.info("inside getTransactionByDealId()");
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Transactions.class);
			criteria.add(Restrictions.eq("id", dealId));
			ArrayList<Transactions> transactions=(ArrayList<Transactions>) criteria.list();
			if(!transactions.isEmpty()){
				return transactions.get(0);
			}
			else{
				throw new QATransactionNotFoundException();
			}
			
		}
		/**
		 * Created By Bhagya On July 09th,2015
		 * @param qaId
		 * @return
		 * 
		 * Getting the Hovey QA Report By QA Id
		 */
		@SuppressWarnings("unchecked")
		public HoveyQA getQAReportDetailsByQAId(Integer qaId) {
			log.info("inside getQAReportDetailsByOrder()");
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(HoveyQA.class);
			criteria.add(Restrictions.eq("qaId", qaId));
			ArrayList<HoveyQA> hoveyQA=(ArrayList<HoveyQA>) criteria.list();
			HoveyQA hoveyQAResult=null;
			if(!hoveyQA.isEmpty()){
				hoveyQAResult= hoveyQA.get(0);
			}
			return hoveyQAResult;
		}
		
		/**
		 * Created By Bhagya On July 13th,2015
		 * @param dealStartDate
		 * @param dealEndDate
		 * @param status
		 * @return
		 * @throws OrderNotFoundException
		 * 
		 * Method For Getting the transactions or deals by qa status in between dealStartDate and dealEndDate
		 */
		@SuppressWarnings("unchecked")
		public ArrayList<Orders> getAllOrdersBetweenStartDateAndEndDate(Date startDate,Date endDate) throws OrderNotFoundException{
			log.info("inside getAllPassDealsBetweenStartDateAndEndDate()");
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Orders.class);
			criteria.add(Restrictions.between("orderDate", startDate, endDate));
			ArrayList<Orders> orders=(ArrayList<Orders>) criteria.list();
			if(!orders.isEmpty()){
				return orders;
			}
			else{
				throw new OrderNotFoundException();
			}
			
		}
		
		
		
		
		/**
		 * Added by Bhagya on August 06th,2015
		 * to get list of hovey qa Orders of Orders
		 * @throws HoveyQAOrdersNotFoundException 
		 */
		@SuppressWarnings("unchecked")
		public ArrayList<HoveyQA> getHoveyQAOrdersOfOrders(ArrayList<Orders> orders)throws HoveyQAOrdersNotFoundException{
			log.info("inside getHoveyQAOrdersOfOrders()");
			Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(HoveyQA.class);
			criteria.add(Restrictions.in("order", orders));
			ArrayList<HoveyQA> qaOrders=(ArrayList<HoveyQA>) criteria.list();
			
			if(!qaOrders.isEmpty()){
				return qaOrders;
			}
			else{
				throw new HoveyQAOrdersNotFoundException();
			}	
			
		}
		
		
		/**
		 * Added by Bhagya on August 0th,2015 to delete qa Orders of Orders
		 */
		
		public Integer deleteHoveyQAOrdersOfOrders(ArrayList<HoveyQA> qaOrders)throws Exception{
			log.info("inside deleteHoveyQAOrdersOfOrders( )");
			hibernateTemplate.deleteAll(qaOrders);
			hibernateTemplate.getSessionFactory().getCurrentSession().flush();
			return qaOrders.get(0).getQaId();
		}
}