package com.hovey.backend.qa.exception;
/**
 * Created By Bhagya On July 06nd,2015
 * It can used or throws a user defined exception,when the QA Deal or transaction is not found
 */
public class QATransactionNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public String toString(){
		return "No QA Deal Found";
	}
	
}
