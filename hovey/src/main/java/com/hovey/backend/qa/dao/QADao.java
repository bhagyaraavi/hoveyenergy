package com.hovey.backend.qa.dao;
/**
 * Created By Bhagya On June 26th,2015
 * Interface for QADao
 */
import java.util.ArrayList;
import java.util.Date;

import com.hovey.backend.agent.exception.OrderNotFoundException;
import com.hovey.backend.agent.model.Orders;
import com.hovey.backend.agent.model.Transactions;
import com.hovey.backend.qa.exception.HoveyQAOrdersNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotFoundException;
import com.hovey.backend.qa.exception.HoveyQAReportNotSavedException;
import com.hovey.backend.qa.exception.QATransactionNotFoundException;
import com.hovey.backend.qa.model.HoveyQA;

public interface QADao{
	public ArrayList<Transactions> getQATotalDealSheets(int pageNo,int pageSize,String sortBy,String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception;
	public Integer getTotalNoofQADealSheets(String searchBy,Boolean completedDeals,Boolean adminQADeals,String statusFilter)throws Exception;
	public HoveyQA getQAReportDetailsByOrder(Orders order);
	public Integer saveOrUpdateHoveyQADetails(HoveyQA hoveyQAReport)throws HoveyQAReportNotSavedException;
	public Transactions getQATransactionByDealId(Integer dealId) throws QATransactionNotFoundException;
	public HoveyQA getQAReportDetailsByQAId(Integer qaId);
	public ArrayList<Orders> getAllOrdersBetweenStartDateAndEndDate(Date startDate,Date endDate) throws OrderNotFoundException;
	public ArrayList<Orders> getQAPendingNotReadyOrdersByDealId(Integer dealId)throws Exception;
	public ArrayList<HoveyQA> getHoveyQAOrdersOfOrders(ArrayList<Orders> orders)throws HoveyQAOrdersNotFoundException;
	public Integer deleteHoveyQAOrdersOfOrders(ArrayList<HoveyQA> qaOrders)throws Exception;

}