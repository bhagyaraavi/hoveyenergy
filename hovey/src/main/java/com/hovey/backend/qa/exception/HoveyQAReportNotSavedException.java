package com.hovey.backend.qa.exception;

/**
 * Created By Bhagya On July 02nd,2015
 * User Defined Exception for ,if the QA Report Not Saved Means
 */

public class HoveyQAReportNotSavedException extends Exception {

	private static final long serialVersionUID = 1L;

	public String toString(){
		return "QA Report Not Saved";
	}
	
}
