package com.hovey.backend.qa.exception;
/**
 * Created By Bhagya On July 02nd,2015
 * It can used or throws a user defined exception,when the QA Report is not found
 */
public class HoveyQAReportNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public String toString(){
		return "No QA Report Found";
	}
	
}
