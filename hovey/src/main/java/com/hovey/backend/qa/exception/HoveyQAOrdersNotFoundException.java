package com.hovey.backend.qa.exception;
/**
 * Created By Bhagya On August 06th,2015
 * It can used or throws a user defined exception,when the QA Orders are not found
 */
public class HoveyQAOrdersNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public String toString(){
		return "No QA Orders Found";
	}
	
}
