package com.hovey.backend.qa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hovey.backend.agent.model.Orders;

/**
 * Created By Bhagya On June 30th,2015
 *	Model for the QA Form ,all fields of qa checklist screen
 */
@Entity
@Table(name="hovey_qa")
public class HoveyQA implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="qa_id")
	private Integer qaId;
	
	@OneToOne
	@JoinColumn(name="order_id",referencedColumnName="order_id",unique=true,nullable=false)
	private Orders order;
	
	@Column(name="qa_date")
	private Date qaDate;
	
	@Column(name="qa_score")
	private String qaScore;
	
	@Column(name="brands_call")
	private Boolean brandsCall;
	
	@Column(name="positive_tone")
	private Boolean positiveTone;
	
	@Column(name="sounds_natural")
	private Boolean soundsNatural;
	
	@Column(name="sticks_pitch")
	private Boolean sticksPitch;
	
	@Column(name="professionalism")
	private Boolean professionalism;
	
	@Column(name="sounds_confident")
	private Boolean soundsConfident;
	
	@Column(name="reassures_sincerely")
	private Boolean reassuresSincerely;
	
	@Column(name="assumptive_swagger")
	private Boolean assumptiveSwagger;
	
	@Column(name="full_disclosure")
	private Boolean fullDisclosure;
	
	@Column(name="re_direct")
	private Boolean reDirect;
	
	@Column(name="of_accounts")
	private Boolean ofAccounts;
	
	@Column(name="accounts")
	private Boolean accounts;
	
	@Column(name="rate")
	private Boolean rate;
	
	@Column(name="tpv")
	private Boolean tpv;
	
	@Column(name="permission")
	private Boolean permission;
	
	@Column(name="cust_name")
	private Boolean custName;
	
	@Column(name="company_name")
	private Boolean companyName;
	
	@Column(name="billing")
	private Boolean billing;
	
	@Column(name="authorization")
	private Boolean authorization;
	
	@Column(name="confirm_accounts")
	private Boolean confirmAccounts;
	
	@Column(name="champion_supplier")
	private Boolean championSupplier;
	
	@Column(name="rescission")
	private Boolean rescission;
	
	@Column(name="confirm_decission")
	private Boolean confirmDecission;
	
	@Column(name="proper_finish")
	private Boolean properFinish;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="qa_status")
	private Boolean qaStatus;

	/**Added on August 05th,2015
	 * As per client Requirement Added these fields
	  */
	@Column(name="transition")
	private Boolean transition;
	
	@Column(name="wrap_up")
	private Boolean wrapUp;
	
	@Column(name="need_courtesy_call")
	private Boolean needCourtesyCall;
	
	@Column(name="courtesy_call_completed")
	private Boolean courtesyCallCompleted;
	
	@Column(name="reported_by")
	private String reportedBy;
	
	/**
	 * Added on June 01st,2021
	 * as per the client requirement
	 */
	
	@Column(name="phone_number")
	private Boolean phoneNumber;
	
	@Column(name="state")
	private Boolean state;
	
	@Column(name="product_id")
	private Boolean productId;
	
	@Column(name="confirmation_number")
	private Boolean confirmationNumber;
	
	@Column(name="full_name")
	private Boolean fullName;
	
	@Column(name="title")
	private Boolean title;
	
	@Column(name="account_number_verification")
	private Boolean accountNumberVerification;
	
	@Column(name="pass_through_variable_product")
	private Boolean passThroughVaraibleProduct;
	
	@Column(name="terms_ETFs")
	private Boolean termsETFs;
	
	@Column(name="complete_TPV")
	private Boolean completeTPV;
	/**
	 * End of adding the fields on june01st,2021
	 */
	
	public Integer getQaId() {
		return qaId;
	}

	public void setQaId(Integer qaId) {
		this.qaId = qaId;
	}
	
	
	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	public Date getQaDate() {
		return qaDate;
	}

	public void setQaDate(Date qaDate) {
		this.qaDate = qaDate;
	}

	public String getQaScore() {
		return qaScore;
	}

	public void setQaScore(String qaScore) {
		this.qaScore = qaScore;
	}

	public Boolean getBrandsCall() {
		return brandsCall;
	}

	public void setBrandsCall(Boolean brandsCall) {
		this.brandsCall = brandsCall;
	}

	public Boolean getPositiveTone() {
		return positiveTone;
	}

	public void setPositiveTone(Boolean positiveTone) {
		this.positiveTone = positiveTone;
	}

	public Boolean getSoundsNatural() {
		return soundsNatural;
	}

	public void setSoundsNatural(Boolean soundsNatural) {
		this.soundsNatural = soundsNatural;
	}

	public Boolean getSticksPitch() {
		return sticksPitch;
	}

	public void setSticksPitch(Boolean sticksPitch) {
		this.sticksPitch = sticksPitch;
	}

	public Boolean getProfessionalism() {
		return professionalism;
	}

	public void setProfessionalism(Boolean professionalism) {
		this.professionalism = professionalism;
	}

	public Boolean getSoundsConfident() {
		return soundsConfident;
	}

	public void setSoundsConfident(Boolean soundsConfident) {
		this.soundsConfident = soundsConfident;
	}

	public Boolean getReassuresSincerely() {
		return reassuresSincerely;
	}

	public void setReassuresSincerely(Boolean reassuresSincerely) {
		this.reassuresSincerely = reassuresSincerely;
	}

	public Boolean getAssumptiveSwagger() {
		return assumptiveSwagger;
	}

	public void setAssumptiveSwagger(Boolean assumptiveSwagger) {
		this.assumptiveSwagger = assumptiveSwagger;
	}

	public Boolean getFullDisclosure() {
		return fullDisclosure;
	}

	public void setFullDisclosure(Boolean fullDisclosure) {
		this.fullDisclosure = fullDisclosure;
	}

	public Boolean getReDirect() {
		return reDirect;
	}

	public void setReDirect(Boolean reDirect) {
		this.reDirect = reDirect;
	}

	public Boolean getOfAccounts() {
		return ofAccounts;
	}

	public void setOfAccounts(Boolean ofAccounts) {
		this.ofAccounts = ofAccounts;
	}

	public Boolean getAccounts() {
		return accounts;
	}

	public void setAccounts(Boolean accounts) {
		this.accounts = accounts;
	}

	public Boolean getRate() {
		return rate;
	}

	public void setRate(Boolean rate) {
		this.rate = rate;
	}

	public Boolean getTpv() {
		return tpv;
	}

	public void setTpv(Boolean tpv) {
		this.tpv = tpv;
	}

	public Boolean getPermission() {
		return permission;
	}

	public void setPermission(Boolean permission) {
		this.permission = permission;
	}

	public Boolean getCustName() {
		return custName;
	}

	public void setCustName(Boolean custName) {
		this.custName = custName;
	}

	public Boolean getCompanyName() {
		return companyName;
	}

	public void setCompanyName(Boolean companyName) {
		this.companyName = companyName;
	}

	public Boolean getBilling() {
		return billing;
	}

	public void setBilling(Boolean billing) {
		this.billing = billing;
	}

	public Boolean getAuthorization() {
		return authorization;
	}

	public void setAuthorization(Boolean authorization) {
		this.authorization = authorization;
	}

	public Boolean getConfirmAccounts() {
		return confirmAccounts;
	}

	public void setConfirmAccounts(Boolean confirmAccounts) {
		this.confirmAccounts = confirmAccounts;
	}

	public Boolean getChampionSupplier() {
		return championSupplier;
	}

	public void setChampionSupplier(Boolean championSupplier) {
		this.championSupplier = championSupplier;
	}

	public Boolean getRescission() {
		return rescission;
	}

	public void setRescission(Boolean rescission) {
		this.rescission = rescission;
	}

	public Boolean getConfirmDecission() {
		return confirmDecission;
	}

	public void setConfirmDecission(Boolean confirmDecission) {
		this.confirmDecission = confirmDecission;
	}

	public Boolean getProperFinish() {
		return properFinish;
	}

	public void setProperFinish(Boolean properFinish) {
		this.properFinish = properFinish;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Boolean getQaStatus() {
		return qaStatus;
	}

	public void setQaStatus(Boolean qaStatus) {
		this.qaStatus = qaStatus;
	}

	public Boolean getTransition() {
		return transition;
	}

	public void setTransition(Boolean transition) {
		this.transition = transition;
	}

	public Boolean getWrapUp() {
		return wrapUp;
	}

	public void setWrapUp(Boolean wrapUp) {
		this.wrapUp = wrapUp;
	}

	public Boolean getNeedCourtesyCall() {
		return needCourtesyCall;
	}

	public void setNeedCourtesyCall(Boolean needCourtesyCall) {
		this.needCourtesyCall = needCourtesyCall;
	}

	public Boolean getCourtesyCallCompleted() {
		return courtesyCallCompleted;
	}

	public void setCourtesyCallCompleted(Boolean courtesyCallCompleted) {
		this.courtesyCallCompleted = courtesyCallCompleted;
	}

	public String getReportedBy() {
		return reportedBy;
	}

	public void setReportedBy(String reportedBy) {
		this.reportedBy = reportedBy;
	}

	public Boolean getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Boolean phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Boolean getProductId() {
		return productId;
	}

	public void setProductId(Boolean productId) {
		this.productId = productId;
	}

	

	public Boolean getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(Boolean confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}


	public Boolean getFullName() {
		return fullName;
	}

	public void setFullName(Boolean fullName) {
		this.fullName = fullName;
	}

	public Boolean getTitle() {
		return title;
	}

	public void setTitle(Boolean title) {
		this.title = title;
	}
	

	public Boolean getAccountNumberVerification() {
		return accountNumberVerification;
	}

	public void setAccountNumberVerification(Boolean accountNumberVerification) {
		this.accountNumberVerification = accountNumberVerification;
	}

	public Boolean getPassThroughVaraibleProduct() {
		return passThroughVaraibleProduct;
	}

	public void setPassThroughVaraibleProduct(Boolean passThroughVaraibleProduct) {
		this.passThroughVaraibleProduct = passThroughVaraibleProduct;
	}

	public Boolean getTermsETFs() {
		return termsETFs;
	}

	public void setTermsETFs(Boolean termsETFs) {
		this.termsETFs = termsETFs;
	}

	public Boolean getCompleteTPV() {
		return completeTPV;
	}

	public void setCompleteTPV(Boolean completeTPV) {
		this.completeTPV = completeTPV;
	}
	
	
}