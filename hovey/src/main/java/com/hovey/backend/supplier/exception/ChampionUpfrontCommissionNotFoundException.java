package com.hovey.backend.supplier.exception;

public class ChampionUpfrontCommissionNotFoundException extends Exception{

	/**
	 * Created By bhagya On august 18th, 2016
	 * 
	 * Customized exception for handling when the champion upfront commissions not found by restricted criteria.
	 */
	private static final long serialVersionUID = 1L;

	public String toString(){
		return "No Champion Upfront Commission Found By Searched Criteria";
	}
}
