package com.hovey.backend.supplier.dao;

import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.hovey.backend.agent.model.ChampionUpfrontCommissions;
import com.hovey.backend.agent.model.Orders;
import com.hovey.backend.supplier.exception.ChampionUpfrontCommissionNotFoundException;
import com.hovey.backend.supplier.exception.SupplierFileAlreadyExistsException;
import com.hovey.backend.supplier.exception.SupplierFileNotFoundException;
import com.hovey.backend.supplier.exception.SupplierMappingNotFoundException;
import com.hovey.backend.supplier.exception.SupplierNotFoundException;
import com.hovey.backend.supplier.exception.SupplierReportsNotFoundException;
import com.hovey.backend.supplier.model.Supplier;
import com.hovey.backend.supplier.model.SupplierFiles;
import com.hovey.backend.supplier.model.SupplierMapping;
import com.hovey.backend.supplier.model.SupplierReports;


/**
 * 
 * @author JEEVAN
 * 
 * 
 * DAO for Supplier Related activities.
 *
 */


@Repository("supplierDao")
@Transactional
public class SupplierDaoImpl implements SupplierDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	private static Logger log=Logger.getLogger(SupplierDaoImpl.class);
	
	
	// Saves/Updates supplier .
	public Integer saveOrUpdateSupplierToDB(Supplier supplier)throws Exception{
		log.info("inside saveOrUpdateSupplierToDB()");
		Assert.notNull(supplier);
		hibernateTemplate.saveOrUpdate(supplier);
		hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		return supplier.getId();
	}
	
	
	//get Supplier by Supplier Name.
	
	@SuppressWarnings("unchecked")
	public Supplier getSupplierByName(String supplierName) throws Exception{
		log.info("inside getSupplierByName");
		Assert.notNull(supplierName);
		ArrayList<Supplier> suppliers=(ArrayList<Supplier>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Supplier.class)
				.add(Restrictions.eq("supplierName", supplierName)).list();
		if(!suppliers.isEmpty()){
			return suppliers.get(0);
		}
		else{
			throw new SupplierNotFoundException();
		}
	}
	
	
//get Supplier by SupplierID
	@SuppressWarnings("unchecked")
	public Supplier getSupplierByID(Integer supplierId) throws Exception{
		log.info("inside getSupplierByName");
		Assert.notNull(supplierId);
		ArrayList<Supplier> suppliers=(ArrayList<Supplier>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Supplier.class)
				.add(Restrictions.eq("id", supplierId)).list();
		if(!suppliers.isEmpty()){
			return suppliers.get(0);
		}
		else{
			throw new SupplierNotFoundException();
		}
	}
	
	
	//view all Suppliers..
	@SuppressWarnings("unchecked")
	public ArrayList<Supplier> getSuppliers()throws SupplierNotFoundException{
		log.info("inside getSuppliers()");
		ArrayList<Supplier> suppliers=(ArrayList<Supplier>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(Supplier.class).list();
		if(!suppliers.isEmpty()){
			return suppliers;
		}
		else{
			throw new SupplierNotFoundException();
		}
	}
	
	//for Deleting the Supplier	
	public void deleteSupplier(Supplier supplier) throws Exception{
		log.info("inside deleteSupplier()");
		hibernateTemplate.delete(supplier);
		hibernateTemplate.getSessionFactory().getCurrentSession().flush();
	}
	
	

	
/*
 * 		Supplier Mappings
 * 
 *  */	
	
	//saves or update supplier Mapping.
	public Integer saveorUpdateSupplierMappings(SupplierMapping mapping)throws Exception{
		log.info("inside saveorupdateSupplierMappings");
		Assert.notNull(mapping);
		hibernateTemplate.saveOrUpdate(mapping);
		hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		return mapping.getId();
	}
	
	//getting SupplierMapping by SupplierName
	@SuppressWarnings("unchecked")
	public SupplierMapping getSupplierMappingBySupplier(String supplierName)throws Exception{
		log.info("inside getSupplierMappingBySupplierName()");
		ArrayList<SupplierMapping> mappings=(ArrayList<SupplierMapping>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierMapping.class)
				.add(Restrictions.eq("supplierName.supplierName", supplierName)).list();
		if(!mappings.isEmpty()){
			return mappings.get(0);
		}
		else{
			throw new SupplierMappingNotFoundException();
		}		
	}
	
	//getting SupplierMapping by id
		@SuppressWarnings("unchecked")
		public SupplierMapping getSupplierMappingById(Integer id)throws Exception{
			log.info("inside getSupplierMappingBySupplierName()");
			ArrayList<SupplierMapping> mappings=(ArrayList<SupplierMapping>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierMapping.class)
					.add(Restrictions.eq("id", id)).list();
			if(!mappings.isEmpty()){
				return mappings.get(0);
			}
			else{
				throw new SupplierMappingNotFoundException();
			}		
		}
		
	
		//view all Supplier Mappings..
		@SuppressWarnings("unchecked")
		public ArrayList<SupplierMapping> getSupplierMappings()throws SupplierMappingNotFoundException{			
			log.info("inside getSuppliers()");
			ArrayList<SupplierMapping> mappings=(ArrayList<SupplierMapping>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierMapping.class).list();
			if(!mappings.isEmpty()){
				return mappings;
			}
			else{
				throw new SupplierMappingNotFoundException();
			}
		}
			
		//deletes supplierMappings.
	  public void deleteSupplierMapping(SupplierMapping mapping)throws Exception{
		  log.info("inside deleteSupplierMapping");
		  hibernateTemplate.delete(mapping);
		  hibernateTemplate.getSessionFactory().getCurrentSession().flush();
	 }
	
	
	  /* 
	   *  	Supplier Reports......
	   * 
	   * */
	  
	  //save or Update Supplier Reports(BULK)
	  public Integer saveorUpdateSupplierReports(ArrayList<SupplierReports> reports)throws Exception{
		  log.info("inside saveSupplierReports()");
		  Assert.notNull(reports);
		  hibernateTemplate.saveOrUpdateAll(reports);
		  hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		  return reports.get(0).getId();
	  }
	  
	//save or Update Supplier Reports(BULK)
	  public Integer saveorUpdateSupplierReports(SupplierReports report)throws Exception{
		  log.info("inside saveSupplierReports()");		
		  hibernateTemplate.saveOrUpdate(report);
		//  hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		  return report.getId();
	  }
	  
	  
	  
	 // gwt all supplier reports..
	  @SuppressWarnings("unchecked")
	public ArrayList<SupplierReports> getSupplierReports()throws SupplierReportsNotFoundException{
		  log.info("inside getSupplierReports()");
		  ArrayList<SupplierReports> reports=(ArrayList<SupplierReports>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class).list();
		  if(!reports.isEmpty()){
			  return reports;
		  }
		  else{
			  throw new SupplierReportsNotFoundException();
		  }
	  }
	  
	  
	  
	  //get supplier reports of a supplier
	 @SuppressWarnings("unchecked")
	public ArrayList<SupplierReports> getSupplierReportsofaSupplier(String supplierName)throws Exception{
		 log.info("inside getSupplierReportsOfaSupplier()");
		 ArrayList<SupplierReports> reports=(ArrayList<SupplierReports>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class)
				 .add(Restrictions.eq("supplierName.supplierName", supplierName)).list();
		 
		if(!reports.isEmpty()){
			return reports;
		}
		else{
			throw new SupplierReportsNotFoundException();
		}
	 }
	 
	 //get supplier report by Id
	@SuppressWarnings("unchecked")
	public SupplierReports getSupplierReportById(Integer id)throws Exception{
		log.info("inside getSupplierReportById()");
		ArrayList<SupplierReports> reports=(ArrayList<SupplierReports>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class)
				.add(Restrictions.eq("id",id)).list();
		if(!reports.isEmpty()){
			return reports.get(0);
		}
		else{
			throw new SupplierReportsNotFoundException();
		}
		
	}
	
	
	
	//save supplier file..
	public SupplierFiles saveSupplierFile(SupplierFiles file)throws SupplierFileAlreadyExistsException{
		log.info("inside save SupplierFiles()");
		Assert.notNull(file);
		hibernateTemplate.save(file);
		hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		return file;
	}
	
	//delete supplier file..
		public Integer deleteSupplierFile(SupplierFiles file)throws Exception{
			log.info("inside save SupplierFiles()");
			Assert.notNull(file);
			hibernateTemplate.delete(file);
			hibernateTemplate.getSessionFactory().getCurrentSession().flush();
			return file.getId();
		}
		
		
		
	  
	
		
     //getting SupplierFiles by Supplier File Name
		@SuppressWarnings("unchecked")
		public SupplierFiles getSupplierFilesByFileName(String fileName)throws SupplierFileNotFoundException{
			log.info("inside getSupplierFilesByFileName( )");
			ArrayList<SupplierFiles> files=(ArrayList<SupplierFiles>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierFiles.class)
					.add(Restrictions.eq("fileName", fileName)).list();
			
			if(!files.isEmpty()){
				return files.get(0);
			}
			else{
				throw new SupplierFileNotFoundException();
			}
			
		}
		
		
		
		
		 //getting SupplierFiles by Supplier File Name
		@SuppressWarnings("unchecked")
		public SupplierFiles getSupplierFilesById(Integer id)throws SupplierFileNotFoundException{
			log.info("inside getSupplierFilesByFileName( )");
			ArrayList<SupplierFiles> files=(ArrayList<SupplierFiles>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierFiles.class)
					.add(Restrictions.eq("id", id)).list();
			
			if(!files.isEmpty()){
				return files.get(0);
			}
			else{
				throw new SupplierFileNotFoundException();
			}
			
		}
		
		//gets all Supplier Files
	  @SuppressWarnings("unchecked")
	 public ArrayList<SupplierFiles> getSupplierFiles()throws SupplierFileNotFoundException{
		  log.info("inside getSupplierFiles()");
		  ArrayList<SupplierFiles> files=(ArrayList<SupplierFiles>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierFiles.class)
				  .addOrder(Order.desc("id"))
				  .list();
		  if(!files.isEmpty()){
			  return files;
		  }
		  else{
			  throw new SupplierFileNotFoundException();
		  }
	  }
	
	  
	 /*
	  * added by Jeevan on August 09,2013.. to get all the Orders between Datess..
	  */
	  
	  @SuppressWarnings("unchecked")
	 public ArrayList<SupplierReports> getSupplierReportsBetweenDays(Date startDate,Date endDate)throws SupplierReportsNotFoundException{
		  log.info("inside getSupplierReport()");
		  ArrayList<SupplierReports> reports=null;
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
		  if(null==startDate && null==endDate){
			  //dont add any restrictions..........
		  }
		  else if(null==startDate){
			  criteria.add(Restrictions.lt("upfrontPaidDate", endDate));
		  }
		  if(null==endDate){
			  criteria.add(Restrictions.ge("upfrontPaidDate", startDate));
		  }
		  else{
			  criteria.add(Restrictions.between("upfrontPaidDate", startDate, endDate));
		  }
		  criteria.addOrder(Order.desc("upfrontPaidDate"));
		  reports=(ArrayList<SupplierReports>) criteria.list();
		  if(!reports.isEmpty()){
			  return reports;
		  }
		  else{
			  throw new SupplierReportsNotFoundException();
		  }
	  }
	  
	  @SuppressWarnings("unchecked")
		 public ArrayList<SupplierReports> getSupplierReportsBetweenDays(Date startDate,Date endDate,String supplier)throws SupplierReportsNotFoundException{
			  log.info("inside getSupplierReport()");
			  ArrayList<SupplierReports> reports=null;
			  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
			  System.out.println(startDate);
			  System.out.println(endDate+"ddd" );
			  if(null!=startDate && null!=endDate){				  
				  Calendar cal=Calendar.getInstance();
					cal.setTime(endDate); cal.add(Calendar.DATE, 1); endDate=cal.getTime();
				  criteria.add(Restrictions.between("upfrontPaidDate", startDate, endDate));
			  }
			  else if(null!=startDate){
				  criteria.add(Restrictions.le("upfrontPaidDate", endDate));
			  }
			  else if(null!=endDate){
				  criteria.add(Restrictions.ge("upfrontPaidDate", startDate));
			  }
			  else{
				//dont add any restrictions..........
			  }	
			  
			  if(null!=supplier && supplier!=""){				 
				  criteria.add(Restrictions.eq("supplierName.supplierName", supplier));
			  }
			  
			  criteria.addOrder(Order.desc("upfrontPaidDate"));
			  reports=(ArrayList<SupplierReports>) criteria.list();
			  if(!reports.isEmpty()){
				  return reports;
			  }
			  else{
				  throw new SupplierReportsNotFoundException();
			  }
		  }
	  
	  //Getting Supplier Reports By Business Name
	  @SuppressWarnings("unchecked")
		 public ArrayList<SupplierReports> getSupplierReportsByBusinessNameBetweenDays(Date startDate,Date endDate,String businessName)throws SupplierReportsNotFoundException{
			  log.info("inside getSupplierReport()");
			  ArrayList<SupplierReports> reports=null;
			  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
			  System.out.println(startDate);
			  System.out.println(endDate+"ddd" );
			  if(null==startDate && null==endDate){
				  //dont add any restrictions..........
			  }
			  else if(null==startDate){
				  criteria.add(Restrictions.le("upfrontPaidDate", endDate));
			  }
			  else if(null==endDate){
				  criteria.add(Restrictions.ge("upfrontPaidDate", startDate));
			  }
			  else{
				  Calendar cal=Calendar.getInstance();
					cal.setTime(endDate); cal.add(Calendar.DATE, 1); endDate=cal.getTime();
				  criteria.add(Restrictions.between("upfrontPaidDate", startDate, endDate));
			  }	
			  
			  if(null!=businessName && businessName!=""){				 
				  criteria.add(Restrictions.eq("customerName", businessName).ignoreCase());
			  }
			  
			  criteria.addOrder(Order.desc("upfrontPaidDate"));
			  reports=(ArrayList<SupplierReports>) criteria.list();
			  if(!reports.isEmpty()){
				  return reports;
			  }
			  else{
				  throw new SupplierReportsNotFoundException();
			  }
		  }	
	  
	  /*
	   * Method to Get Supplier Reports Not Loaded on Pipeline..
	   * Added by Jeevan on August 28,2013 as per Clients Need to Display Supplier Reports Not in Pipeline
	   */
	  @SuppressWarnings("unchecked")
		 public ArrayList<SupplierReports> getSupplierReportsNotinPipeline(Date startDate,Date endDate,String supplier,int pageNo,int range)throws SupplierReportsNotFoundException{
			  log.info("inside getSupplierReport()");
			  int pageSize=range;
			  
			  ArrayList<SupplierReports> reports=null;
			  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);	
			  criteria.add(Restrictions.eq("updatedPipeline", false));
			  if(null==startDate && null==endDate){
				  //dont add any restrictions..........
			  }
			  else if(null==startDate){
				  criteria.add(Restrictions.le("upfrontPaidDate", endDate));
			  }
			  else if(null==endDate){
				  criteria.add(Restrictions.ge("upfrontPaidDate", startDate));
			  }
			  else{
				  Calendar cal=Calendar.getInstance();
					cal.setTime(endDate); cal.add(Calendar.DATE, 1); endDate=cal.getTime();
				  criteria.add(Restrictions.between("upfrontPaidDate", startDate, endDate));
			  }	
			  
			  if(null!=supplier && supplier!=""){				 
				  criteria.add(Restrictions.eq("supplierName.supplierName", supplier));
			  }
			  
			  criteria.addOrder(Order.desc("upfrontPaidDate"));
			  //added pagination by bhagya on spetember 17th, 2021
			  Integer totalRecords=criteria.list().size();
			  if(range>0){
					criteria.setFirstResult(pageNo*pageSize);
					criteria.setMaxResults(pageSize);
				}
			  reports=(ArrayList<SupplierReports>) criteria.list();
			  if(!reports.isEmpty()){
				  reports.get(0).setTotalRecords(totalRecords);
				  return reports;
			  }
			  else{
				  throw new SupplierReportsNotFoundException();
			  }
		  }
	  
	  /*
	   * Added by Jeevan on October 17, 2013 to get Supplier Reports of am Agent
	   * 
	   */
	  @SuppressWarnings("unchecked")
	public ArrayList<SupplierReports> getSupplierReportsOfAgent(String agentNumber)throws SupplierReportsNotFoundException{
		  log.info("inside getSupplierReportsOfAgent()");
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
		  criteria.createCriteria("agentId").add(Restrictions.eq("agentNumber", agentNumber));		  
		  ArrayList<SupplierReports> reports=(ArrayList<SupplierReports>) criteria.list();
		  if(!reports.isEmpty()){
			  return reports;
		  }
		  else{
			  throw new SupplierReportsNotFoundException();
		  }
	  }
	  
	  
	  /*
	   * Added by Jeevan on November 20,2013
	   * Getting Supplier Reports By Ids
	   */
	  @SuppressWarnings("unchecked")
	public ArrayList<SupplierReports> getSupplierReportsByIds(ArrayList<Integer> ids) throws SupplierReportsNotFoundException{
		  log.info("inside getSupplierReportsByIds()");
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
		  criteria.add(Restrictions.in("id", ids));	  
		  ArrayList<SupplierReports> reports=(ArrayList<SupplierReports>) criteria.list();
		  if(!reports.isEmpty()){
			  return reports;
		  }
		  else{
			  throw new SupplierReportsNotFoundException();
		  }		  
	  }
	  
	  
	 
	  /*
	   * Added by Jeevan on November 20,2013.
	   * Method to get Supplier Reports by Supplier File
	   * 
	   */
	  @SuppressWarnings("unchecked")
	public ArrayList<SupplierReports> getSupplierReportsBySupplierFile(Integer fileId) throws SupplierReportsNotFoundException{
		  log.info("inside getSupplierReportsBySupplierFile()");
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class);
		  criteria.createCriteria("supplierFile").add(Restrictions.eq("id", fileId));
		  ArrayList<SupplierReports> supplierReports=(ArrayList<SupplierReports>) criteria.list();
		  if(!supplierReports.isEmpty()){
			  return supplierReports;
		  }
		  else{
			  throw new SupplierReportsNotFoundException();
		  }
	  }
	  
	  
	  
	  /*
	   * Added by Jeevan on November 20, 2013
	   * Method to Delete Supplier Reports
	   */
	  public Integer deleteSupplierReports(ArrayList<SupplierReports> reports) throws Exception{
		  log.info("inside deleteSupplierReports()");
		   hibernateTemplate.deleteAll(reports);
		  hibernateTemplate.getSessionFactory().getCurrentSession().flush();
		  return reports.get(0).getId();		  
	  }
	  
	  /**
	   * Created By Bhagya On December 02nd 2015
	   * @return
	   * @throws Exception
	   * Method for getting the Business Names
	   */
	  @SuppressWarnings("unchecked")
		public ArrayList<String> getBusinessNamesFromSupplierReports()throws Exception{
			log.info("inside getBusinessNames()");
			ArrayList<String> orders=(ArrayList<String>) hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(SupplierReports.class)
					.setProjection(Projections.distinct(Projections.property("customerName"))).addOrder(Order.asc("customerName")).list();				
			return orders;					
		}
	  /**
	   * Created By Bhagya On August 16th, 2016
	   * @param championUpfrontCommissions
	   * @return
	   * 
	   * Method for to save the championUpfrontCommissions 
	   */
	  
	  public Integer saveChampionUpfrontCommissions(Integer orderId,Date dealStartDate,Integer term,Double commission) throws Exception{
		  log.info("inside saveChampionUpfrontCommissions()");
		  Query callStoredProcedure_MYSQL = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery("{CALL setup_payment_schedule(?,?,?,?)} ");
		  callStoredProcedure_MYSQL.setInteger(0, orderId);
		  callStoredProcedure_MYSQL.setDate(1, dealStartDate);
		  callStoredProcedure_MYSQL.setInteger(2, term);
		  callStoredProcedure_MYSQL.setDouble(3, commission);
		  
		  callStoredProcedure_MYSQL.list();
			
		  return orderId;
		 
	  }
	 
	 
	  /**
	   * Created By Bhagya on August 25th, 2016
	   * @param order
	   * @return
	   * @throws ChampionUpfrontCommissionNotFoundException
	   * 
	   * Method for to get the champion upfront commissions By Order
	   */
	  @SuppressWarnings("unchecked")
	  public ArrayList<ChampionUpfrontCommissions> getChampionUpfrontCommissionsByOrder(Integer orderId) throws ChampionUpfrontCommissionNotFoundException{
		  log.info("inside getChampionUpfrontCommissionsByOrder");
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(ChampionUpfrontCommissions.class);
		  criteria.add(Restrictions.eq("orderId", orderId));
		  ArrayList<ChampionUpfrontCommissions> championUpfrontCommissions=(ArrayList<ChampionUpfrontCommissions>)criteria.list();
		  if(!championUpfrontCommissions.isEmpty()){
			  return championUpfrontCommissions;
		  }
		  else{
			  throw new ChampionUpfrontCommissionNotFoundException();
		  }
	  }
	  
	  /***
	   * Created By Bhagya On sep 12th, 2016
	   * 
	   * @param orderId
	   * @param dealStartDate
	   * @param term
	   * @param commission
	   * @return
	   * @throws Exception
	   * 
	   * Method for to update the champion upfront commission ..
	   */
	  public Integer updateChampionUpfrontCommissions(Integer orderId,Date upfrontCommissionReceivedDate,Double upfrontCommissionReceived) throws Exception{
		  log.info("inside saveChampionUpfrontCommissions()");
		  Query callStoredProcedure_MYSQL = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery("{CALL update_payments(?,?,?)} ");
		  callStoredProcedure_MYSQL.setInteger(0, orderId);
		  if(null!=upfrontCommissionReceivedDate){
			  callStoredProcedure_MYSQL.setDate(1, upfrontCommissionReceivedDate);
		  }
		  else{
			  callStoredProcedure_MYSQL.setDate(1, new Date());
		  }
		  callStoredProcedure_MYSQL.setDouble(2, upfrontCommissionReceived);
		  callStoredProcedure_MYSQL.executeUpdate();
			
		  return orderId;
		 
	  }
	  /**
	   * Created By Bhagya on september 12th, 2016
	   * 
	   * @param orderId
	   * @param upfrontCommissionReceivedDate
	   * @param upfrontCommissionReceived
	   * @return
	   * @throws Exception
	   * 
	   * Method for to remove the commission of champion 
	   */
	  
	  public Integer removeChampionUpfrontCommissions(Integer orderId,Date upfrontCommissionReceivedDate,Double upfrontCommissionReceived) throws Exception{
		  log.info("inside saveChampionUpfrontCommissions()");
		  Query callStoredProcedure_MYSQL = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery("{CALL remove_payments(?,?,?)} ");
		  callStoredProcedure_MYSQL.setInteger(0, orderId);
		  if(null!=upfrontCommissionReceivedDate){
			  callStoredProcedure_MYSQL.setDate(1, upfrontCommissionReceivedDate);
		  }
		  else{
			  callStoredProcedure_MYSQL.setDate(1, new Date());
		  }
		  callStoredProcedure_MYSQL.setDouble(2, upfrontCommissionReceived);
		  callStoredProcedure_MYSQL.executeUpdate();
			
		  return orderId;
		 
	  }
	  /**
	   * Craeted By Bhagay On sep 14th, 2016
	   * @param orderId
	   * @param installmentNumber
	   * @param upfrontCommissionReceived
	   * @param upfrontCommissionReceivedDate
	   * @return
	   * 
	   * Method for to update the residual payments of champion order based on orderId and installment number
	   */
	  
	  public Integer updateResidualPaymentOfChampionOrderByOrderIdAndMonth(Integer orderId,Integer installmentNumber,Double upfrontCommissionReceived ,Date upfrontCommissionReceivedDate){
		  log.info("inside updateResidualPaymentOfChampionOrderByOrderIdAndMonth");
		  Query updatePaymentQuery = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery("UPDATE champion_upfront_commissions SET upfront_commission_received=? ,upfront_commission_received_date=? WHERE order_id=? and installment_number=? ");
		  updatePaymentQuery.setDouble(0, upfrontCommissionReceived);
		  if(null!=upfrontCommissionReceivedDate){
			  updatePaymentQuery.setDate(1, upfrontCommissionReceivedDate);
		  }
		  else{
			  updatePaymentQuery.setDate(1, new Date());
		  }
		  updatePaymentQuery.setInteger(2, orderId);
		  updatePaymentQuery.setDouble(3,installmentNumber);
		 Integer result= updatePaymentQuery.executeUpdate();
		 return result;
	  }
	  /***
	   * Created By bhagya On september 14th, 2016
	   * @param orderId
	   * @return
	   * @throws ChampionUpfrontCommissionNotFoundException
	   * 
	   * Method for get the commission paid Records by orderId
	   */
	  @SuppressWarnings("unchecked")
	  public ArrayList<ChampionUpfrontCommissions> getPaidChamiponResidualPaymentsByorderId(Integer orderId) throws ChampionUpfrontCommissionNotFoundException{
		  Criteria criteria=hibernateTemplate.getSessionFactory().getCurrentSession().createCriteria(ChampionUpfrontCommissions.class);
		  criteria.add(Restrictions.eq("orderId", orderId));
		  criteria.add(Restrictions.and(Restrictions.isNotNull("upfrontCommissionReceived"),Restrictions.isNotNull("upfrontCommissionReceivedDate")));
		  		  ArrayList<ChampionUpfrontCommissions> championUpfrontCommissions=(ArrayList<ChampionUpfrontCommissions>)criteria.list();
		  if(!championUpfrontCommissions.isEmpty()){
			  return championUpfrontCommissions;
		  }
		  else{
			  throw new ChampionUpfrontCommissionNotFoundException();
		  }
	  }
	  
}
