package com.hovey.backend.agent.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hovey.backend.agent.model.Orders;

/**
 * 	@author Bhagya
 *   created on August 09th, 2016
 *   A model for Champion Upfront commissions Details
 *   
 *   
 *   
 * **/

/**
 * @author Home
 *
 */
@Entity
@Table(name="champion_upfront_commissions")
public class ChampionUpfrontCommissions implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	
	@Column(name="order_id")
	private Integer orderId;
	
	
	@Column(name="installment_number")
	private Integer installmentNumber;
	
	@Column(name="upfront_commission_expected")
	private Double upfrontCommissionExpected;

	@Column(name="upfront_commission_expected_date")
	private Date upfrontCommissionExpectedDate;
	
	@Column(name="upfront_commission_received")
	private Double upfrontCommissionReceived;

	@Column(name="upfront_commission_received_date")
	private Date upfrontCommissionReceivedDate;

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	

	public Integer getInstallmentNumber() {
		return installmentNumber;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public void setInstallmentNumber(Integer installmentNumber) {
		this.installmentNumber = installmentNumber;
	}

	public Double getUpfrontCommissionExpected() {
		return upfrontCommissionExpected;
	}

	public void setUpfrontCommissionExpected(Double upfrontCommissionExpected) {
		this.upfrontCommissionExpected = upfrontCommissionExpected;
	}

	public Date getUpfrontCommissionExpectedDate() {
		return upfrontCommissionExpectedDate;
	}

	public void setUpfrontCommissionExpectedDate(Date upfrontCommissionExpectedDate) {
		this.upfrontCommissionExpectedDate = upfrontCommissionExpectedDate;
	}

	public Double getUpfrontCommissionReceived() {
		return upfrontCommissionReceived;
	}

	public void setUpfrontCommissionReceived(Double upfrontCommissionReceived) {
		this.upfrontCommissionReceived = upfrontCommissionReceived;
	}

	public Date getUpfrontCommissionReceivedDate() {
		return upfrontCommissionReceivedDate;
	}

	public void setUpfrontCommissionReceivedDate(Date upfrontCommissionReceivedDate) {
		this.upfrontCommissionReceivedDate = upfrontCommissionReceivedDate;
	}

	
	
}
