package com.hovey.backend.agent.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transactions")
public class Transactions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @author JEEVAN
	 * Created to include unique transaction_id for list of Orders in a Deal Sheet.
	 */
	
	
	@Id
	@GeneratedValue
	@Column(name="transaction_no")
	private Integer id;
	
	/**
	 *added qaDealStatus by bhagya on june 25th,2015 for implementing qa module,
	 *this field we are using for admin deal management 
	 */
	@Column(name="qa_deal_status")
	private Boolean qaDealStatus;
	
	/**
	 * added qaOrderStatus by bhagya on july 01st,2015 for implementing qa module,
	 * this field we are using at hoveyQA user
	 */
	@Column(name="qa_order_status")
	private Boolean qaOrderStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getQaDealStatus() {
		return qaDealStatus;
	}

	public void setQaDealStatus(Boolean qaDealStatus) {
		this.qaDealStatus = qaDealStatus;
	}

	public Boolean getQaOrderStatus() {
		return qaOrderStatus;
	}

	public void setQaOrderStatus(Boolean qaOrderStatus) {
		this.qaOrderStatus = qaOrderStatus;
	}
	
	
	
	
	
}
